from celery.schedules import crontab

"""
任务名称 算法名称+执行时间  
h:小时
m:分钟
dayofmonth: 每月的几号
"""
beat_schedule = {
    # 供需模块
    # 'link_1_hour_6': {
    #     'task': 'celery_task.balance_tasks.link_1',
    #     'schedule': crontab(hour=6)
    # },
    # 'link_2_h_6_m_40': {
    #     'task': 'celery_task.balance_tasks.link_2',
    #     'schedule': crontab(hour=6, minute=40)
    # },
    # 'link_3_hour_7_m_20': {
    #     'task': 'celery_task.balance_tasks.link_3',
    #     'schedule': crontab(hour=7, minute=20)
    # },
    'pr_range_dayofmonth_1_h_12': {
        'task': 'celery_task.balance_tasks.pr_range',
        'schedule': crontab(hour=12, day_of_month=1)
    },
    # 'parkduration_cluster_sr001_dayofmonth_1_h_0_m_0':{
    #     'task': 'celery_task.balance_tasks.parkduration_cluster_sr001',
    #     'schedule': crontab(hour=0, minute=0, day_of_month=1)
    # },
    # 'parkduration_cluster_sr002_dayofmonth_1_h_0_m_0': {
    #     'task': 'celery_task.balance_tasks.parkduration_cluster_sr002',
    #     'schedule': crontab(hour=0, minute=0, day_of_month=1)
    # },
    # 'parkduration_cluster_sr003_dayofmonth_1_h_0_m_0': {
    #     'task': 'celery_task.balance_tasks.parkduration_cluster_sr003',
    #     'schedule': crontab(hour=0, minute=0, day_of_month=1)
    # },


    # 自行车模块
    # 供给质态
    'bicycle_link_1_dayofmonth_1_h_0_m_0': {
        'task': 'celery_task.bicycle_tasks.bicycle_link_1',
        'schedule': crontab(hour=0, minute=0, day_of_month=1)
    },
    # # 需求特征分析
    # 'formatrel_defridmil_h_0_m_0': {
    #     'task': 'celery_task.balance_tasks.defsiterel_formatrel_defridmil_day',
    #     'schedule': crontab(minute=10, hour=0)
    # },
    # 'defsiterel_havgm_dayofmonth_1_h_1_m_30': {
    #     'task': 'celery_task.balance_tasks.defsiterel_havgm',
    #     'schedule': crontab(minute=30, hour=1, day_of_month=1)
    # },
    # 'formatrel_defridmil_davgm_dayofmonth_1_h_1': {
    #     'task': 'celery_task.balance_tasks.defsiterel_formatrel_defridmil_davgm',
    #     'schedule': crontab(minute=0, hour=1, day_of_month=1)
    # },
    # 'defsiterel_hour_h_*_m_10': {
    #     'task': 'celery_task.balance_tasks.defsiterel_hour',
    #     'schedule': crontab(minute=10, hour='*')
    # },
    # 动态调度
    'defregionpilecur_sitedispatch_hour_*_m_10_40': {
        'task': 'celery_task.bicycle_tasks.defregionpilecur_sitedispatch_hour',
        'schedule': crontab(minute='10,40', hour='*')
    },
    # # 静态调度
    # 'siterushhour_sitedispatch_sitedispatchrel*_m_10': {
    #     'task': 'celery_task.bicycle_tasks.siterushhour_sitedispatch_sitedispatchrel',
    #     'schedule': crontab(minute=30, hour='0')
    # },
    # 需求质态监测
    'regiondmnd_hour_h_*_m_10': {
        'task': 'celery_task.bicycle_tasks.regiondmnd_hour_day',
        'schedule': crontab(minute=10, hour='*')
    },
    # # 站点选址支持
    # 'bicycle_link_2_dayofmonth_1_h_0_m_10': {
    #     'task': 'celery_task.bicycle_tasks.bicycle_link_2',
    #     'schedule': crontab(minute=10, hour=0, day_of_month=1)
    # },
    # 站点运行效率评价
    'defsiteefcy_hour_h_*_m_10': {
        'task': 'celery_task.bicycle_tasks.defsiteefcy_hour',
        'schedule': crontab(minute=10, hour='*')
    },
    'defsiteefcy_day_h_0_m_40': {
        'task': 'celery_task.bicycle_tasks.defsiteefcy_day',
        'schedule': crontab(minute=40, hour=0)
    },
    'defsiteefcy_havgm_davgm_dayofmonth_1_h_0_m_15': {
        'task': 'celery_task.bicycle_tasks.defsiteefcy_havgm_davgm',
        'schedule': crontab(minute=30, hour=1, day_of_month=1)
    },
    # # 景区模块
    # # 景区人流分布
    # 'realtimeflow_h_*_m_15': {
    #     'task': 'celery_task.bicycle_tasks.realtimeflow',
    #     'schedule': crontab(minute=10, hour='*')
    # },
    # 'scenicinfo_flowimpactfact_dayofmonth_1_h_0_m_30': {
    #     'task': 'celery_task.bicycle_tasks.scenicinfo_flowimpactfact',
    #     'schedule': crontab(minute=30, hour=0, day_of_month=1)
    # },
}

