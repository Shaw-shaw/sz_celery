import os
import subprocess

from celery import chain, shared_task

from .celery import cel

ALGORITHM_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BICYCLE_ALGORITHM_DIR = os.path.join(ALGORITHM_PATH, "analysis_algorithm", 'bicycle_algorithm')


@cel.task(bind=True)
def example(self):
    """
    城市自定义区域建筑功能表 srcity_ft_defregionbldgfunc
    :return:
    """
    # command_list = ['/usr/bin/python3.7']
    # algorithm_path = os.path.join(BALANCE_ALGORITHM_DIR, 'defregioninfo.py')
    # command_list.append(algorithm_path)
    try:
        # subprocess.run(command_list)
        # subprocess.run(f'python {algorithm_path}')
        # import 导入
        from analysis_algorithm.bicycle_algorithm import defregioninfo
    except Exception as e:
        """

                retry的参数可以有：
                    exc：指定抛出的异常
                    throw：重试时是否通知worker是重试任务
                    eta：指定重试的时间／日期
                    countdown：在多久之后重试（每多少秒重试一次）
                    max_retries：最大重试次数
                """
        raise self.retry(exc=e, countdown=3, max_retries=5)


# 供给质态
@shared_task
def regionsupl():
    command_list = ['/usr/bin/python3.7']
    algorithm_path = os.path.join(BICYCLE_ALGORITHM_DIR, 'regionsupl.py')
    command_list.append(algorithm_path)
    try:
        subprocess.run(command_list)
    except Exception as e:
        print("任务执行失败")


@shared_task
def bicyclesiteinfo():
    command_list = ['/usr/bin/python3.7']
    algorithm_path = os.path.join(BICYCLE_ALGORITHM_DIR, 'bicyclesiteinfo.py')
    command_list.append(algorithm_path)
    try:
        subprocess.run(command_list)
    except Exception as e:
        print("任务执行失败")


@cel.task
def bicycle_link_1():
    task_link = chain(bicyclesiteinfo.signature(immutable=True),
                      regionsupl.si(immutable=True))
    task_link()


# 需求特征分析
@cel.task
def defsiterel_formatrel_defridmil_davgm(self):
    pass


@cel.task
def defsiterel_formatrel_defridmil_day(self):
    pass


@cel.task
def defsiterel_havgm(self):
    pass


@cel.task
def defsiterel_hour(self):
    pass


# 动态调度
@shared_task
def defregionpilecur_sitedispatch_hour():
    # from analysis_algorithm.bicycle_algorithm import defregionpilecur_sitedispatchdmd__sitedispatch_hour
    command_list = ['/usr/bin/python3.7']
    algorithm_path = os.path.join(BICYCLE_ALGORITHM_DIR, 'defregionpilecur_sitedispatchdmd__sitedispatch_hour.py')
    command_list.append(algorithm_path)
    try:
        subprocess.run(command_list)
    except Exception as e:
        print("任务执行失败")


# 静态调度
@cel.task
def siterushhour_sitedispatch_sitedispatchrel():
    pass


# 需求质态监测
@cel.task
def regiondmnd_hour_day():
    # from analysis_algorithm.bicycle_algorithm import regiondmnd_hour_day
    command_list = ['/usr/bin/python3.7']
    algorithm_path = os.path.join(BICYCLE_ALGORITHM_DIR, 'regiondmnd_hour_day.py')
    command_list.append(algorithm_path)
    try:
        subprocess.run(command_list)
    except Exception as e:
        print("任务执行失败")


# 站点选址支持
@shared_task
def defregionkpi_sitecrvblock():
    pass


@shared_task
def sitelocfactor():
    pass


@shared_task
def sitelocfactor_nocvr():
    pass


@cel.task
def bicycle_link_2():
    task_link = chain(defregionkpi_sitecrvblock.signature(immutable=True),
                      sitelocfactor.si(immutable=True),
                      sitelocfactor_nocvr.si(immutable=True)
                      )
    task_link()


# 运行效率评价
@cel.task
def defsiteefcy_day():
    command_list = ['/usr/bin/python3.7']
    algorithm_path = os.path.join(BICYCLE_ALGORITHM_DIR, 'defsiteefcy_day.py')
    command_list.append(algorithm_path)
    try:
        subprocess.run(command_list)
    except Exception as e:
        print("任务执行失败")


@cel.task
def defsiteefcy_havgm_davgm():
    command_list = ['/usr/bin/python3.7']
    algorithm_path = os.path.join(BICYCLE_ALGORITHM_DIR, 'defsiteefdefsiteefcy_havgm_davgm.py')
    command_list.append(algorithm_path)
    try:
        subprocess.run(command_list)
    except Exception as e:
        print("任务执行失败")


@cel.task
def defsiteefcy_hour():
    command_list = ['/usr/bin/python3.7']
    algorithm_path = os.path.join(BICYCLE_ALGORITHM_DIR, 'defsiteefcy_hour.py')
    command_list.append(algorithm_path)
    try:
        subprocess.run(command_list)
    except Exception as e:
        print("任务执行失败")


if __name__ == '__main__':
    print(ALGORITHM_PATH)
    print(BICYCLE_ALGORITHM_DIR)
