import os
import subprocess

from celery import chain, shared_task

from .celery import cel

ALGORITHM_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BALANCE_ALGORITHM_DIR = os.path.join(ALGORITHM_PATH, "analysis_algorithm", 'balance_algorithm')


@shared_task
def area_block_sr001():
    pass


@shared_task
def area_block_sr002():
    pass


@shared_task
def area_block_sr003():
    pass


@shared_task
def queuedcar_estimate_sr001():
    pass


@shared_task
def queuedcar_estimate_sr002():
    pass


@shared_task
def queuedcar_estimate_sr003():
    pass


@shared_task
def car_demand_sr001():
    pass


@shared_task
def car_demand_sr002():
    pass


@shared_task
def car_demand_sr003():
    pass


@shared_task
def car_demand_sr003():
    pass


@shared_task
def parkduration_cluster_sr001():
    command_list = ['/usr/bin/python3.7']
    algorithm_path = os.path.join(BALANCE_ALGORITHM_DIR, 'parkduration_cluster_sr001.py')
    command_list.append(algorithm_path)
    try:
        subprocess.run(command_list)
    except Exception as e:
        print("任务执行失败")


@shared_task
def parkduration_cluster_sr002():
    command_list = ['/usr/bin/python3.7']
    algorithm_path = os.path.join(BALANCE_ALGORITHM_DIR, 'parkduration_cluster_sr002.py')
    command_list.append(algorithm_path)
    try:
        subprocess.run(command_list)
    except Exception as e:
        print("任务执行失败")


@shared_task
def parkduration_cluster_sr003():
    command_list = ['/usr/bin/python3.7']
    algorithm_path = os.path.join(BALANCE_ALGORITHM_DIR, 'parkduration_cluster_sr003.py')
    command_list.append(algorithm_path)
    try:
        subprocess.run(command_list)
    except Exception as e:
        print("任务执行失败")


@cel.task
def link_1():
    task_link = chain(area_block_sr001.signature(immutable=True),
                      queuedcar_estimate_sr001.si(immutable=True),
                      car_demand_sr001.si(immutable=True))
    task_link()


@cel.task
def link_2():
    task_link = chain(area_block_sr002.signature(immutable=True),
                      queuedcar_estimate_sr002.si(immutable=True),
                      car_demand_sr002.si(immutable=True))
    task_link()


@cel.task
def link_3():
    task_link = chain(area_block_sr003.signature(immutable=True),
                      queuedcar_estimate_sr003.si(immutable=True),
                      car_demand_sr003.si(immutable=True))
    task_link()


@shared_task
def pr_range():
    command_list = ['/usr/bin/python3.7']
    algorithm_path = os.path.join(BALANCE_ALGORITHM_DIR, 'pr_range_0704.py')
    command_list.append(algorithm_path)
    try:
        subprocess.run(command_list)
    except Exception as e:
        print("任务执行失败")
