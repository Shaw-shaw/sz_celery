import os
import sys

# import celeryconfig
from celery import Celery
from datetime import timedelta
from celery.schedules import crontab
sys.path.insert(0, os.path.dirname(os.path.dirname(__file__)))
broker = 'redis://2.43.250.17:6379/0'  # 设置
# broker = 'redis://127.0.0.1:6379/0'  # 设置
backend = 'redis://2.43.250.17:6379/1'
# backend = 'redis://127.0.0.1:6379/1'
# 实例化Celery对象
cel = Celery('test', broker=broker, backend=backend,
             # 包含以下两个任务文件，去相应的py文件中找任务，对多个任务做分类
             include=[
                 'celery_task.balance_tasks',
                 'celery_task.bicycle_tasks',
                 'celery_task.scenic_tasks',
             ]
             )
# 时区
cel.conf.timezone = 'Asia/Shanghai'
# 是否使用UTC
cel.conf.enable_utc = False
# 在当前目录下创建配置文件用下面一行代码导入配置信息
cel.config_from_object('celeryconfig')



