from .celery import cel
from celery import chain, shared_task


@cel.task
def realtimeflow():
    from analysis_algorithm.scenic_algorithm import realtimeflow


@cel.task
def scenicinfo_flowimpactfact():
    from analysis_algorithm.scenic_algorithm import scenicinfo_flowimpactfact