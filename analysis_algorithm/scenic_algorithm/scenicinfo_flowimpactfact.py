#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 17 14:51:14 2020

@author: xin-yi.song
"""
import os
import datetime
import time
import json
import pandas as pd
import calendar
from shapely.geometry import Polygon,Point
import geopandas as gpd
from sklearn.ensemble import RandomForestRegressor
from dateutil.relativedelta import relativedelta

#定义时间，规定在每个月第一天的00：30在计算出前六个月/前一年的景区人流分布
now = datetime.datetime.now()
#now = datetime.datetime(2020, 5, 1, 0, 30, 0)   
begin_six = datetime.datetime((now - relativedelta(months=6)).year, 
                              (now - relativedelta(months=6)).month, 
                              1, 0, 0, 0)
begin_one = datetime.datetime((now - relativedelta(years=1)).year, 
                              (now - relativedelta(years=1)).month, 
                              1, 0, 0, 0)

end = datetime.datetime((now-datetime.timedelta(days=1)).year, 
                        (now-datetime.timedelta(days=1)).month, 
                        (now-datetime.timedelta(days=1)).day,
                        0, 0, 0)

#程序开始时间
start_time = time.time() 

################################连接到RDS#######################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'testdb_suzhou_analysis','utf8mb4'))

##############################导入自定义区域-拙政园###############################
#导入拙政园，为gcj坐标
with engine.connect() as conn:
    query = 'select sr_defregion_code, sr_defregion_cname, sr_defregion_boundarycns, sr_defregion_spatialtype,sr_defregion_center,sr_defregion_center_spatialtype' + '\n' \
          + 'from srcity_bt_defregioninfo' + '\n' \
          + 'where sr_defregion_code="sr007"'
    scenic_info = pd.read_sql(query, engine)

#按字段顺序准备要导入的数据表
scenic_info.rename(columns={'sr_defregion_code':'sr_scenic_code',
                            'sr_defregion_cname':'sr_scenic_cname',
                            'sr_defregion_boundarycns':'sr_scenic_boundarycns',
                            'sr_defregion_spatialtype':'sr_scenic_spatialtype',
                            'sr_defregion_center':'sr_scenic_center',
                            'sr_defregion_center_spatialtype':'sr_scenic_center_spatialtype'}, inplace=True) 
scenic_info['sr_insertedby'] = 'Xin-Yi Song'
scenic_info['sr_updatedby'] = 'Xin-Yi Song'
       
with engine.connect() as conn:
    conn.execute('delete from srcity_bt_scenicinfo')
    scenic_info.to_sql(
              name = 'srcity_bt_scenicinfo',
              con = engine,
              index = False,
              if_exists = 'append'
              )

###############################导入交通小区######################################
#导入交通小区，为gcj坐标
with engine.connect() as conn:
    query = 'select sr_neighbourhood_code, sr_neighbourhood_cname, sr_neighbourhood_boundarycns' + '\n' \
          + 'from srcity_bt_neighbourhoodinfo' + '\n' \
          + 'where sr_defregion_code="sr007"'
    middlezone_info = pd.read_sql(query, engine)

#将geojson的中括号转化为polygon
middlezone_info['geometry'] = middlezone_info['sr_neighbourhood_boundarycns'].apply(lambda x: [tuple(i) for i in json.loads(x)[0]])
middlezone_info['geometry'] = middlezone_info['geometry'].apply(lambda x: Polygon(x))
middlezone_info = gpd.GeoDataFrame(middlezone_info, geometry=middlezone_info['geometry'], crs='epsg:4326')
middlezone_info.to_crs('epsg:32651', inplace=True)
##通过绘图来检查
#base = middlezone_info.plot(facecolor='none', edgecolor='black')

###################################导入苏州POI##################################
#从文件中导入poi，不以code区分类型，直接以retype区分类型
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
SOURCE_DATA_DIR = os.path.join(BASE_DIR, 'source_data','suzhou_poi_alltypes_gcj_20200413.geojson')
poi = gpd.read_file(SOURCE_DATA_DIR)

#将地理坐标转化为投影坐标
poi.to_crs('epsg:32651', inplace=True)

#列出所有类型
retypes = list(poi['retype'].unique())
#依次跟middlezone做相交
middlezoneinfra = pd.DataFrame()

for retype in retypes:
    tmp = gpd.sjoin(middlezone_info, poi[poi['retype']==retype], how="left", op='contains')
    tmp['retype'] = retype
    tmp = tmp.groupby(by=['sr_neighbourhood_code','retype'],as_index=True).size().reset_index(name='count')
    middlezoneinfra = pd.concat([middlezoneinfra, tmp], ignore_index=True)

#按字段顺序准备要导入的数据表
middlezoneinfra.rename(columns={'sr_neighbourhood_code':'sr_middlezone_code',
                                'retype':'sr_infratype',
                                'count':'sr_infranum'}, inplace=True) 
middlezoneinfra['sr_scenic_code'] = 'sr007'  
middlezoneinfra['sr_stat_year'] = str(now.year)
middlezoneinfra['sr_stat_month'] = str(now.year) + '-' + str(now.month).zfill(2)
middlezoneinfra['sr_insertedby'] = 'Xin-Yi Song'
middlezoneinfra['sr_updatedby'] = 'Xin-Yi Song'
       
with engine.connect() as conn:
    #conn.execute('delete from srcity_index_scenic_middlezoneinfra')
    middlezoneinfra.to_sql(
              name = 'srcity_index_scenic_middlezoneinfra',
              con = engine,
              index = False,
              if_exists = 'append'
              )

################################连接到ODPS######################################
from odps import ODPS

odps_config = {
                "access_id": "LZR4s9cdwJdi82ID",
                "access_key": "jbelpqOhlBNDG4Wv6ILdOty0jaccO8",
                "project": "csdnsz_dma",
                "endpoint": "http://service.cn-suzhou-szdn-d01.odps.cloud-ops.szdn.suzhou.gov.cn/api"
                }
od = ODPS(
          odps_config.get("access_id"),
          odps_config.get("access_key"), 
          odps_config.get("project"),
          endpoint=odps_config.get("endpoint"))

##########################导入zone_id###########################################
#从大脑中读取运营商zone_id
sql = 'select * from traffic_base.idt_grid_info'

with od.execute_sql(sql).open_reader() as reader:
    grid_info = reader
    grid_info = grid_info.to_pandas()
    grid_info = grid_info[['grid','c_lon','c_lat']]
    
#将经度、纬度转化为Point
grid_info['geometry'] = grid_info.apply(lambda x: Point(x['c_lon'],x['c_lat']), axis=1)
grid_info = gpd.GeoDataFrame(grid_info, geometry=grid_info['geometry'], crs='epsg:4326')
grid_info.to_crs('epsg:32651', inplace=True)
    
#跟middlezone_info做相交
grid = gpd.sjoin(middlezone_info, grid_info, how="left", op='contains')
grid_scenic = grid[grid['sr_neighbourhood_cname'].str.contains('拙政园-02')]['grid'].tolist()

##############################建立前一年的日期属性表###############################
'''星期以数字标识:Sunday=0, Monday=1, ..., Saturday=6
   日期类型:1：工作日, 2：休息日, 3：节假日'''  
date_type = {'1':'工作日', '2':'双休日', '3':'节假日'}

#统计前一年有多少天
days = 366 if calendar.isleap(int(str(begin_one.year))) else 365
#生成上一个月的日历
period = pd.date_range(start=begin_one, end=end, periods=days)
period = pd.DataFrame(period, columns=['date'])
period['date'] = period['date'].dt.date
period['date'] = period['date'].apply(lambda x: str(x))
    
#查询上一个月每天的时间属性
sql = 'select * from traffic_base.dim_date_base' + '\n' \
    + 'where date_list in ' + str(tuple(period['date']))
    
with od.execute_sql(sql).open_reader() as reader:
    dates = reader
    dates = dates.to_pandas()
    dates['sr_date_type'] = dates['date_type'].apply(lambda x: date_type[x])
    dates['dt'] = dates['dt'].astype(int)

##############################从大脑中读取天气数据################################
sql = 'select * from csdnsz_bas.vastio_weather' + '\n' \
    + 'where date>=' + str(begin_one.year) + str(begin_one.month).zfill(2) + str(begin_one.day).zfill(2) + '\n' \
    + 'and date<=' + str(end.year) + str(end.month).zfill(2) + str(end.day).zfill(2) + '\n' \

with od.execute_sql(sql).open_reader() as reader:
    weather = reader
    weather = weather.to_pandas()
    weather['date'] = weather['date'].astype(int)
    weather['temp'] = (weather['max'].astype(int)+weather['min'].astype(int))/2
    weather['rain'] = weather['weather'].apply(lambda x: 1 if '雨' in x else 0)

########################求出前六个月/前一年各天每个小时的平均人流#############################
names = locals()
pop_all = pd.DataFrame(columns=['stat_date', 'zone_id', 'hour'])

#从大脑中读取运营商区域实时人数
for i in ['10010','10000','10086']:
    sql = 'select * from csdnsz_bas.' + 'yys_' + i + '_qyssrs' + '\n' \
        + 'where stat_date>=' + str(begin_one.year) + str(begin_one.month).zfill(2) + str(begin_one.day).zfill(2) + '\n' \
        + 'and stat_date<=' + str(end.year) + str(end.month).zfill(2) + str(end.day).zfill(2) + '\n' \
        + 'and zone_id in ' + str(tuple(grid['grid']))
        #+ 'and substring(time,-2)="00"' 

    with od.execute_sql(sql).open_reader() as reader:
        names['pop_%s' %i] = reader
        names['pop_%s' %i] = names['pop_%s' %i].to_pandas()
        names['pop_%s' %i]['time'] = names['pop_%s' %i]['time'].apply(lambda x: x.zfill(4))
        names['pop_%s' %i]['hour'] = names['pop_%s' %i]['time'].apply(lambda x: x[0:2])
        names['pop_%s' %i]['cnt_user'] = names['pop_%s' %i]['cnt_user'].astype(int)
    
    #将所有类型的人流相加
    names['pop_%s' %i] = names['pop_%s' %i].groupby(by=['stat_date','zone_id','time','hour'])['cnt_user'].sum().reset_index(name='cnt_user_'+i)
    #将每个小时内相隔十分钟的数据取平均
    names['pop_%s' %i] = names['pop_%s' %i].groupby(by=['stat_date','zone_id','hour'])['cnt_user_'+i].mean().reset_index()
    #合并三大运营商的数据
    pop_all = pd.merge(pop_all, names['pop_%s' %i], on=['stat_date', 'zone_id', 'hour'], how='outer')

#对合并后的三大运营商人流求和    
pop_all.fillna(0, inplace=True)
pop_all['cnt_user_all'] = pop_all['cnt_user_10010'] + pop_all['cnt_user_10000'] + pop_all['cnt_user_10086']

####################为景区及周边地区人流标注日期类型、地块信息和天气因素##################
pop_all = pd.merge(pop_all, dates[['dt','sr_date_type']], how='left', left_on='stat_date', right_on='dt')
pop_all = pd.merge(pop_all, grid[['sr_neighbourhood_code', 'sr_neighbourhood_cname','grid']], how='left', left_on='zone_id', right_on='grid')
pop_all = pd.merge(pop_all, weather[['date','temp','rain']], how='left', left_on='stat_date', right_on='date')

#挑选出景区内地块
pop_scenic = pop_all[pop_all['zone_id'].isin(grid_scenic)]

###############################求出景区人流高峰时段################################
for recordtime in ['最近一年','最近六个月']:
    #针对最近六个月调整数据时间
    if recordtime == '最近六个月':
        pop_scenic = pop_scenic[pop_scenic['stat_date']>=int(str(begin_six.year) + str(begin_six.month).zfill(2) + str(begin_six.day).zfill(2))]
    else:
        pass
    factors = pd.DataFrame()
    #根据日期类型，分别求出景区高峰时段
    for i in ['工作日', '双休日', '节假日']:
        tmp = pop_scenic[pop_scenic['sr_date_type']==i].groupby(by=['hour'])['cnt_user_all'].mean()
        if len(tmp.index)!=0:
            rushhour = tmp.idxmax()#.rstrip('00')
            print(i,rushhour)
            #按字段顺序准备要导入的数据表
            df = pd.DataFrame()
            df.loc[0,'sr_scenic_code'] = 'sr007'  
            df.loc[0,'sr_stat_year'] = str(now.year)
            df.loc[0,'sr_stat_month'] = str(now.year) + '-' + str(now.month).zfill(2)
            df.loc[0,'sr_date_type'] = i
            df.loc[0,'sr_stat_recordtime'] = recordtime
            df.loc[0,'sr_scenic_rushhour'] = rushhour
            df.loc[0,'sr_insertedby'] = 'Xin-Yi Song'
            df.loc[0,'sr_updatedby'] = 'Xin-Yi Song'
               
            with engine.connect() as conn:
               #conn.execute('delete from srcity_index_scenic_rushhour')
               df.to_sql(
                       name = 'srcity_index_scenic_rushhour',
                       con = engine,
                       index = False,
                       if_exists = 'append'
                       )
    
    ##########################计算景区及周边地块人流分布################################
    #根据日期类型，分别求出景区高峰时段的各地块人流
            pop_tmp = pop_all[(pop_all['sr_date_type']==i) & (pop_all['hour']==rushhour)]
            pop_tmp = pop_tmp.groupby(by=['zone_id','sr_neighbourhood_code', 'sr_neighbourhood_cname'])['cnt_user_all'].mean().reset_index()
            pop_tmp = pop_tmp.groupby(by=['sr_neighbourhood_code', 'sr_neighbourhood_cname'])['cnt_user_all'].sum().reset_index()
            #按字段顺序准备要导入的数据表
            dff = pd.DataFrame()
            dff['sr_middlezone_code'] = pop_tmp['sr_neighbourhood_code']
            dff['sr_withinscenic'] = pop_tmp['sr_neighbourhood_cname'].apply(lambda x: 1 if '拙政园-02' in x else 0)
            dff['sr_scenic_code'] = 'sr007'  
            dff['sr_stat_year'] = str(now.year)
            dff['sr_stat_month'] = str(now.year) + '-' + str(now.month).zfill(2)
            dff['sr_date_type'] = i
            dff['sr_stat_recordtime'] = recordtime
            dff['sr_scenic_middzoneflow'] = pop_tmp['cnt_user_all'].apply(lambda x: '%.0f' %x)
            dff['sr_insertedby'] = 'Xin-Yi Song'
            dff['sr_updatedby'] = 'Xin-Yi Song'
               
            with engine.connect() as conn:
               #conn.execute('delete from srcity_index_scenic_middlezoneflow')
               dff.to_sql(
                       name = 'srcity_index_scenic_middlezoneflow',
                       con = engine,
                       index = False,
                       if_exists = 'append'
                       )
    
    ##########################计算景区及区域范围影响因素###############################
            #根据日期类型，分别求出景区高峰时段的各基站人流后合并
            pop_tmp = pop_all[(pop_all['sr_date_type']==i) & (pop_all['hour']==rushhour)]
            #合并各特征日的数据
            factors = pd.concat([factors, pop_tmp], ignore_index=True)
    #挑选出要回归的要素
    factors['scenic'] = factors['sr_neighbourhood_cname'].apply(lambda x: 1 if '拙政园-02' in x else 0)
    factors['holiday'] = factors['sr_date_type'].apply(lambda x: 1 if x=='节假日' else 0)
    factors = factors[['cnt_user_all', 'temp', 'rain', 'holiday','scenic']]
    
    #针对全区域进行回归       
    #准备X_train, y_train
    X_train = factors[['temp','rain', 'holiday']]
    y_train = factors['cnt_user_all']
    #随机森林回归
    rf = RandomForestRegressor(n_estimators=100, criterion='mse', n_jobs=-1)
    rf.fit(X_train, y_train)
    #特征重要性选择
    imp = rf.feature_importances_
    imp = pd.DataFrame(data=imp, index=X_train.columns.tolist(), columns=['importance']).reset_index()
    #按字段顺序准备要导入的数据表
    factor_dict = {'temp':'温度', 'rain':'湿度', 'holiday':'法定假日'}
    dfff = pd.DataFrame()
    dfff['sr_scenic_factor'] = imp['index'].apply(lambda x: factor_dict[x])
    dfff['sr_scenic_factorvalue'] = imp['importance'].apply(lambda x: '%.2f' %x)
    dfff['sr_scenic_code'] = 'sr007'  
    dfff['sr_stat_year'] = str(now.year)
    dfff['sr_stat_month'] = str(now.year) + '-' + str(now.month).zfill(2)
    dfff['sr_stat_recordtime'] = recordtime
    dfff['sr_scenic_iftype'] = 0
    dfff['sr_insertedby'] = 'Xin-Yi Song'
    dfff['sr_updatedby'] = 'Xin-Yi Song'
           
    with engine.connect() as conn:
        #conn.execute('delete from srcity_index_scenic_flowimpactfact')
        dfff.to_sql(
                  name = 'srcity_index_scenic_flowimpactfact',
                  con = engine,
                  index = False,
                  if_exists = 'append'
                  )
    
    ##针对景区范围进行回归       
    #准备X_train, y_train
    factors_scenic = factors[factors['scenic']==1]
    X_train = factors_scenic[['temp','rain', 'holiday']]
    y_train = factors_scenic['cnt_user_all']
    #随机森林回归
    rf = RandomForestRegressor(n_estimators=100, criterion='mse', n_jobs=-1)
    rf.fit(X_train, y_train)
    #特征重要性选择
    imp = rf.feature_importances_
    imp = pd.DataFrame(data=imp, index=X_train.columns.tolist(), columns=['importance']).reset_index()
    #按字段顺序准备要导入的数据表
    factor_dict = {'temp':'温度', 'rain':'湿度', 'holiday':'法定假日'}
    dfff = pd.DataFrame()
    dfff['sr_scenic_factor'] = imp['index'].apply(lambda x: factor_dict[x])
    dfff['sr_scenic_factorvalue'] = imp['importance'].apply(lambda x: '%.2f' %x)
    dfff['sr_scenic_code'] = 'sr007'  
    dfff['sr_stat_year'] = str(now.year)
    dfff['sr_stat_month'] = str(now.year) + '-' + str(now.month).zfill(2)
    dfff['sr_stat_recordtime'] = recordtime
    dfff['sr_scenic_iftype'] = 1
    dfff['sr_insertedby'] = 'Xin-Yi Song'
    dfff['sr_updatedby'] = 'Xin-Yi Song'
           
    with engine.connect() as conn:
        #conn.execute('delete from srcity_index_scenic_flowimpactfact')
        dfff.to_sql(
                  name = 'srcity_index_scenic_flowimpactfact',
                  con = engine,
                  index = False,
                  if_exists = 'append'
                  )
    
    #程序结束时间
    over_time = time.time() 
    #程序运行时间
    total_time = over_time - start_time
    #打印耗费时间
    print(begin_one,'~',end,':',total_time)