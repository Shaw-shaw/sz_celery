#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 16:16:04 2020

@author: xin-yi.song
"""

import datetime
import time
import json
import pandas as pd
from shapely.geometry import Polygon,Point
import geopandas as gpd

#定义时间，规定在下个小时的第10分钟，计算上个小时的交通小区人流分布
now = datetime.datetime.now()
#now = datetime.datetime(2020, 6, 1, 23, 10, 0)
begin = datetime.datetime((now-datetime.timedelta(hours=1)).year, 
                          (now-datetime.timedelta(hours=1)).month, 
                          (now-datetime.timedelta(hours=1)).day, 
                          (now-datetime.timedelta(hours=1)).hour, 0, 0)
end = begin+datetime.timedelta(hours=1)

#程序开始时间
start_time = time.time() 

################################连接到RDS#######################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'testdb_suzhou_analysis','utf8mb4'))

###############################导入交通小区######################################
#导入交通小区，为gcj坐标
with engine.connect() as conn:
    query = 'select sr_neighbourhood_code, sr_neighbourhood_cname, sr_neighbourhood_boundarycns' + '\n' \
          + 'from srcity_bt_neighbourhoodinfo' + '\n' \
          + 'where sr_defregion_code="sr007"'
    middlezone_info = pd.read_sql(query, engine)

#将geojson的中括号转化为polygon
middlezone_info['geometry'] = middlezone_info['sr_neighbourhood_boundarycns'].apply(lambda x: [tuple(i) for i in json.loads(x)[0]])
middlezone_info['geometry'] = middlezone_info['geometry'].apply(lambda x: Polygon(x))
middlezone_info = gpd.GeoDataFrame(middlezone_info, geometry=middlezone_info['geometry'], crs='epsg:4326')
middlezone_info.to_crs('epsg:32651', inplace=True)
##通过绘图来检查
#base = middlezone_info.plot(facecolor='none', edgecolor='black')

################################连接到ODPS######################################
from odps import ODPS

odps_config = {
                "access_id": "LZR4s9cdwJdi82ID",
                "access_key": "jbelpqOhlBNDG4Wv6ILdOty0jaccO8",
                "project": "csdnsz_dma",
                "endpoint": "http://service.cn-suzhou-szdn-d01.odps.cloud-ops.szdn.suzhou.gov.cn/api"
                }
od = ODPS(
          odps_config.get("access_id"),
          odps_config.get("access_key"), 
          odps_config.get("project"),
          endpoint=odps_config.get("endpoint"))

###############################查询日期属性##################################
'''星期以数字标识:Sunday=0, Monday=1, ..., Saturday=6
   日期类型:1：工作日, 2：休息日, 3：节假日'''  
date_type = {'1':'工作日', '2':'双休日', '3':'节假日'}
    
#查询当天的时间属性
sql = 'select * from traffic_base.dim_date_base' + '\n' \
    + 'where date_list=' + '"' + str(begin.year) + '-' + str(begin.month).zfill(2) + '-' + str(begin.day).zfill(2) + '"'

with od.execute_sql(sql).open_reader() as reader:
    date = reader
    date = date.to_pandas()
  
##########################导入zone_id###########################################
#从大脑中读取运营商zone_id
sql = 'select * from traffic_base.idt_grid_info'

with od.execute_sql(sql).open_reader() as reader:
    grid_info = reader
    grid_info = grid_info.to_pandas()
    grid_info = grid_info[['grid','c_lon','c_lat']]
    
#将经度、纬度转化为Point
grid_info['geometry'] = grid_info.apply(lambda x: Point(x['c_lon'],x['c_lat']), axis=1)
grid_info = gpd.GeoDataFrame(grid_info, geometry=grid_info['geometry'], crs='epsg:4326')
grid_info.to_crs('epsg:32651', inplace=True)
    
#跟middlezone_info做相交
grid = gpd.sjoin(middlezone_info, grid_info, how="left", op='contains')

########################求出前一小时交通小区内的实时人流#############################
names = locals()
pop_all = pd.DataFrame(columns=['stat_date', 'zone_id', 'hour'],dtype = float)

#从大脑中读取运营商区域实时人数
for i in ['10010','10000','10086']:
    if begin.hour <23:
        sql = 'select * from csdnsz_bas.' + 'yys_' + i + '_qyssrs' + '\n' \
            + 'where stat_date=' + str(begin.year) + str(begin.month).zfill(2) + str(begin.day).zfill(2) + '\n' \
            + 'and time>=' + str(begin.hour).zfill(2) + str(begin.minute).zfill(2) + '\n' \
            + 'and time<' + str(end.hour).zfill(2) + str(end.minute).zfill(2) + '\n' \
            + 'and zone_id in ' + str(tuple(grid['grid']))
            #+ 'and substring(time,-2)="00"' 
    elif begin.hour == 23:
        sql = 'select * from csdnsz_bas.' + 'yys_' + i + '_qyssrs' + '\n' \
            + 'where stat_date=' + str(begin.year) + str(begin.month).zfill(2) + str(begin.day).zfill(2) + '\n' \
            + 'and time>=' + str(begin.hour).zfill(2) + str(begin.minute).zfill(2) + '\n' \
            + 'and zone_id in ' + str(tuple(grid['grid']))

    with od.execute_sql(sql).open_reader() as reader:
        names['pop_%s' %i] = reader
        names['pop_%s' %i] = names['pop_%s' %i].to_pandas()
        names['pop_%s' %i]['time'] = names['pop_%s' %i]['time'].apply(lambda x: x.zfill(4))
        names['pop_%s' %i]['hour'] = names['pop_%s' %i]['time'].apply(lambda x: x[0:2])
        names['pop_%s' %i]['cnt_user'] = names['pop_%s' %i]['cnt_user'].astype(int)
        
    #将所有类型的人流相加
    names['pop_%s' %i] = names['pop_%s' %i].groupby(by=['stat_date','zone_id','time','hour'])['cnt_user'].sum().reset_index(name='cnt_user_'+i)
    #将每个小时内相隔十分钟的数据取平均
    names['pop_%s' %i] = names['pop_%s' %i].groupby(by=['stat_date','zone_id','hour'])['cnt_user_'+i].mean().reset_index()
    #合并三大运营商的数据
    pop_all = pd.merge(pop_all, names['pop_%s' %i], on=['stat_date', 'zone_id', 'hour'], how='outer')

#对合并后的三大运营商人流求和    
pop_all.fillna(0, inplace=True)
pop_all['cnt_user_all'] = pop_all['cnt_user_10010'] + pop_all['cnt_user_10000'] + pop_all['cnt_user_10086']

####################为景区及周边地区人流标注地块信息，并按地块统计##################
pop_all = pd.merge(pop_all, grid[['sr_neighbourhood_code', 'sr_neighbourhood_cname','grid']], how='left', left_on='zone_id', right_on='grid')
pop_tmp = pop_all.groupby(by=['sr_neighbourhood_code', 'sr_neighbourhood_cname'])['cnt_user_all'].sum().reset_index()

###################################导入苏州项目库################################
#按字段顺序准备要导入的数据表
df = pd.DataFrame()
df['sr_middlezone_code'] = pop_tmp['sr_neighbourhood_code']
df['sr_scenic_middlezoneflow'] = pop_tmp['cnt_user_all']
df['sr_scenic_code'] = 'sr007'  
df['sr_stat_year'] = str(begin.year)
df['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
df['sr_stat_date'] = str(begin.year) + '-' + str(begin.month).zfill(2) + '-' + str(begin.day).zfill(2)
df['sr_date_type'] = date_type[date['date_type'][0]]
df['sr_stat_hour'] = str(begin.hour)
df['sr_insertedby'] = 'Xin-Yi Song'
df['sr_updatedby'] = 'Xin-Yi Song'
               
with engine.connect() as conn:
    #conn.execute('delete from srcity_index_scenic_realtimeflow')
    df.to_sql(
            name = 'srcity_index_scenic_realtimeflow',
            con = engine,
            index = False,
            if_exists = 'append'
            )
    
#程序结束时间
over_time = time.time() 
#程序运行时间
total_time = over_time - start_time
#打印耗费时间
print(begin,'~',end,':',total_time)