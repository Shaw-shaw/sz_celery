# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 14:19:56 2020

@author: levil
"""
from datetime import date, datetime, timedelta
import time
from sqlalchemy import create_engine
import pandas as pd
import numpy as np
from odps import ODPS
from sqlalchemy_tomysql import retry_write_todb
import requests
import json

def query_pandas(odps_config, sql):
        od = ODPS(odps_config.get("access_id"),odps_config.get("access_key"), odps_config.get("project"),
            endpoint=odps_config.get("endpoint"))
        with od.execute_sql(sql).open_reader() as reader:
            data = reader
        print('query df from odps successfully !!!')
        return data.to_pandas()

def request_url(url):
    js = requests.get(url).text
    data = json.loads(js)
    time_estimate = data['data']['rows'][0]['duration']
    print('request_url successfully !!!')
    return time_estimate

def count_queueTime(df,i_hour):
    high_low_ratio_dict ={0:1.2,  1:1.2,  2:1.2,  3:1.2, 
                          4:1.2,  5:1.2,  6:3,    7:3,
                          8:2.5,    9:2.5,  10:2.5, 11:2.5,
                          12:2.5, 13:2.5, 14:2.5, 15:2,
                          16:2  , 17:2,   18:1.5, 19:1.5,
                          20:1.2, 21:1.2, 22:1.2, 23:1.2}
    if len(df) != 0:
        df['time_estimate'] = np.nan
        error_url = []      
        for index, row in df.iterrows():
            parkinglot_coor = row['sr_pl_gc'][1:-1]
            time_record = row['timeinterval']        
            start_point = str(row['kkjd'])+','+str(row['kkwd'])
            end_point = parkinglot_coor
            url = 'http://2.46.4.200:21009/service/route/driving3?sPoint={}&ePoint={}&st=4&appKey=d4319e55861849cbb530e3ffc8b56f1f' 
            try:
                time_estimate = request_url(url.format(start_point,end_point))
            except:
                try:
                    time.sleep(1)
                    time_estimate = request_url(url.format(start_point,end_point))
                except:
                    time_estimate = float('inf')
                    error_url.append(url)
            queued = (high_low_ratio_dict[i_hour]*time_estimate < time_record and time_record <= 600)
            df.loc[index,'queued'] = queued
            df.loc[index,'time_estimate'] = time_estimate
        if 1 in df['queued'].values:
            print('count_queueTime successfully !!! queuedcar_num:  ',df.groupby('queued')['hphm'].count()[True])
            return df.groupby('queued')['hphm'].count()[True],df
        else:
            return 0,df

    else:
        pass
    
def update_output_df(formatted_df:pd.DataFrame,
                     region_code:str,
                     current_hour_datetime:datetime,
                     queuedcar_num:int):
    '''
    formatted_df为符合入库格式的空表
    '''
    formatted_df.loc[0,'sr_defregion_code'] = region_code
    formatted_df.loc[0,'sr_stat_time'] = current_hour_datetime
    # sr_defregion_aquecar字段虽然写入了int，但此时字段类型依然是object
    formatted_df.loc[0,'sr_defregion_aquecar'] = queuedcar_num
    
    formatted_df.loc[0,'sr_stat_date'] = date(current_hour_datetime.year,
                                         current_hour_datetime.month,
                                         current_hour_datetime.day)
    formatted_df.loc[0,['sr_insertedat','sr_updatedat']] = datetime.now()
    
    temp_columns_list = list(set(formatted_df.columns).difference(set(['sr_defregion_code',
                                                          'sr_stat_time',
                                                          'sr_defregion_aquecar',
                                                          'sr_stat_date',
                                                          'sr_insertedat',
                                                          'sr_updatedat'])))
    # 将剩余字段赋值为0，防止数据库字段默认NOT NULL而导致写入失败
    formatted_df.loc[0,temp_columns_list] = 0
    '''
    for field in temp_columns_list:
        formatted_df[field] = formatted_df[field].astype('int')
    '''
    # 将上面写入整形的sr_defregion_aquecar字段转换类型为int
    formatted_df['sr_defregion_aquecar'] = formatted_df['sr_defregion_aquecar'].astype('int')   
    return formatted_df

if __name__ == '__main__':
    odps_config = {
                    "access_id": "V6MeotKzDBgOv6vI",
                    "access_key": "P7YzxeRMfdr43e22C55ioOmsJP2UNu",
                    "project": "csdnsz_dma",
                    "endpoint": "http://service.cn-suzhou-szdn-d01.odps.cloud-ops.szdn.suzhou.gov.cn/api"
                    }
    conn = create_engine("mysql+pymysql://qftc_01:qftc123@rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn:3306/testdb_suzhou_analysis?charset=utf8mb4")
    sql_command_1 = "select * from srcity_bt_parkinglotinfo"
    #生成一张符合入库格式的空表
    formatted_output_df = pd.read_sql("select * from srcity_index_parking_defregiondmnd_hour limit 1", conn)
    formatted_output_df = pd.DataFrame(columns=formatted_output_df.columns)
    
    park_region_df = pd.read_sql(sql_command_1, conn)[['sr_pl_code','sr_defregion_code']]
    print('query park_region_df successfully !!!')

    for i_hour in range(24):
        
        stat_day = (datetime.now()-timedelta(days=1)).strftime('%Y-%m-%d 00:00:00')
        current_hour_datetime = datetime.strptime(stat_day,'%Y-%m-%d %H:%M:%S')+timedelta(hours=i_hour)

        current_hour =  str(current_hour_datetime)
        next_hour =  str(current_hour_datetime+timedelta(minutes=59,seconds=59))
        prev_hour = str(current_hour_datetime-timedelta(hours=1))
        current_dt = current_hour[:10].replace('-','')
        yesterday_dt =  str(current_hour_datetime-timedelta(hours=24))[:10].replace('-','')
        
        '''
        current_hour_datetime = datetime(2020, 3, i_day, i_hour, 0)
        current_hour =  str(current_hour_datetime)
        next_hour =  str(current_hour_datetime+timedelta(minutes=59,seconds=59))
        prev_hour = str(current_hour_datetime-timedelta(hours=1))
        current_dt = current_hour[:10].replace('-','')
        '''
        yesterday_dt =  str(current_hour_datetime-timedelta(hours=24))[:10].replace('-','')
        region_code = 'sr004'
        #sql_command_0 = 'select c.carid,c.in_time,c.parkcode,d.dwbh,d.jgsj,d.hphm,d.kkjd,d.kkwd,(UNIX_TIMESTAMP(c.in_time)-UNIX_TIMESTAMP(d.jgsj)) as timeinterval from \n    (select a1.carid,a1.in_time,a2.parkcode from traffic_base.ods_parking_log_di a1 join \n    (select STRING(inner_code) as inner_code,park_code as parkcode from traffic_base.dim_parking_siteinfo) a2 \n    on a1.park_code = a2.inner_code \n    where in_time between datetime("{current_hour}") and datetime("{next_hour}")\n    and parkcode in {parkcode_list}) c \nleft join\n    (select b1.dwbh,b1.jgsj,b1.hphm,b2.lon as kkjd,b2.lat as kkwd from traffic_base.dws_kkod_dwlx_di b1  \n    left join traffic_base.dim_kk_siteinfo b2\n    on  b1.dwbh = b2.dwbh \n    where jgsj between  datetime("{prev_hour}") and datetime("{next_hour}")) d \non c.carid = d.hphm\nwhere d.dwbh is not null and (UNIX_TIMESTAMP(c.in_time)-UNIX_TIMESTAMP(d.jgsj)) > 0'    
        if 5 < i_hour < 22:
            sql_command_0 = 'SELECT  c.carid\n        ,c.in_time\n        ,c.parkcode\n        ,d.dwbh\n        ,d.jgsj\n        ,d.hphm\n        ,d.kkjd\n        ,d.kkwd\n        ,(UNIX_TIMESTAMP(c.in_time)-UNIX_TIMESTAMP(d.jgsj)) AS timeinterval\nFROM    (\n            SELECT  a1.carid\n                    ,a1.in_time\n                    ,a2.parkcode\n            FROM    (\n                        SELECT  *\n                        FROM    traffic_base.ods_parking_log_di\n                        WHERE   dt = "{current_dt}"\n                    ) a1\n            JOIN    (\n                        SELECT  STRING(inner_code) AS inner_code\n                                ,park_code AS parkcode\n                        FROM    traffic_base.dim_parking_siteinfo\n                    ) a2\n            ON      a1.park_code = a2.inner_code\n            AND     (in_time BETWEEN datetime("{current_hour}") AND datetime("{next_hour}"))\n            AND     parkcode IN {parkcode_list}\n        ) c LEFT\nJOIN    (\n            SELECT  b1.dwbh\n                    ,b1.jgsj\n                    ,b1.hphm\n                    ,b2.lon AS kkjd\n                    ,b2.lat AS kkwd\n            FROM    (\n                        SELECT  *\n                        FROM    traffic_base.dws_kkod_dwlx_di\n                        WHERE   (dt = "{current_dt}" OR dt = "{yesterday_dt}")\n                    ) b1\n            JOIN    traffic_base.dim_kk_siteinfo b2\n            ON      b1.dwbh = b2.dwbh\n            AND     (jgsj BETWEEN datetime("{prev_hour}") AND datetime("{next_hour}"))\n        ) d\nON      (\n                c.carid = d.hphm\n            AND d.dwbh IS NOT NULL\n            AND (UNIX_TIMESTAMP(c.in_time) - UNIX_TIMESTAMP(d.jgsj)) > 0\n        )\n'

            parkcode_list = tuple(park_region_df[park_region_df['sr_defregion_code']==region_code]['sr_pl_code'].unique())
            sql_para_dict = {'parkcode_list':parkcode_list,
                             'prev_hour':prev_hour,
                            'current_hour':current_hour,
                            'next_hour':next_hour,
                            'current_dt':current_dt,
                            'yesterday_dt':yesterday_dt}

            sql_command_2 = sql_command_0.format(**sql_para_dict)
            df = query_pandas(odps_config, sql_command_2)
            df = df.where(df.notnull(),np.nan)
            if len(df) != 0:
                # 按停车场和车牌号取出卡口检测时间和停车入库时间相距最短的记录
                min_timeinterval = df['timeinterval'].groupby([df['parkcode'],df['carid']]).min().reset_index()
                df = pd.merge(left=df, right=min_timeinterval, how='inner', 
                          left_on=['parkcode','carid','timeinterval'], right_on=['parkcode','carid','timeinterval'])
                df = df.drop_duplicates().reset_index()
    
                #关联srcity_bt_parkinglotinfo的停车场经纬度
                parkinglot_df = pd.read_sql("select * from srcity_bt_parkinglotinfo", conn)
                print('query parkinglot_df successfully !!!')
                df = df.merge(how='left',
                               right = parkinglot_df,
                               left_on = 'parkcode',
                               right_on = 'sr_pl_code')[['carid', 'in_time', 'parkcode', 
                                                       'dwbh', 'jgsj', 'hphm', 'kkjd', 'kkwd',
                                                       'timeinterval','sr_pl_code','sr_pl_gc']]
                                        
                queuedcar_num,queuedcar_temp_df = count_queueTime(df,i_hour)

                # 按自定义区域，统计日期和统计时段查询 srcity_index_parking_defregiondmnd_hour
                
                temp_df = update_output_df(formatted_output_df,region_code,
                                           current_hour_datetime,queuedcar_num)
            else:
                temp_df = update_output_df(formatted_output_df,region_code,
                                           current_hour_datetime,0)
        else:
                temp_df = update_output_df(formatted_output_df,region_code,
                                                   current_hour_datetime,0)

        retry_write_todb(temp_df, conn, 5, 'srcity_index_parking_defregiondmnd_hour')
        
    conn.dispose()
    
