# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 23:00:50 2020

@author: Boyang_Xia
"""
from .aaa import a
from logger_test import get_logger
get_logger('testaaa').warning('ceshi')
#人民路-观前街 区域 sr001
#该脚本用于 计算
#5.在停车辆进出区域基础结果表 sr_ft_parkinregion_result 
import numpy as np
import pandas as pd
#import geopandas
import datetime
from sqlalchemy import create_engine
import time
import gc
#import os
#

#目前数据库中有3.16 - 4.19 号的数据。
'''
#代码的入参：当前时间 - 1days 的一整天(24小时)数据
#这24小时的第一小时（0点）作为tm_start，这一天的结束（23点）作为tm_end
day_use = datetime.datetime.now() - datetime.timedelta(days=1)
tm_start = pd.to_datetime(day_use.strftime('%Y-%m-%d'))
tm_end = tm_start + datetime.timedelta(days=1) - datetime.timedelta(hours=1)
'''

#阿里云数据库
engine = create_engine("mysql+pymysql://qftc_01:qftc123@rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn:3306/testdb_suzhou_analysis?charset=utf8mb4")
#
'''
删除中间结果表 sr_ft_parkinregion_result 中的 sr_defregion_code 为‘00’的数据 （清理空间）
'''
sql_words = "DELETE FROM sr_ft_parkinregion_result WHERE sr_defregion_code = '00'"
engine.execute(sql_words)     



#从城市大脑加载车辆od
from odps import ODPS
#from odps import ODPS,options
#from odps.df import DataFrame

odps_config = {
    "access_id": "V6MeotKzDBgOv6vI",
    "access_key": "P7YzxeRMfdr43e22C55ioOmsJP2UNu",
    "project": "csdnsz_dma",
    "endpoint": "http://service.cn-suzhou-szdn-d01.odps.cloud-ops.szdn.suzhou.gov.cn/api"
}


def query_pandas(odps_config, sql):
        od = ODPS(odps_config.get("access_id"),odps_config.get("access_key"), odps_config.get("project"),
            endpoint=odps_config.get("endpoint"))
        with od.execute_sql(sql).open_reader() as reader:
            data = reader
        return data.to_pandas()
#sql_words = "SELECT * from traffic_base.dws_kkod_dwlx_di WHERE (jgsj >= DATETIME('2020-03-13 08:00:00')) AND (jgsj < DATETIME('2020-03-13 09:00:00'))"


#写一个大的循环，每一小时运行一次，首先把卡口数据包括的一天的数据拆成24小时



#从数据库读入
#中位数停车时长表（按月更新）
#from sqlalchemy import create_engine
#engine = create_engine("mysql+pymysql://suzhou_analysis:123456@101.89.91.175:33065/testdb_suzhou_analysis?charset=utf8mb4")
sql_cmd = "SELECT * FROM sr_ft_parkduration_median"
pktime_df = pd.read_sql(sql_cmd,con=engine)
pktime_df['sr_isweekday'][pktime_df['sr_isweekday'] == 'TRUE'] = 'True'
pktime_df['sr_isweekday'][pktime_df['sr_isweekday'] == 'FALSE'] = 'False'
pktime_df.rename(columns={'sr_timetype':'feng','sr_bldgfunc':'function','sr_isweekday':'is_weekday',
                          'sr_parkdrtn_median':'parktime'},inplace=True)
pktime_df = pktime_df[['feng','function','is_weekday','parktime']]
median_pkm = pktime_df


#地块吸引力表（按月更新）
#from sqlalchemy import create_engine
#engine = create_engine("mysql+pymysql://suzhou_analysis:123456@101.89.91.175:33065/testdb_suzhou_analysis?charset=utf8mb4")
sql_cmd = "SELECT * FROM sr_ft_blockattract"
blk_attract = pd.read_sql(sql_cmd,con=engine)
blk_attract['sr_isweekday'][blk_attract['sr_isweekday'] == 'TRUE'] = 'True'
blk_attract['sr_isweekday'][blk_attract['sr_isweekday'] == 'FALSE'] = 'False'
blk_attract.rename(columns={'sr_defblock_code':'block_id','sr_timetype':'feng','sr_isweekday':'is_weekday',
                          'sr_defblock_attrvalue':'attract'},inplace=True)
blk_attract = blk_attract[['block_id','feng','is_weekday','attract']]
block_attract = blk_attract


#自定义区域卡口服务域表
#from sqlalchemy import create_engine
#engine = create_engine("mysql+pymysql://suzhou_analysis:123456@101.89.91.175:33065/testdb_suzhou_analysis?charset=utf8mb4")
sql_cmd = "SELECT * FROM sr_ft_defregion_iid"
newid_serve = pd.read_sql(sql_cmd,con=engine)
#改格式
iid_to_blockID_n = pd.DataFrame(columns=['iid','block_id'])
uq_id = pd.unique(newid_serve.sr_defbayonet_code)
#i = uq_id[0]
for i in uq_id:
    blk = list(newid_serve['sr_defblock_code'][newid_serve.sr_defbayonet_code == i])
    iid_blk = pd.DataFrame({'iid':i,'block_id':[blk]},index=[0])
    iid_to_blockID_n = iid_to_blockID_n.append(iid_blk, ignore_index=True)

#iid_to_blockID_n#其中 iid为卡口编号，blick_id为地块编号
    
  
    
#路径概率结果表
#from sqlalchemy import create_engine
#engine = create_engine("mysql+pymysql://suzhou_analysis:123456@101.89.91.175:33065/testdb_suzhou_analysis?charset=utf8mb4")
sql_cmd = "SELECT * FROM sr_ft_pathprob"
road_prob = pd.read_sql(sql_cmd,con=engine)
#删除超过2公里距离的，超过两公里认为两个卡口就没有关系了
road_prob = road_prob[road_prob['sr_defbayonet_dist'] < 2000]
#
road_prob.rename(columns={'sr_defbayonet_start':'origin','sr_defbayonet_end':'destin','sr_defbayonet_dist':'Odist',
                          'sr_defbayonet_stopstart':'freq'},inplace=True)

road_prob = road_prob[['origin','destin','Odist','freq']]




#
'''
找出 区域内 卡口 和 区域内+周边卡口。较之于旧版，dwid和iid统一起来了，所以两个字段用相同的即可
'''
'''
这部分数据后续从库里取
'''
#从数据库中读取卡口基础数据
sql_cmd = "SELECT * FROM sr_ft_bayonetinfo"
kk_haveserve = pd.read_sql(sql_cmd,con=engine)
#提取区域内卡口
#修改字段名与代码一致
kakou_inside = kk_haveserve[kk_haveserve['sr_defregion1_code'] == 'sr001'][['sr_defbayonet_code']]
kakou_inside['dwbh'] = kakou_inside['sr_defbayonet_code'] 
kakou_inside['iid'] = kakou_inside['sr_defbayonet_code']
kakou_inside = kakou_inside[['dwbh','iid']]
kakou_inside = kakou_inside.reset_index(drop=True)
#提取缓冲区区域内卡口
#修改字段名与代码一致
kakou_st = kk_haveserve[kk_haveserve['sr_defregionbuffer1_code'] == 'sr001'][['sr_defbayonet_code']]
kakou_st['dwbh'] = kakou_st['sr_defbayonet_code'] 
kakou_st['iid'] = kakou_st['sr_defbayonet_code']
kakou_st = kakou_st[['dwbh','iid']]
kakou_st = kakou_st.reset_index(drop=True)



'''
#加载试点区域地块id，后续固定下来，不需要读文件了
import pickle
file_path = 'D:/suzhou_police_program/gongxu_analysis/exp_data_to_mysql_20200331/srcity_bt_defblockinfo.pkl'
blk_uq_new = pickle.load(open(file_path,'rb'))
blk_uq_new.plot()

block_shidian = list(blk_uq_new['sr_defblock_code'][blk_uq_new.sr_defregion_code == 'sr001'])
'''
#从数据库取 block 的主要建筑功能
sql_cmd = "SELECT * FROM srcity_bt_defblockinfo"
block_info = pd.read_sql(sql_cmd,con=engine)
block = block_info[['sr_defblock_code','sr_bldgfunc_primary']]
block.rename(columns = {'sr_defblock_code':'block_id','sr_bldgfunc_primary':'function'},inplace=True)


'''
#建表：字段包括：区域名称、车牌号、出发时间、进场时间、出场时间、停车时长、起始点卡口、终点卡口、
#              停在地块一的概率、停在地块二的概率......停在地块n的概率、更新时间
'''

#试点区域地块名称：
#最后只选出关心的地块数据

#构建空表(后续会使用其列名)
basic_df = pd.DataFrame(columns=['area_name','car_id','pk_code','departtime','intime','outtime','time_len','inday_tp','inhour_tp','kk_start',
                                 'kk_end','kk_out','sb00044','sb00045','sb00091','sb00092','sb00096','sb00100','sb00101',
                                 'sb00109','sb00110','sb00111','sb00113','sb00114','update_time'])


'''
#定义一个function：，判断特征日
'''
def is_week(dt):
    wk = dt.weekday()
    if wk < 5:
        is_weekday = 'True'
    else:
        is_weekday = 'False'
    return is_weekday
#tm_mid = tm_start + datetime.timedelta(days=6)
#is_week(tm_mid)

'''
#定义一个function：判断时段类型
'''
def which_feng(dt):
    tm = int(dt.strftime('%H'))
    if (tm >= 0) & (tm < 7):
        tp = "7_夜间"
    elif tm >= 22:
        tp = "7_夜间"
    elif (tm >= 7) & (tm < 9):
        tp = "1_早高峰"
    elif (tm >= 9) & (tm < 11):
        tp = "2_早平峰"
    elif (tm >= 11) & (tm < 13):
        tp = "3_午高峰"
    elif (tm >= 13) & (tm < 16):
        tp = "4_午平峰"
    elif (tm >= 16) & (tm < 18):
        tp = "5_晚高峰"
    elif (tm >= 18) & (tm < 22):
        tp = "6_晚平峰"
    return tp




'''
##定义一个function：输入一行数据，输出车牌号、进场时间、起始点卡口、终点卡口，停在各个地块的概率、更新时间。
'''
def prob_distribution(row_data):
    #由于apply传参是series，因此需要先转为dataframe
    row_data = pd.DataFrame(data=row_data.values.reshape(1,-1),columns=row_data.index,index=[row_data.name])
    #匹配卡口编号到iid
    find_iid = kakou_inside[kakou_inside.dwbh.isin(row_data.dwbh)].iid
    #根据iid找到要分配的地块，
    blk_use = iid_to_blockID_n[iid_to_blockID_n.iid.isin(find_iid)]['block_id']
    #提取相应地块的吸引力
    blk_tp = block_attract[(block_attract.is_weekday == day_tp) & (block_attract.feng == feng_tp)]
    #在此假设用1进行分配
    blk_get = blk_tp[blk_tp.block_id.isin(list(blk_use)[0])]
    #计算分配到各个地块的概率
    prob_distrb = np.array(blk_get.attract / blk_get.attract.sum())
    blk_distrb = list(map(lambda m: str(m),list(blk_get.block_id)))
    carin_df = pd.DataFrame(prob_distrb.reshape(1,-1),columns=blk_distrb)
    #向上寻找直到找到O
    last_row = od_relaD.loc[row_data.index,]  
    while list(last_row['dwlx']) != ['O点']:
        try:
            od_relaD.loc[last_row.index-1,]
        except(KeyError):
            last_row.iloc[:,:] = np.nan
            break           
        else:
            last_row = od_relaD.loc[last_row.index-1,]
    #last_row = od_relaD.iloc[row_data.index-1,]  
    #while list(last_row['dwlx']) != ['O点']:
    #    last_row = od_relaD.iloc[last_row.index-1,]
    #组成完整的df，append 到basic_df上
    results_row = pd.DataFrame({'area_name':'sr001','car_id':list(row_data.hphm),'departtime':list(last_row.jgsj),
                  'intime':list(row_data.jgsj),'inday_tp':day_tp,'inhour_tp':feng_tp,'kk_start':list(last_row.dwbh),'kk_end':list(row_data.dwbh),
                  'update_time':tm_mid
                  },index=[0])
    results_row_2 = results_row.join(carin_df)
    results_df = results_row_2.reindex(columns=basic_df.columns)
    return results_df

'''
定义一个function，分配两个卡口的服务域。(若N是试点区域内的卡口，那么重新分配概率，分配原则是同时分配到D和N的服务区域)
'''
def prob_distribution_DN(row_in,N_poi):
    #由于apply传参是series，因此需要先转为dataframe
    #row_data = pd.DataFrame(data=row_data.values.reshape(1,-1),columns=row_data.index,index=[row_data.name])
    #D和N分别匹配卡口编号到iid
    find_iid_D = kakou_inside[kakou_inside.dwbh.isin(row_in.kk_end)].iid
    find_iid_N = kakou_inside[kakou_inside.dwbh.isin(N_poi.dwbh)].iid
    #find_iid_N = kakou_inside[1:2].iid
    #根据iid找到D，N要分配的地块，
    blk_use_D = iid_to_blockID_n[iid_to_blockID_n.iid.isin(find_iid_D)]['block_id'].values[0]
    blk_use_N = iid_to_blockID_n[iid_to_blockID_n.iid.isin(find_iid_N)]['block_id'].values[0]
    blk_use_DN = pd.unique(blk_use_D + blk_use_N)    
    #提取相应地块的吸引力
    blk_tp = block_attract[(block_attract.is_weekday == row_in.inday_tp.values[0]) & (block_attract.feng == row_in.inhour_tp.values[0])]
    #在此假设用1进行分配
    blk_get = blk_tp[blk_tp.block_id.isin(blk_use_DN)]
    #计算分配到各个地块的概率
    prob_distrb = np.array(blk_get.attract / blk_get.attract.sum())
    blk_distrb = list(map(lambda m: str(m),list(blk_get.block_id)))
    carin_df = pd.DataFrame(prob_distrb.reshape(1,-1),columns=blk_distrb)
    #原数据和刚刚重新概率分配的结果进行拼接。
    first_pa = row_in.iloc[:,0:12]
    second_pa = carin_df
    second_pa.index = first_pa.index
    thrid_pa = row_in.iloc[:,24:25]
    out_df = first_pa.join(second_pa).join(thrid_pa)
    #输出拼接后的dataframe
    results_df = out_df.reindex(columns=basic_df.columns)
    return results_df

  


'''
追踪 N 点（下一次出现点），注意：car_in_byD中实际上有一些在当时的小时就已经出场了。所以追踪要考虑当时小时和下一小时，两个小时。
'''   
'''
先追踪当前小时出场的数据
'''  
#提取所有car_in_byD的车牌号，查看该小时是否有出场，如果有就附上出场时间和停车时长。
#定义一个function，输入每行数据，输出可能带出场时间和停车时长的数据。
def out_car_check(row_in):
    #由于apply传参是series，因此需要先转为dataframe
    row_in = pd.DataFrame(data=row_in.values.reshape(1,-1),columns=row_in.index,index=[row_in.name])
    #寻找该车牌号在这一小时的所有记录
    record_carod = od_relaO[od_relaO.hphm.isin(row_in.car_id)]
    #找出该行所在位置的下一条数据，如果有在判断刚才是否真的停了。如果没有直接返回原数据
    record_later = record_carod[record_carod.jgsj > row_in.intime.values[0]]
    #如果record_later为空，则返回原数据，不添加更新时间
    if len(record_later) == 0:
        out_df = row_in
    else:
        #否则，需要找出N点，然后判断N点和D点的关系。并添加更新时间。
        N_poi = record_later.iloc[0:1]
        #若N和D是同一点，那么直接添加出场时间、出场卡口、和停车时长
        if N_poi.dwbh.values[0] == row_in.kk_end.values[0]:
            row_in.outtime = N_poi.jgsj.values[0]
            tm_len = (N_poi.jgsj.values[0] - row_in.intime.values[0]).astype('timedelta64[m]').astype(int)
            row_in.time_len = tm_len
            row_in.kk_out = N_poi.dwbh.values[0]
        #若N是试点区域内的卡口，那么重新分配概率，分配原则是分配到D和N的服务区域，添加出场时间、出场卡口和停车时长
        #定义一个function，分配两个卡口的服务域。
        elif N_poi.dwbh.isin(kakou_inside.dwbh).values: 
            row_in = prob_distribution_DN(row_in,N_poi)
            #添加出场时间和停车时长
            row_in.outtime = N_poi.jgsj.values[0]
            tm_len = (N_poi.jgsj.values[0] - row_in.intime.values[0]).astype('timedelta64[m]').astype(int)
            row_in.time_len = tm_len
            row_in.kk_out = N_poi.dwbh.values[0]
        #若N是缓冲区内的卡口，那么根据路径概率，计算D应该分配多少，重新在D附近分配，添加出场时间和停车时长
        elif N_poi.dwbh.isin(kakou_st.dwbh).values:
            N_iid = kakou_st[kakou_st.dwbh.isin(N_poi.dwbh)].iid
            D_iid = kakou_st[kakou_st.dwbh.isin(row_in.kk_end)].iid
            #根据路径概率，判断应该有多少概率用于分配。
            new_prob = road_prob[road_prob.origin.isin(D_iid) & road_prob.destin.isin(N_iid)].freq
            if len(new_prob) == 0:
                prob_re = 0
                #所有字段都赋值nan
                row_in.iloc[:,:] = np.nan                           
            else:
                prob_re = new_prob.values[0]
                #所有已经分配的概率值都乘以prob_re
                row_in.iloc[:,12:24] = row_in.iloc[:,12:24] * prob_re
                #添加出场时间和停车时长
                row_in.outtime = N_poi.jgsj.values[0]
                tm_len = (N_poi.jgsj.values[0] - row_in.intime.values[0]).astype('timedelta64[m]').astype(int)
                row_in.time_len = tm_len
                row_in.kk_out = N_poi.dwbh.values[0]
        #若N是远距离卡口，那直接把该行数据的值全部归为nan。
        else:
            row_in.iloc[:,:] = np.nan
        #添加更新时间
        row_in.update_time = tm_mid
        #输出处理后的该行记录
        out_df = row_in
    return out_df             

'''
中位数停车时长 function：输入一行数据，根据日期、时段、最大停车概率地块业态，赋予出场时间和停车时长
'''   
def median_park_len(row_in):
    #由于apply传参是series，因此需要先转为dataframe
    row_in = pd.DataFrame(data=row_in.values.reshape(1,-1),columns=row_in.index,index=[row_in.name])
    #停入概率最大地块的用地类型(astype(float),之前没有这一步，所以总报错)
    max_block = row_in.iloc[:,12:24].T.astype(float).idxmax().values[0]
    #对照地块信息提取用地类型
    blk_func =  block[block.block_id == max_block].function.values[0]
    #提取停车时长,单位：分钟 
    pkm_row = median_pkm[(median_pkm.function == blk_func) & (median_pkm.is_weekday == row_in.inday_tp.values[0]) & (median_pkm.feng == row_in.inhour_tp.values[0])]
    pkm_len = int(pkm_row.parktime.values[0])
    row_in.time_len = pkm_len
    #出场时间：进场时间+停车时长
    row_in.outtime = row_in.intime + datetime.timedelta(minutes=pkm_len)
    #更新时间进行更新：
    row_in.update_time = tm_mid
    #返回该条数据
    return row_in

'''
定义function，将O点在区域内的OD数据转化为基础结果表的形式
'''
def O_convert_basicTB(row_o):
    #由于apply传参是series，因此需要先转为dataframe
    row_o = pd.DataFrame(data=row_o.values.reshape(1,-1),columns=row_o.index,index=[row_o.name])
    #已知信息：'area_name','car_id','outtime','kk_out','update_time'
    basic_o = pd.DataFrame({'area_name':'sr001','car_id':row_o.hphm.values[0],'outtime':row_o.jgsj.values[0],
                  'kk_out':row_o.dwbh.values[0],'update_time':tm_mid},index=[0])
    #字段名和格式统一起来
    basic_o = basic_o.reindex(columns=basic_df.columns)
    basic_o[['departtime','intime']] = basic_o[['departtime','intime']].apply(pd.to_datetime)
    return basic_o
    


'''
定义function：寻找 L 点，赋予进场信息，进行概率分布。
'''
def in_car_check(row_o):
    #由于apply传参是series，因此需要先转为dataframe
    row_o = pd.DataFrame(data=row_o.values.reshape(1,-1),columns=row_o.index,index=[row_o.name])
    #浪费时间范例：record_mayL = od_back[od_back.hphm.isin(row_o.car_id) & (od_back.jgsj < row_o.outtime.values[0])]
    #寻找该O点的L点:车牌号相同，时间在O之前。
    record_mayL_1 = od_back[od_back.hphm.isin(row_o.car_id)]
    record_mayL = record_mayL_1[record_mayL_1.jgsj < row_o.outtime.values[0]]
    #如果record_mayL为空，说明没有L点，所有字段变为空值
    if len(record_mayL) == 0:
        #所有字段都赋值nan
        row_o.iloc[:,:] = np.nan
    #如果record_mayL不为空，则提取出L点
    else:
        L_poi_se = record_mayL[record_mayL.jgsj == record_mayL.jgsj.max()].iloc[-1]
        L_poi = pd.DataFrame(data=L_poi_se.values.reshape(1,-1),columns=L_poi_se.index,index=[L_poi_se.name])
        #判断L点和O点的关系，从而给不同的行赋值。并添加更新时间
        
        #1.如果L和O在同一点，那么以O点为停车点，进行概率分配。
        if row_o.kk_out.values[0] == L_poi.dwbh.values[0]:
            ##匹配卡口编号到iid
            find_iid = kakou_inside[kakou_inside.dwbh.isin(row_o.kk_out)].iid
            #根据iid找到要分配的地块，
            blk_use = iid_to_blockID_n[iid_to_blockID_n.iid.isin(find_iid)]['block_id']
            #停入的时段和日期类型，由L_poi决定：
            day_tp_L = is_week(pd.to_datetime(L_poi.jgsj.values[0]))
            feng_tp_L = which_feng(pd.to_datetime(L_poi.jgsj.values[0]))
            #提取相应地块的吸引力
            blk_tp = block_attract[(block_attract.is_weekday == day_tp_L) & (block_attract.feng == feng_tp_L)]
            #在此假设用1进行分配
            blk_get = blk_tp[blk_tp.block_id.isin(list(blk_use)[0])]
            #计算分配到各个地块的概率
            prob_distrb = np.array(blk_get.attract / blk_get.attract.sum())
            blk_distrb = list(map(lambda m: str(m),list(blk_get.block_id)))
            carin_df = pd.DataFrame(prob_distrb.reshape(1,-1),columns=blk_distrb)
            carin_df_reindex = carin_df.reindex(columns=basic_df.columns)
            carin_df_reindex.index = row_o.index.values
            
            #向上寻找直到找L的O（Lo）：如果L是D点，那么L有对应的Lo，也就是出发点，如果L是其他点（应该肯定是唯一点），则出发点为nan
            if L_poi.dwlx.values[0] == 'D点':
                last_row = od_back.loc[L_poi.index,]                
                while last_row.dwlx.values[0] != 'O点':
                    try:
                        od_back.loc[last_row.index-1,]
                    except(KeyError):
                        last_row.iloc[:,:] = np.nan
                        break
                    else:
                        last_row = od_back.loc[last_row.index-1,]
                row_o.departtime = last_row.jgsj.values[0]
                row_o.kk_start = last_row.dwbh.values[0]              
            else:
                row_o.departtime = pd.NaT
                row_o.kk_start = np.nan
            #组成完整的输出dataframe
            row_o.intime = L_poi.jgsj.values[0]
            row_o.time_len = (row_o.outtime.values[0] - L_poi.jgsj.values[0]).astype('timedelta64[m]').astype(int)
            row_o.inday_tp = day_tp_L
            row_o.inhour_tp = feng_tp_L
            row_o.kk_end = L_poi.dwbh.values[0]
            row_o[['sb00044','sb00045','sb00091','sb00092','sb00096','sb00100','sb00101','sb00109','sb00110','sb00111','sb00113','sb00114']] = carin_df_reindex[['sb00044','sb00045','sb00091','sb00092','sb00096','sb00100','sb00101','sb00109','sb00110','sb00111','sb00113','sb00114']]
            #
        #2.如果L和O点不是同一点，但是L在区域内，在L和O的共同服务区域内进行分配。
        elif L_poi.dwbh.isin(kakou_inside.dwbh).values:
            #L和O分别匹配卡口编号到iid
            find_iid_O = kakou_inside[kakou_inside.dwbh.isin(row_o.kk_out)].iid
            find_iid_L = kakou_inside[kakou_inside.dwbh.isin(L_poi.dwbh)].iid
            #find_iid_L = kakou_inside[1:2].iid
            #根据iid找到D，N要分配的地块，
            blk_use_O = iid_to_blockID_n[iid_to_blockID_n.iid.isin(find_iid_O)]['block_id'].values[0]
            blk_use_L = iid_to_blockID_n[iid_to_blockID_n.iid.isin(find_iid_L)]['block_id'].values[0]
            blk_use_OL = pd.unique(blk_use_O + blk_use_L)
            #停入的时段和日期类型，由L_poi决定：
            day_tp_L = is_week(pd.to_datetime(L_poi.jgsj.values[0]))
            feng_tp_L = which_feng(pd.to_datetime(L_poi.jgsj.values[0]))
            #提取相应地块的吸引力
            blk_tp = block_attract[(block_attract.is_weekday == day_tp_L) & (block_attract.feng == feng_tp_L)]
            #在此假设用1进行分配
            blk_get = blk_tp[blk_tp.block_id.isin(blk_use_OL)]
            #计算分配到各个地块的概率
            prob_distrb = np.array(blk_get.attract / blk_get.attract.sum())
            blk_distrb = list(map(lambda m: str(m),list(blk_get.block_id)))
            carin_df = pd.DataFrame(prob_distrb.reshape(1,-1),columns=blk_distrb)
            carin_df_reindex = carin_df.reindex(columns=basic_df.columns)
            carin_df_reindex.index = row_o.index.values
            #向上寻找直到找L的O（Lo）：如果L是D点，那么L有对应的Lo，也就是出发点，如果L是其他点（应该肯定是唯一点），则出发点为nan
            if L_poi.dwlx.values[0] == 'D点':
                last_row = od_back.loc[L_poi.index,]                
                while last_row.dwlx.values[0] != 'O点':
                    try:
                        od_back.loc[last_row.index-1,]
                    except(KeyError):
                        last_row.iloc[:,:] = np.nan
                        break
                    else:
                        last_row = od_back.loc[last_row.index-1,]
                row_o.departtime = last_row.jgsj.values[0]
                row_o.kk_start = last_row.dwbh.values[0]              
            else:
                row_o.departtime = pd.NaT
                row_o.kk_start = np.nan
            #组成完整的输出dataframe
            row_o.intime = L_poi.jgsj.values[0]
            row_o.time_len = (row_o.outtime.values[0] - L_poi.jgsj.values[0]).astype('timedelta64[m]').astype(int)
            row_o.inday_tp = day_tp_L
            row_o.inhour_tp = feng_tp_L
            row_o.kk_end = L_poi.dwbh.values[0]
            row_o[['sb00044','sb00045','sb00091','sb00092','sb00096','sb00100','sb00101','sb00109','sb00110','sb00111','sb00113','sb00114']] = carin_df_reindex[['sb00044','sb00045','sb00091','sb00092','sb00096','sb00100','sb00101','sb00109','sb00110','sb00111','sb00113','sb00114']]
             #
        #3.若L是缓冲区内的卡口，那么根据路径概率，计算O应该分配多少，重新在O附近分配.
        elif L_poi.dwbh.isin(kakou_st.dwbh).values:
            L_iid = kakou_st[kakou_st.dwbh.isin(L_poi.dwbh)].iid
            O_iid = kakou_st[kakou_st.dwbh.isin(row_o.kk_out)].iid
            #根据路径概率，判断应该有多少概率用于分配。
            new_prob = road_prob[road_prob.origin.isin(O_iid) & road_prob.destin.isin(L_iid)].freq
            if len(new_prob) == 0:
                #所有字段都赋值nan
                row_o.iloc[:,:] = np.nan
            else:
                prob_re = new_prob.values[0]
                #根据O进行概率分配，概率值都乘以prob_re
                ##匹配卡口编号到iid
                find_iid = kakou_inside[kakou_inside.dwbh.isin(row_o.kk_out)].iid
                #根据iid找到要分配的地块，
                blk_use = iid_to_blockID_n[iid_to_blockID_n.iid.isin(find_iid)]['block_id']
                #停入的时段和日期类型，由L_poi决定：
                day_tp_L = is_week(pd.to_datetime(L_poi.jgsj.values[0]))
                feng_tp_L = which_feng(pd.to_datetime(L_poi.jgsj.values[0]))
                #提取相应地块的吸引力
                blk_tp = block_attract[(block_attract.is_weekday == day_tp_L) & (block_attract.feng == feng_tp_L)]
                #在此假设用1*prob_re进行分配
                blk_get = blk_tp[blk_tp.block_id.isin(list(blk_use)[0])]
                #计算分配到各个地块的概率
                prob_distrb = np.array(blk_get.attract / blk_get.attract.sum()) * prob_re
                blk_distrb = list(map(lambda m: str(m),list(blk_get.block_id)))
                carin_df = pd.DataFrame(prob_distrb.reshape(1,-1),columns=blk_distrb)
                carin_df_reindex = carin_df.reindex(columns=basic_df.columns)
                carin_df_reindex.index = row_o.index.values
                
                #向上寻找直到找L的O（Lo）：如果L是D点，那么L有对应的Lo，也就是出发点，如果L是其他点（应该肯定是唯一点），则出发点为nan
                if L_poi.dwlx.values[0] == 'D点':
                    last_row = od_back.loc[L_poi.index,]                
                    while last_row.dwlx.values[0] != 'O点':
                        try:
                            od_back.loc[last_row.index-1,]
                        except(KeyError):
                            last_row.iloc[:,:] = np.nan
                            break
                        else:
                            last_row = od_back.loc[last_row.index-1,]
                    row_o.departtime = last_row.jgsj.values[0]
                    row_o.kk_start = last_row.dwbh.values[0]              
                else:
                    row_o.departtime = pd.NaT
                    row_o.kk_start = np.nan    
                #组成完整的输出dataframe
                row_o.intime = L_poi.jgsj.values[0]
                row_o.time_len = (row_o.outtime.values[0] - L_poi.jgsj.values[0]).astype('timedelta64[m]').astype(int)
                row_o.inday_tp = day_tp_L
                row_o.inhour_tp = feng_tp_L
                row_o.kk_end = L_poi.dwbh.values[0]
                row_o[['sb00044','sb00045','sb00091','sb00092','sb00096','sb00100','sb00101','sb00109','sb00110','sb00111','sb00113','sb00114']] = carin_df_reindex[['sb00044','sb00045','sb00091','sb00092','sb00096','sb00100','sb00101','sb00109','sb00110','sb00111','sb00113','sb00114']]
                #
        #4.如果L点在远距离卡口，则全部赋值为nan
        else:
            row_o.iloc[:,:] = np.nan  
    return row_o   
    
'''
修改字段名，适配数据库中的表
'''
def Modify_field_name(old_df):
    new_df = old_df.rename(columns={'area_name':'sr_defregion_code','car_id':'sr_carno',
                                    'pk_code':'sr_pl_code','departtime':'sr_car_starttime',
                                    'intime':'sr_car_inregiontime','outtime':'sr_car_outregiontime',
                                    'time_len':'sr_car_duration','inday_tp':'sr_date_type',
                                    'inhour_tp':'sr_timetype','kk_start':'sr_car_defbayonetstart',
                                    'kk_end':'sr_car_defbayonetend','kk_out':'sr_car_defbayonetout',
                                    'sb00044':'sr_stopblock0_rate','sb00045':'sr_stopblock1_rate','sb00091':'sr_stopblock2_rate',
                                    'sb00092':'sr_stopblock3_rate','sb00096':'sr_stopblock4_rate','sb00100':'sr_stopblock5_rate',
                                    'sb00101':'sr_stopblock6_rate','sb00109':'sr_stopblock7_rate','sb00110':'sr_stopblock8_rate',
                                    'sb00111':'sr_stopblock9_rate','sb00113':'sr_stopblock10_rate','sb00114':'sr_stopblock11_rate',
                                    'update_time':'sr_recordin'})
    return new_df


'''
修改字段名，适配算法中的表
'''
def Modify_field_name_back(new_df):
    old_df = new_df.rename(columns={'sr_defregion_code':'area_name','sr_carno':'car_id',
                                    'sr_pl_code':'pk_code','sr_car_starttime':'departtime',
                                    'sr_car_inregiontime':'intime','sr_car_outregiontime':'outtime',
                                    'sr_car_duration':'time_len','sr_date_type':'inday_tp',
                                    'sr_timetype':'inhour_tp','sr_car_defbayonetstart':'kk_start',
                                    'sr_car_defbayonetend':'kk_end','sr_car_defbayonetout':'kk_out',
                                    'sr_stopblock0_rate':'sb00044','sr_stopblock1_rate':'sb00045','sr_stopblock2_rate':'sb00091',
                                    'sr_stopblock3_rate':'sb00092','sr_stopblock4_rate':'sb00096','sr_stopblock5_rate':'sb00100',
                                    'sr_stopblock6_rate':'sb00101','sr_stopblock7_rate':'sb00109','sr_stopblock8_rate':'sb00110',
                                    'sr_stopblock9_rate':'sb00111','sr_stopblock10_rate':'sb00113','sr_stopblock11_rate':'sb00114',
                                    'sr_recordin':'update_time'})
    #提取所需字段
    old_df = old_df[['area_name','car_id','pk_code','departtime','intime','outtime','time_len','inday_tp','inhour_tp','kk_start',
                                 'kk_end','kk_out','sb00044','sb00045','sb00091','sb00092','sb00096','sb00100','sb00101',
                                 'sb00109','sb00110','sb00111','sb00113','sb00114','update_time']]
    return old_df

#多次尝试写出数据库
def retry_write_todb(data,try_limit,table_name):
    attempts = 0
    success = False
    while attempts < try_limit and not success:
        try:
            data.to_sql(name=table_name,con=engine,if_exists='append',index=False)
            success = True
            print(table_name + ' 写入成功')
        except:
            time.sleep(1)
            attempts += 1
            print(table_name + ' 再次尝试写入')
            if attempts == try_limit:
                print(table_name + ' 已经尝试写入多次仍未成功')
                break
            
#多次尝试用sql语句操作数据库函数     
def retry_sql_execute(sql_words,try_limit):
    attempts = 0
    success = False
    while attempts < try_limit and not success:
        try:
            engine.execute(sql_words)
            success = True
            print(sql_words + ' 操作成功')
        except:
            time.sleep(1)
            attempts += 1
            print(sql_words + ' 再次尝试操作')
            if attempts == try_limit:
                print(sql_words + ' 已经尝试操作多次仍未成功')
                break


# 写一个for循环，循环计算一个月的历史数据:3.16 - 4.19。
his_tm_start = pd.to_datetime('2020-04-01')
his_tm_end = pd.to_datetime('2020-04-19')

time_history = []
date_history = []

while his_tm_start <= his_tm_end:
    time_history.append(his_tm_start)
    his_tm_start = his_tm_start + datetime.timedelta(days=1)

date_history = list(map(lambda x: pd.to_datetime(x).strftime('%Y%m%d') ,time_history))

day_num = range(0,len(date_history))

# 循环运行

for day_i in day_num:
    date_roll = date_history[day_i]
    tm_start = time_history[day_i]
    tm_end = tm_start + datetime.timedelta(days=1)
    
    #取车牌颜色的唯一值
    sql_words = "SELECT DISTINCT hpys FROM traffic_base.dws_kkod_dwlx_di"
    hpys_uq = query_pandas(odps_config,sql_words)
    hpys_uq = hpys_uq.values
    #
    kakou_od = pd.DataFrame(columns=[])
    #根据车牌颜色取数据
    for hpys_i in hpys_uq:
        sql_words = "SELECT * from traffic_base.dws_kkod_dwlx_di WHERE (dt = '{}') AND (dwlx in ('O点','D点')) AND (hpys = {})".format(date_roll,hpys_i[0])
        kakou_od_i = query_pandas(odps_config,sql_words)
        kakou_od = kakou_od.append(kakou_od_i,ignore_index=True)
        del kakou_od_i
        gc.collect()
    
    gc.collect()
    '''
    #时间
    tm_middle = tm_start + datetime.timedelta(hours=12)
    #每次读取半天的OD
    sql_words = "SELECT * from traffic_base.dws_kkod_dwlx_di WHERE (dt = '{}') AND (dwlx in ('O点','D点')) AND (jgsh < '{}')".format(date_roll,tm_middle)
    #示例：sql_cmd = "SELECT * FROM srcity_index_parking_defregiondmnd_hour WHERE (sr_defregion_code = 'sr001') AND (sr_stat_time >= TIMESTAMP('{}')) AND (sr_stat_time < TIMESTAMP('{}'))".format(tm_oneweek_ago,tm_start)
    kakou_od = query_pandas(odps_config,sql_words)
    #再读半天OD
    sql_words = "SELECT * from traffic_base.dws_kkod_dwlx_di WHERE dt = '{}' AND dwlx in ('O点','D点') AND jgsh > TIMESTAMP('{}')".format(date_roll,tm_middle)
    #示例：sql_cmd = "SELECT * FROM srcity_index_parking_defregiondmnd_hour WHERE (sr_defregion_code = 'sr001') AND (sr_stat_time >= TIMESTAMP('{}')) AND (sr_stat_time < TIMESTAMP('{}'))".format(tm_oneweek_ago,tm_start)
    kakou_od_2 = query_pandas(odps_config,sql_words)
    #合起来
    kakou_od.append(kakou_od_2)
    '''
    
    '''
    D在区域内的，进行概率分配，输出到数据库
    '''
    #basic_df相当于整个数据库
    #tm_start = pd.to_datetime("2020-03-20 00:00:00")
    #tm_end = pd.to_datetime("2020-03-21 00:00:00")
    
    tm_start = tm_start - datetime.timedelta(hours=1) #定位到昨天23点作为起始
    while tm_start < tm_end:
        #从每天的0点开始出结果
        tm_mid = tm_start + datetime.timedelta(hours=1)
        #苏规院给数据的时候必须有一个update_time，这样才可以不必每次都从全天数据中拆分OD链。
        #读取该小时的、D点、在试点区域的卡口数据
        od = kakou_od[(kakou_od.jgsj >= tm_start) & (kakou_od.jgsj < tm_mid)]
        od_2 = od[od.dwlx == 'D点']
        od_3 = od_2[od_2.dwbh.isin(kakou_inside.dwbh)]
        #从几千万条量级的kakou_od中，提取出和od_3车牌号一样的数据，这样可以减小后续追溯O时候的查找集，提高速度。
        od_relaD = kakou_od[kakou_od.hphm.isin(od_3.hphm)]
        #
        #判断特征日和高峰时段
        day_tp = is_week(tm_mid)
        feng_tp = which_feng(tm_mid)
        '''
        1.首先把这一小时停进来的概率分配。
        '''
        #定义一个function：输入一行数据，输出车牌号、出发时间、进场时间、起始点卡口、终点卡口，停在各个地块的概率、更新时间。
        #这个function的名字：prob_distribution，套用function，进行计算。一小时的数据计算时间：26秒以内。
        #import time
        #start =time.clock()
        car_in = od_3.apply(lambda x: prob_distribution(x),axis=1)
        #end =time.clock()
        #print(end - start)
        car_in_byD = pd.DataFrame()
        for i in car_in.index:
            car_in_byD = car_in_byD.append(car_in[i],ignore_index=True)
            
        #car_in_byD 的时间格式转换，以后到数据库里就不需要了！！
        if len(car_in_byD) != 0:
            car_in_byD.outtime = pd.to_datetime(car_in_byD.outtime)
        #写入数据库
        car_in_byD_todb = Modify_field_name(car_in_byD)
        retry_write_todb(car_in_byD_todb,3,'sr_ft_parkinregion_result')
        #car_in_byD_todb.to_sql(name='sr_ft_parkinregion_result',con=engine,if_exists='append',index=False)
        #basic_df = basic_df.append(car_in_byD,ignore_index=True)  
        
        '''
        2.其次把这一小时进来的，之前进来没出去的都找出来，如果在这个小时有动态，就附上出场信息。
        '''
        #注意：：：：这一步的所有操作都要更新updatetime！！！！！！！特别是函数：out_car_check的结果
        #之所以要更新updatetime是因为该步骤都是针对历史数据的操作。这样可以影响到其他的数据结果的改变。
        #抽取没有出场信息，且区域名称不为nan的数据：
        
        #从数据库读取 未出场数据
        sql_cmd = "SELECT * FROM sr_ft_parkinregion_result WHERE sr_defregion_code='sr001' AND sr_car_outregiontime is null"
        car_not_out = pd.read_sql(sql_cmd,con=engine)
        car_not_out = Modify_field_name_back(car_not_out)
    
        #car_not_out = basic_df[pd.isnull(basic_df.outtime) & (~pd.isnull(basic_df.area_name))]
        
        #1.如果已经在停超过一周，那么直接赋予其出场时间为平均停车时长。
        #2.如果在停车辆在该小时出场，则判断N和D的关系，赋予出场信息。
        #3.如果在停车辆在该小时没出场，则不发生任何改变。
        
        #1.如果已经在停超过一周，那么直接赋予其出场时间为停入时间+平均停车时长。
        if_long = (tm_mid - car_not_out.intime) > datetime.timedelta(days=3)
        #用function:median_park_len.查找到停车概率最大的地块，根据其业态找到平均停车时长.
        car_in_moreThanWeek = car_not_out[if_long]
        car_in_moreThanWeek_change = car_in_moreThanWeek.apply(lambda x: median_park_len(x),axis=1)
        car_out_medianlen = pd.DataFrame()
        for i in car_in_moreThanWeek_change.index:
            car_out_medianlen = car_out_medianlen.append(car_in_moreThanWeek_change[i],ignore_index=False)
        car_not_out[if_long] = car_out_medianlen
        #至此，未出场的数据中，停车时长超过一周的已经完成出场信息赋值。下面提取出还不到一周的，进行出场判断。
        
        #2.如果在停车辆在该小时出场，则判断N和D的关系，赋予出场信息。
        #3.如果在停车辆在该小时没出场，则不发生任何改变。
        not_long = ~if_long
        car_in_lessThanWeek = car_not_out[not_long]
        #为了提高算法效率，先把今日数据，也就是od中，和car_in_lessThanWeek的车牌号相同的筛选出来。
        od_relaO = od[od.hphm.isin(car_in_lessThanWeek.car_id)]
        #紧接着判断该小时内，是否有出场的车，带入out_car_check function。一小时的数据计算时间：148秒以内。
        #import time
        #start =time.clock()
        car_in_lessThanWeek_change = car_in_lessThanWeek.apply(lambda x: out_car_check(x),axis=1)
        #end =time.clock()
        #print(end - start)  
        car_out_thisHour = pd.DataFrame()
        for i in car_in_lessThanWeek_change.index:
            car_out_thisHour = car_out_thisHour.append(car_in_lessThanWeek_change[i],ignore_index=False)
        car_not_out[not_long] = car_out_thisHour #这一步，第二次进行值的替换，导致先前因为超过一周出场的车辆的outtime变为int格式。在此进行修改。
        #outtime全部改为时间格式
        car_not_out['outtime'] = car_not_out.outtime.apply(lambda x:pd.to_datetime(x))       
        #修改字段名
        car_not_out_todb = Modify_field_name(car_not_out)
        #删除目前库里未出场的数据
        sql_words = "DELETE FROM sr_ft_parkinregion_result WHERE sr_defregion_code='sr001' AND sr_car_outregiontime is null"
        retry_sql_execute(sql_words,3)
        #engine.execute("DELETE FROM sr_ft_parkinregion_result WHERE sr_defregion_code='sr001' AND sr_car_outregiontime is null")
        #先把数据中可能存在的格式问题解决。再数据库中增加修改后的数据：
        car_not_out_todb['sr_car_outregiontime'] = car_not_out_todb['sr_car_outregiontime'].apply(lambda x: pd.to_datetime(x))
        car_not_out_todb['sr_defregion_code'][pd.isnull(car_not_out_todb['sr_defregion_code'])] = '00'
        car_not_out_todb['sr_carno'][pd.isnull(car_not_out_todb['sr_carno'])] = '00'
        retry_write_todb(car_not_out_todb,3,'sr_ft_parkinregion_result')
        #car_not_out_todb.to_sql(name='sr_ft_parkinregion_result',con=engine,if_exists='append',index=False)
        '''
        3.最后，把从这一小时出发的数据，追溯其进场信息(最远追溯一天)，输出完整的所有信息。
        '''
        #提取以试点区域为O的数据。相当于先假设这些车是从这出场的
        od_22 = od[od.dwlx == 'O点']
        od_32 = od_22[od_22.dwbh.isin(kakou_inside.dwbh)]
        #1.如果该点的车牌号hphm和时间jgsj刚好和car_not_out的car_id以及outtime相同，说明其进出已经统计过了。把这些点剔除
        od_42 = od_32[~(od_32.hphm.isin(car_not_out.car_id) & od_32.jgsj.isin(car_not_out.outtime))]
        #od_42转化为基础结果表的格式：只有出场信息
        O_poi_basic = od_42.apply(lambda x: O_convert_basicTB(x),axis=1)
        O_poi_basic_thisHour = pd.DataFrame()
        for i in O_poi_basic.index:
            O_poi_basic_thisHour = O_poi_basic_thisHour.append(O_poi_basic[i],ignore_index=True)
        
        #2.追溯这些车的上一个点 L 点的 位置：（1）若是同一点，则直接在O点概率分配。（2）若是区域内点，则在LO附近分配
        #  （3）若是缓冲区内点，则在O附近按照路径概率进行分配。（4）若在远距离卡口，直接删除。
        #相当于可以直接改造：out_car_check，prob_distribution，prob_distribution_DN函数。
        #？？直接从一周数据查询速度会如何？(苏规院要保存好一周前数据，保证可追溯)：先追溯一整天内的。
        
        #读取该小时前一天的数据
        tm_back = tm_mid - datetime.timedelta(days=1)
        od_back = kakou_od[(kakou_od.jgsj > tm_back) & (kakou_od.jgsj < tm_mid)]
        #从前一天的数据中，先提取出和O_poi_basic_thisHour车牌号相关的数据，缩小后续的查询集，提升速度。
        if len(O_poi_basic_thisHour) != 0:
            od_back = od_back[od_back.hphm.isin(O_poi_basic_thisHour.car_id)]
            #定义function：寻找 L 点，赋予进场信息，进行概率分布。运行4.5分钟.
            #注意：有的OD链顺序被打乱，这是由于跨天导致的，最后苏规院提供每小时的数据不能出现这种问题，需要沟通。
            O_poi_basic_thisHour_ininfo = O_poi_basic_thisHour.apply(lambda x: in_car_check(x),axis=1)      
            
            O_poi_allInfo = pd.DataFrame()
            for i in O_poi_basic_thisHour_ininfo.index:
                O_poi_allInfo = O_poi_allInfo.append(O_poi_basic_thisHour_ininfo[i],ignore_index=True)
            #注意：减少在大规模数据中的查询次数能大大节省时间！！共600s降低到245s。将series转化为dataframe节省60%时间。
            #抽取O_poi_allInfo中非NAN的数据，合并到 basic_df（数据库）中
            O_poi_haveData = O_poi_allInfo[O_poi_allInfo.area_name.isin(['sr001'])]
            #写入数据库
            O_poi_haveData_todb = Modify_field_name(O_poi_haveData)
            retry_write_todb(O_poi_haveData_todb,3,'sr_ft_parkinregion_result')
        
            
        tm_start = tm_mid
        
        '''
        数据库 会不断增加数据，直到 tm_start == tm_end
        '''
    

   










