# -*- coding: utf-8 -*-
"""
Created on Sat Jul  4 21:19:37 2020

@author: Boyang_Xia
"""

#输出pr停车场服务范围. 

#正式代码:
import numpy as np
import pandas as pd
import geopandas
import datetime
from sqlalchemy import create_engine

from shapely.geometry import shape
from sklearn.cluster import KMeans
import json
from scipy.spatial import Delaunay

from shapely.geometry import Polygon, mapping
from shapely.geometry import LineString,Point
from shapely.geometry import MultiPolygon
import copy

def construct_gdf_from_df(df:pd.DataFrame, type_fieldname:str, coordinates_fieldname:str, input_crs='+init=epsg:4326'):
    features_list = []
    for index,i in df.iterrows():

        geometry_dict={}

        geometry_dict['geometry'] = {}

        geometry_dict['geometry']['type'] = i[type_fieldname]

        geometry_dict['geometry']['coordinates'] = eval(i[coordinates_fieldname])

        features_list.append(geometry_dict)

    geojson_geometry = {}

    geojson_geometry['features'] = features_list

    new_geom = geopandas.GeoDataFrame()

    for i_feature in geojson_geometry['features']:

        gp_geom = shape(i_feature['geometry'])

        temp_geom = geopandas.GeoDataFrame(index=[0], crs=input_crs, geometry=[gp_geom])

        new_geom = new_geom.append(temp_geom,ignore_index= True)

    gdf = geopandas.GeoDataFrame(df, geometry=new_geom.geometry)

    return gdf

#上个月
#当前时间（每月月初）
day_now = datetime.datetime.date(datetime.datetime.now())
#上一个月
month_last = (day_now - datetime.timedelta(days=25)).strftime('%Y-%m')

    
#区域限制：不能在姑苏区
#次数限制：聚5类，取前三类
#距离限制：站点辐射最远15公里
#数量限制：点位数量大于5个
#

#加载所有匹配好的卡口od数据
#engine = create_engine("mysql+pymysql://root:xby1703@localhost:3306/ecs_transfer_0427?charset=utf8mb4")
engine = create_engine("mysql+pymysql://qftc_01:qftc123@rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn:3306/testdb_suzhou_analysis?charset=utf8mb4")

sql_words = "SELECT * FROM srcity_generate_pr_kk"
pr_range = pd.read_sql(sql_words,engine)
#删除点位编号为空的
pr_range = pr_range[pr_range.dwbh != 'null']

#加载卡口点位
sql_words = "SELECT sr_defbayonet_code,sr_defbayonet_gc,sr_defbayonet_spatialtype FROM sr_ft_bayonetinfo"
kk_loc = pd.read_sql(sql_words,engine)
#生成空间数据
kk_loc = construct_gdf_from_df(kk_loc, 'sr_defbayonet_spatialtype', 'sr_defbayonet_gc', input_crs='+init=epsg:4326')
kk_loc_use = kk_loc[['sr_defbayonet_code','geometry']]
#加载行政区数据
sql_words = "SELECT sr_region_code,sr_region_cname,sr_region_boundarycns,sr_spatial_datatype FROM srcity_bt_regioninfo"
sz_region = pd.read_sql(sql_words,engine)
sz_region = construct_gdf_from_df(sz_region, 'sr_spatial_datatype', 'sr_region_boundarycns', input_crs='+init=epsg:4326')

#去除在姑苏区的卡口点位：
kk_region = geopandas.sjoin(kk_loc,sz_region,how='left',op='intersects')
kk_in_gusu = kk_region[kk_region.sr_region_cname == '姑苏区']

#读取停车场点位
sql_words = "SELECT * FROM srcity_bt_parkinglotinfo WHERE sr_pl_type = 'pr停车场'"
pr_pk = pd.read_sql(sql_words,engine)
pr_pk = construct_gdf_from_df(pr_pk, 'sr_pl_spatialtype', 'sr_pl_gc', input_crs='+init=epsg:4326')
pr_pk = pr_pk[['sr_pl_code','sr_pl_cname','geometry']]
#pr_pk.plot()
#buffer 5千米
pr_pk = pr_pk.to_crs('+init=epsg:32651')
pr_pk_bf = pr_pk.buffer(5000)
pr_pk_bf = pr_pk_bf.to_crs('+init=epsg:4326')
pr_pk['geometry'] = pr_pk_bf

#加载居住人口数据
sql_words = "SELECT sr_pop_pointgc,sr_pop_pointspatialtype,sr_pop_resdntnum FROM srcity_ft_popdstr WHERE sr_stat_month = '2020-04'"
pop = pd.read_sql(sql_words,engine)
pop = construct_gdf_from_df(pop, 'sr_pop_pointspatialtype', 'sr_pop_pointgc', input_crs='+init=epsg:4326')
pop = pop[['sr_pop_resdntnum','geometry']]

'''
##lotcode（停车场编号）, 覆盖面积数值, 覆盖范围边界,覆盖人口数值, month（月份）

'''



#构建function，输入三个点，输出三根线
def poi_to_line(poi_array):
    line1 = LineString(poi_array[[0,1]])
    line2 = LineString(poi_array[[0,2]])
    line3 = LineString(poi_array[[1,2]])
    geomList = []
    geomList.append(line1)
    geomList.append(line2)
    geomList.append(line3)
    geolines = geopandas.GeoDataFrame(geometry = geomList)
    return geolines
    
    

#####多种规则
#调整方向：聚类取前几类，三角网阈值，buffer宽度，距离停车场的距离
    
pk_code = pd.unique(pr_range.lot_code)
range_out = pd.DataFrame()
#pcode = pk_code[2]
for pcode in pk_code:
    pk_kk = pr_range[pr_range.lot_code == pcode]
    pk_kk['cnt'] = 1
    pk_kk = pk_kk.groupby(['dwbh','lot_code'])['cnt'].sum().reset_index()
    #如果数据量小于5，直接跳过
    if len(pk_kk) < 5:
        continue
    #kmeans 聚5类 取前4类
    estimator = KMeans(n_clusters=5)
    estimator.fit(np.array(pk_kk.cnt).reshape(-1, 1))
    pk_kk['label'] = estimator.labels_
    cluster_sort = np.argsort(estimator.cluster_centers_.reshape(1,-1))
    cluster_max3 = cluster_sort[0][1:5]
    pk_kk = pk_kk[pk_kk['label'].isin(cluster_max3)]
    #去掉姑苏区的点位
    pk_kk = pk_kk[~pk_kk.dwbh.isin(list(kk_in_gusu.sr_defbayonet_code))]
    pk_kk_geo = pd.merge(pk_kk,kk_loc_use,how='left',left_on='dwbh',right_on='sr_defbayonet_code')
    pk_kk_geo = geopandas.GeoDataFrame(pk_kk_geo)   
    pk_kk_geo.crs = '+init=epsg:4326'
    ##如果数据量小于4，直接跳过
    if len(pk_kk_geo) < 4:
        continue
    #生成delaynay三角网
    #转为32651
    pk_kk_geo = pk_kk_geo.to_crs('+init=epsg:32651')
    geo_coor = list()
    for i in mapping(pk_kk_geo.geometry)['features']:
        #提取坐标
        coor_raw = list(i['geometry']['coordinates'])
        #
        geo_coor.append(coor_raw)
    
    points = np.array(geo_coor)
    tri = Delaunay(points)    
    #按照三角网画出lines
    points_array = points[tri.simplices]
    delaunay_lines = geopandas.GeoDataFrame()
    
    for i in points_array:    
        delaunay_lines = delaunay_lines.append(poi_to_line(i),ignore_index=True)
    
    #delaunay_lines.plot()
    #删除超过平均值+一个标准差长度的线
    thr = np.mean(delaunay_lines.length) + np.std(delaunay_lines.length)
    #thr = np.mean(delaunay_lines.length)
    #阈值至少8公里
    if thr > 8000:
        thr = 8000
    
    delaunay_lines = delaunay_lines[delaunay_lines.length < thr]
    #若没有剩余的线，跳过
    if len(delaunay_lines) == 0:
        continue
    #delaunay_lines.plot()
    
    del_li = delaunay_lines.buffer(400)
    #del_li.plot()
    del_li_geo = geopandas.GeoDataFrame(geometry=del_li)
    del_li_geo['level'] = '1'
    #del_li_geo.plot()

    del_li_dissolve = del_li_geo.dissolve(by='level')
    
    
    #判断是否为multipolygon,拆解为polygon。循环去hull
    hull_geo_df = geopandas.GeoDataFrame()
    if del_li_dissolve.geom_type.values[0] == 'MultiPolygon':
        new_feature = mapping(del_li_dissolve.geometry)['features'][0]
        for f_i in range(0,len(new_feature['geometry']['coordinates'])):
            coor_rmhull = new_feature['geometry']['coordinates'][f_i][0]
            poly_i = Polygon(coor_rmhull)
            hull_geo_df_i = geopandas.GeoDataFrame({'id':f_i},geometry=[poly_i],index=[0])
            hull_geo_df = hull_geo_df.append(hull_geo_df_i,ignore_index=True)            
    else:
        new_feature = mapping(del_li_dissolve.geometry)['features'][0]
        coor_rmhull = mapping(del_li_dissolve.geometry)['features'][0]['geometry']['coordinates'][0]
        poly_i = Polygon(coor_rmhull)
        hull_geo_df_i = geopandas.GeoDataFrame({'id':0},geometry=[poly_i],index=[0])
        hull_geo_df = hull_geo_df.append(hull_geo_df_i,ignore_index=True)
     
    #       
    hull_geo_df.crs = '+init=epsg:32651'
    hull_geo_df = hull_geo_df.to_crs('+init=epsg:4326')
    
    
    #距离停车场小于5公里
    pr_pk_i = pr_pk[pr_pk['sr_pl_code'] == pcode]
    hull_pk = geopandas.sjoin(hull_geo_df,pr_pk_i,how='left',op='intersects')
    hull_pk_inrange = hull_pk[~hull_pk.sr_pl_code.isnull()]
    #hull_pk_inrange.plot()
    #如果没有在附近的区域，跳过
    if len(hull_pk_inrange) == 0:
        continue
    #如果有多个区域，合并到一起成为multipolygon
    if len(hull_pk_inrange) > 1:
        mulp = MultiPolygon(list(hull_pk_inrange.geometry))
        hull_pk_inrange = hull_pk_inrange.iloc[0:1,]
        hull_pk_inrange['geometry'] = [mulp]
        #hull_pk_inrange.plot()
    
    #计算相关指标，输出完整的数据
    hull_pk_inrange['lotcode'] = pcode
    #面积
    hull_pk_inrange.crs = '+init=epsg:4326'
    hull_pk_inrange['range_area'] = round((hull_pk_inrange.to_crs('+init=epsg:32651').area / 1000000),2)
    #人口
    hull_pk_inrange.drop(columns='index_right',inplace=True)
    pop_i = geopandas.sjoin(pop,hull_pk_inrange,how='left',op='intersects')
    hull_pk_inrange['range_pop'] = round((pop_i[~pop_i.lotcode.isnull()]['sr_pop_resdntnum'].sum() / 10000) , 2)
    #坐标和类型
    hull_pk_inrange['geojson'] = hull_pk_inrange.geometry.apply(lambda x: geopandas.GeoSeries(x).to_json())
    hull_pk_inrange['range_coord'] = hull_pk_inrange.geojson.apply(lambda x:str(json.loads(x)['features'][0]['geometry']['coordinates']))
    hull_pk_inrange['range_spatialtype'] = hull_pk_inrange.geojson.apply(lambda x:str(json.loads(x)['features'][0]['geometry']['type']))
    #
    
    hull_pk_inrange['sr_stat_month'] = month_last
    hull_pk_inrange = hull_pk_inrange[['lotcode','range_area','range_pop','range_spatialtype','range_coord','geometry','sr_stat_month']]
    
    range_out = range_out.append(hull_pk_inrange,ignore_index=True)

    
   
#整体 union
range_out_union = copy.deepcopy(range_out.iloc[0:1,])
range_out_union['geometry'] = [range_out.unary_union]
range_out_union['lotcode'] = 'all'
range_out_union['range_area'] = np.sum(range_out.range_area)
range_out_union['range_pop'] = np.sum(range_out.range_pop)
#坐标和类型
range_out_union['geojson'] = range_out_union.geometry.apply(lambda x: geopandas.GeoSeries(x).to_json())
range_out_union['range_coord'] = range_out_union.geojson.apply(lambda x:str(json.loads(x)['features'][0]['geometry']['coordinates']))
range_out_union['range_spatialtype'] = range_out_union.geojson.apply(lambda x:str(json.loads(x)['features'][0]['geometry']['type']))
del range_out_union['geojson'] 
range_out = range_out.append(range_out_union,ignore_index=True)


#range_out.iloc[2:3,].plot()
#range_out.plot()
'''
range_out = geopandas.GeoDataFrame(range_out)
range_out.to_file('D:/boyang_xia/pr_range_out_0705.shp',encoding='UTF-8')
'''
range_out_nogeo = range_out.drop(columns={'geometry'})
    
range_out_nogeo.to_sql(name='srcity_generate_pr_range',con=engine,if_exists='append',index=False)









