# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 15:23:40 2020

@author: levil

output_table:srcity_index_parking_defregiontimedist_month
"""

import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
import datetime
from sqlalchemy import create_engine
from odps import ODPS
from sqlalchemy_tomysql import retry_write_todb

def query_pandas(odps_config, sql):
        od = ODPS(odps_config.get("access_id"),odps_config.get("access_key"), odps_config.get("project"),
            endpoint=odps_config.get("endpoint"))
        with od.execute_sql(sql).open_reader() as reader:
            data = reader
        print('query df from odps successfully !!!')
        return data.to_pandas()

def to_min(x):
    return x.hour * 60 + x.minute
    
if __name__ == "__main__":
    #读取近90天的停车进出场数据
    region_code = 'sr003'
    odps_config = {
                    "access_id": "V6MeotKzDBgOv6vI",
                    "access_key": "P7YzxeRMfdr43e22C55ioOmsJP2UNu",
                    "project": "csdnsz_dma",
                    "endpoint": "http://service.cn-suzhou-szdn-d01.odps.cloud-ops.szdn.suzhou.gov.cn/api"
                    }
    this_month = str(datetime.datetime.now()- datetime.timedelta(days=15))[:11]+'00:00:00'
    pre_3month =  str(datetime.datetime.now() - datetime.timedelta(days=105))[:11]+'00:00:00'
    conn = create_engine("mysql+pymysql://qftc_01:qftc123@rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn:3306/testdb_suzhou_analysis?charset=utf8mb4")
     
    output_df = pd.read_sql("select * from srcity_index_parking_defregiontimedist_month",conn)
    output_df = pd.DataFrame(columns=output_df.columns)
    sql_command_0 = 'select sr_carno as carid,sr_car_inregiontime as in_time,sr_car_outregiontime as out_time,sr_car_duration as park_time,sr_defregion_code as parkcode from sr_ft_parkinregion_result where sr_car_inregiontime between timestamp("{pre_3month}") and timestamp("{this_month}") and sr_car_inregiontime is not null and sr_car_outregiontime is not null and sr_car_duration > 0  and sr_car_duration < 1440 and sr_defregion_code = "{region_code}"'
    sql_para_dict = {'pre_3month':pre_3month,
                     'this_month':this_month,
                     'region_code':region_code
                     }
    sql_command = sql_command_0.format(**sql_para_dict)
    raw_df = pd.read_sql(sql_command,conn)
    datetype_df = query_pandas(odps_config,
                               'SELECT * FROM traffic_base.dim_date_base where date_list <= "{}" and date_list >= "{}"'.format(this_month[:10],pre_3month[:10]))
    datetype_df['dt'] = datetype_df['dt'].astype('int')
    datetype_df['date_type'] = datetype_df['date_type'].astype('int')
    raw_df['in_day'] = raw_df['in_time'].apply(lambda x:int(str(x)[:11].replace('-','')))
    df = pd.merge(left=raw_df, right=datetype_df, how='left', left_on='in_day', right_on='dt')
    df = df[['carid', 'in_time', 'out_time', 'park_time', 'parkcode','date_type']]
    date_type_dict ={1:'工作日日均' ,2:'双休日日均',3:'节假日日均'}
    for date_type_i in [1,2,3]:
        df_i = df[df['date_type']==date_type_i].reset_index()
        if len(df_i) == 0:
            continue
        #聚类
        df_getfield = pd.DataFrame(df_i,columns=['carid','parkcode','in_time','out_time','park_time','in_time_min','out_time_min'])
        df_getfield['park_time'] = df_getfield['park_time'].astype('int')
        # 新增两列将进出时刻转为百分制的累计分钟数
        df_getfield['in_time_min'] = df_getfield['in_time'].apply(lambda x:to_min(x))
        df_getfield['out_time_min'] = df_getfield['out_time'].apply(lambda x:to_min(x))    
        X = df_getfield[['in_time_min','park_time']]  
        kmeans = KMeans(n_clusters=5, random_state=0).fit(X)
        y_pred = kmeans.predict(X)
        df_getfield['y_pred'] = y_pred
        
        # 统计各类的停车特征  
        df_quant_125 = df_getfield.groupby('y_pred')[['in_time_min']].quantile(0.125).reset_index()
        df_quant_875 = df_getfield.groupby('y_pred')[['park_time']].quantile(0.875).reset_index()
        df_quant_125.columns = ['y_pred','in_time_min_125']
        df_quant_875.columns = ['y_pred','park_time_875']
        #取进场时间前12.5%-出场时间87.5%间的样本
        for quant_df in [df_quant_125,df_quant_875]:
            df_getfield = df_getfield.merge(how='left',right=quant_df,on='y_pred')
        df_getfield = df_getfield[df_getfield['in_time_min']>=df_getfield['in_time_min_125']]
        df_getfield = df_getfield[df_getfield['park_time']<=df_getfield['park_time_875']].reset_index()
        df_mean = df_getfield.groupby('y_pred')[['park_time']].mean()
        df_count = df_getfield.groupby('y_pred')[['carid']].count()        
        df_analysis = df_mean.join(df_quant_125['in_time_min_125']/60).join(df_quant_875['park_time_875']/60).join(df_count).reset_index()    
        #将跨日的出库时间调整为24小时制
        for index,row in df_analysis.iterrows():
            if row['in_time_min_125']+row['park_time_875']>=24:
                df_analysis.loc[index,'out_time_min'] = row['in_time_min_125']+row['park_time_875']-24
            else:
                df_analysis.loc[index,'out_time_min'] = row['in_time_min_125']+row['park_time_875']
        df_analysis = df_analysis[['y_pred', 'park_time', 'in_time_min_125', 'out_time_min', 'carid']]
        df_analysis.columns = ['cluster','park_time','in_time_min','out_time_min','count']
        # 八分位线筛出样本的75%。每一类的比例都除以0.75，总量扩成100%
        df_analysis['proportion'] = (df_analysis['count'] * 100/ len(X)) / (0.75)
        df_analysis['park_time'] = df_analysis['park_time'].apply(lambda x:np.round(x,0))
        df_analysis['in_time_min'] = df_analysis['in_time_min'].apply(lambda x:datetime.time(int(np.round(x,0)),0,0))
        df_analysis['out_time_min'] = df_analysis['out_time_min'].apply(lambda x:datetime.time(int(np.round(x,0)),0,0))
        df_analysis['proportion'] = df_analysis['proportion'].apply(lambda x:np.round(x,2))
        df_analysis = df_analysis.sort_values(by='in_time_min',ascending=True).reset_index()
        df_analysis['cum_proportion'] = 100 - df_analysis.proportion.cumsum()
        df_analysis.loc[4,'cum_proportion'] = 0 #防止上一行语句出现的误差(还没完全到0或略微小于0)
        df_analysis['cum_proportion'] = df_analysis['cum_proportion'].apply(lambda x:np.round(x,2))
        ###########前端绘图需要用的字段内容###########
        temp_result_list = []
        for index,row in df_analysis.iterrows():
            if row['in_time_min'] < row['out_time_min']:
                temp_dict1={}
                if index == 0:
                    temp_dict1["x1"] = row['in_time_min'].hour
                    temp_dict1["y1"] = 100
                    temp_dict1["x2"] = row['out_time_min'].hour
                    temp_dict1["y2"] = row['cum_proportion']
                else:
                    temp_dict1["x1"] = row['in_time_min'].hour
                    temp_dict1["y1"] = df_analysis.loc[index-1,'cum_proportion']
                    temp_dict1["x2"] = row['out_time_min'].hour
                    temp_dict1["y2"] = row['cum_proportion']
                temp_dict1_str = str(temp_dict1)
                temp_result_list.append(temp_dict1_str)
            #如果是跨天的时段类型
            else:
                temp_dict1={}
                temp_dict2={}
                if index == 0:
                    temp_dict1["x1"] = 0
                    temp_dict1["y1"] = 100
                    temp_dict1["x2"] = row['out_time_min'].hour
                    temp_dict1["y2"] = row['cum_proportion']
                    temp_dict2["x1"] = row['in_time_min'].hour
                    temp_dict2["y1"] = 100
                    temp_dict2["x2"] = 24
                    temp_dict2["y2"] = row['cum_proportion']
                    temp_dict1_str = str(temp_dict1)
                    temp_dict2_str = str(temp_dict2)
                    temp_result_list.append(temp_dict1_str)
                    temp_result_list.append(temp_dict2_str)
                else:
                    temp_dict1["x1"] = 0
                    temp_dict1["y1"] = df_analysis.loc[index-1,'cum_proportion']
                    temp_dict1["x2"] = row['out_time_min'].hour
                    temp_dict1["y2"] = row['cum_proportion']
                    temp_dict2["x1"] = row['in_time_min'].hour
                    temp_dict2["y1"] = df_analysis.loc[index-1,'cum_proportion']
                    temp_dict2["x2"] = 24
                    temp_dict2["y2"] = row['cum_proportion']
                    temp_dict1_str = str(temp_dict1)
                    temp_dict2_str = str(temp_dict2)
                    temp_result_list.append(temp_dict1_str)
                    temp_result_list.append(temp_dict2_str)
        ###########前端绘图需要用的字段内容###########

        output_df['sr_defregion_parkin'] = df_analysis['in_time_min']
        output_df['sr_defregion_parkout'] = df_analysis['out_time_min']
        output_df['sr_defregion_parkstop'] = df_analysis['park_time']                    
        output_df['sr_defregion_code'] = region_code
        output_df['sr_stat_year'] = this_month[:4]
        output_df['sr_stat_month'] = this_month[:7]
        output_df['sr_date_type'] = date_type_dict[date_type_i]
        output_df['sr_defregion_comecarrate_havgm'] = df_analysis['proportion']
        sr_defregion_result_list = [eval(i) for i in temp_result_list]
        output_df['sr_defregion_result'] = str(sr_defregion_result_list).replace("'",'"')
        output_df['id'] = 0
        output_df['id'] = output_df['id'].astype('int')
        output_df[['sr_defregion_parkstop','sr_defregion_comecarrate_havgm']] = output_df[['sr_defregion_parkstop','sr_defregion_comecarrate_havgm']].astype('float')
        output_df[['sr_insertedby', 'sr_updatedby']] = '0'
        output_df[['sr_insertedat','sr_updatedat']] = datetime.datetime.now()
        
        retry_write_todb(output_df, conn, 2, 'srcity_index_parking_defregiontimedist_month')
    conn.dispose()
