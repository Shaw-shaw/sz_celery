# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 18:11:39 2020

@author: Boyang_Xia
"""

import numpy as np
import pandas as pd
import datetime
from sqlalchemy import create_engine
'''
#在中间结果算完之后运行，代码的入参：当前时间。计算前一天的24小时数据——算24次。
#这24小时的第一小时（0点）作为tm_start，这一天的结束（23点）作为tm_end
day_use = datetime.datetime.now() - datetime.timedelta(days=1)
tm_start = pd.to_datetime(day_use.strftime('%Y-%m-%d'))
tm_end = tm_start + datetime.timedelta(days=1) - datetime.timedelta(hours=1)
'''
#力宇输出的每小时排队车辆和累计排队车辆 保留不动，其余进行修改。

#RDS
engine = create_engine("mysql+pymysql://qftc_01:qftc123@rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn:3306/testdb_suzhou_analysis?charset=utf8mb4")

#从城市大脑加载日期类型表
from odps import ODPS
#from odps import ODPS,options
#from odps.df import DataFrame

odps_config = {
    "access_id": "V6MeotKzDBgOv6vI",
    "access_key": "P7YzxeRMfdr43e22C55ioOmsJP2UNu",
    "project": "csdnsz_dma",
    "endpoint": "http://service.cn-suzhou-szdn-d01.odps.cloud-ops.szdn.suzhou.gov.cn/api"
}


def query_pandas(odps_config, sql):
        od = ODPS(odps_config.get("access_id"),odps_config.get("access_key"), odps_config.get("project"),
            endpoint=odps_config.get("endpoint"))
        with od.execute_sql(sql).open_reader() as reader:
            data = reader
        return data.to_pandas()
    
date_base = query_pandas(odps_config,'select * from traffic_base.dim_date_base')


# date_base 是大脑里的日期类型对照表

#字段名称修改函数
def Modify_field_name_back(new_df):
    old_df = new_df.rename(columns={'sr_defregion_code':'sr_defregion_code','sr_carno':'car_id',
                                    'sr_pl_code':'pk_code','sr_car_starttime':'departtime',
                                    'sr_car_inregiontime':'intime','sr_car_outregiontime':'outtime',
                                    'sr_car_duration':'time_len','sr_date_type':'inday_tp',
                                    'sr_timetype':'inhour_tp','sr_car_defbayonetstart':'kk_start',
                                    'sr_car_defbayonetend':'kk_end','sr_car_defbayonetout':'kk_out',
                                    'sr_stopblock0_rate':'sb00044','sr_stopblock1_rate':'sb00045','sr_stopblock2_rate':'sb00091',
                                    'sr_stopblock3_rate':'sb00092','sr_stopblock4_rate':'sb00096','sr_stopblock5_rate':'sb00100',
                                    'sr_stopblock6_rate':'sb00101','sr_stopblock7_rate':'sb00109','sr_stopblock8_rate':'sb00110',
                                    'sr_stopblock9_rate':'sb00111','sr_stopblock10_rate':'sb00113','sr_stopblock11_rate':'sb00114',
                                    'sr_recordin':'update_time'})
    #提取所需字段
    old_df = old_df[['sr_defregion_code','car_id','pk_code','departtime','intime','outtime','time_len','inday_tp','inhour_tp','kk_start',
                                 'kk_end','kk_out','sb00044','sb00045','sb00091','sb00092','sb00096','sb00100','sb00101',
                                 'sb00109','sb00110','sb00111','sb00113','sb00114','update_time']]
    return old_df

#多次尝试写出数据库
def retry_write_todb(data,try_limit,table_name):
    attempts = 0
    success = False
    while attempts < try_limit and not success:
        try:
            data.to_sql(name=table_name,con=engine,if_exists='append',index=False)
            success = True
            print(table_name + ' 写入成功')
        except:
            time.sleep(1)
            attempts += 1
            print(table_name + ' 再次尝试写入')
            if attempts == try_limit:
                print(table_name + ' 已经尝试写入多次仍未成功')
                break
            
#多次尝试用sql语句操作数据库函数     
def retry_sql_execute(sql_words,try_limit):
    attempts = 0
    success = False
    while attempts < try_limit and not success:
        try:
            engine.execute(sql_words)
            success = True
            print(sql_words + ' 操作成功')
        except:
            time.sleep(1)
            attempts += 1
            print(sql_words + ' 再次尝试操作')
            if attempts == try_limit:
                print(sql_words + ' 已经尝试操作多次仍未成功')
                break

# basic_df.columns

#从RDS数据库中读取 区域信息，提取总泊位数
sql_cmd = "SELECT * FROM srcity_bt_defregioninfo WHERE sr_defregion_code = 'sr001'"
region_info = pd.read_sql(sql_cmd,con=engine)
berth_all = region_info.sr_defregion_parkingspotnum.values[0]

#从RDS数据库中读取 地块信息，提取总泊位数
sql_cmd = "SELECT * FROM srcity_bt_defblockinfo WHERE sr_defregion_code = 'sr001'"
block_info = pd.read_sql(sql_cmd,con=engine)

#在上线的时候：area_results_table 应该是选择近一周的结果，改完后直接把这一周结果替换掉即可。

#根据updatetime提取每小时的数据。首先修正历史数据，然后更新新的数据。
#tm_start = pd.to_datetime('2020-03-18 00:00:00')
#tm_end = pd.to_datetime('2020-03-18 23:00:00')


# 写一个for循环，循环计算一个月的历史数据:3.16 - 4.19。
his_tm_start = pd.to_datetime('2020-04-01')
his_tm_end = pd.to_datetime('2020-04-19')

time_history = []
date_history = []

while his_tm_start <= his_tm_end:
    time_history.append(his_tm_start)
    his_tm_start = his_tm_start + datetime.timedelta(days=1)

date_history = list(map(lambda x: pd.to_datetime(x).strftime('%Y%m%d') ,time_history))

day_num = range(0,len(date_history))

# 循环运行
for day_i in day_num:
    #date_roll = date_history[day_i]
    tm_start = time_history[day_i]
    tm_end = tm_start + datetime.timedelta(days=1) - datetime.timedelta(hours=1) 
        
    #循环修正历史数据
    import time
    start =time.clock()
    
    while tm_start <= tm_end:
        #upd_data = basic_df[basic_df.update_time == tm_start]
        #upd_data 中包括了：
        #1.本小时作为D点停入的数据。策略：直接算作停入车辆，是否在停看其是否在本小时出场，若没出场算在停。
        #2.之前进入和出场的假僵尸车（分配概率不变）。策略：在其出场之后的小时中，减去相应的概率值。（最远追溯一周）
        #3.以前在停，本小时出场的数据（出场点可能会导致区域、地块概率分配的变化，比如说概率重新分配了，
        #  但是丢失了之前旧的概率分配结果，所以就不知道该加减多少了。）。策略：重算整个一周前的指标。
        #4.以前没有算在停，本小时出场的数据。策略：修改相应历史指标。（最远追溯一周）
        #总结：最优策略就是直接把上一周的数据拿出来重算一遍：停入车辆数、周转率、在停车辆数、停车饱和度。
        #策略：根据当前小时，提取前一周入场的所有数据，计算每小时的指标。
        
        
        #直接提取前一周入场和出场的所有数据：最早一天的数据必须是完整一天的数据（最终判断高峰小时需要完整一天的数据）。
        tm_oneweek_ago = tm_start - datetime.timedelta(days=7)
        tm_oneweek_ago = pd.to_datetime(tm_oneweek_ago.strftime('%Y-%m-%d')) - datetime.timedelta(hours=1)
        
        #从RDS通过sr_stat_time 提取出一周前至今的area_results_table
        sql_cmd = "SELECT * FROM srcity_index_parking_defregiondmnd_hour WHERE (sr_defregion_code = 'sr001') AND (sr_stat_time >= TIMESTAMP('{}')) AND (sr_stat_time < TIMESTAMP('{}'))".format(tm_oneweek_ago,tm_start)
        #sql_cmd = "SELECT * FROM sr_ft_parkinregion_result WHERE (sr_defregion_code = 'sr001') AND (sr_car_starttime >= DATETIME('{}')) AND (sr_updatedat < DATETIME('{}'))".format(tm_oneweek_ago,tm_start)
        area_results_table_oneWeek = pd.read_sql(sql_cmd,con=engine)
        #去掉没有用的自增字段,以及不在本脚本输出的字段
        area_results_table_oneWeek.drop(columns=['id','sr_insertedby','sr_insertedat','sr_updatedby','sr_updatedat'],inplace=True)
        
        #从RDS通过sr_stat_time 提取出一周前至今的 block_results_table
        sql_cmd = "SELECT * FROM srcity_index_parking_defblockdmnd_hour WHERE (sr_defregion_code = 'sr001') AND (sr_stat_time >= TIMESTAMP('{}')) AND (sr_stat_time < TIMESTAMP('{}'))".format(tm_oneweek_ago,tm_start)
        block_results_table_oneWeek = pd.read_sql(sql_cmd,con=engine)
        #去掉没有用的自增字段,以及不在本脚本输出的字段
        block_results_table_oneWeek.drop(columns=['id','sr_insertedby','sr_insertedat','sr_updatedby','sr_updatedat'],inplace=True)
        #area_results_table_oneWeek = area_results_table[(area_results_table.sr_stat_time >= tm_oneweek_ago) & (area_results_table.sr_stat_time < tm_start)]
        
        #通过 intime 提取出所有的一周前至今的入场数据
        sql_cmd = "SELECT * FROM sr_ft_parkinregion_result WHERE (sr_defregion_code = 'sr001') AND (sr_car_inregiontime >= TIMESTAMP('{}')) AND (sr_car_inregiontime < TIMESTAMP('{}'))".format(tm_oneweek_ago,tm_start)
        in_oneweek_tonow = pd.read_sql(sql_cmd,con=engine)
        in_oneweek_tonow = Modify_field_name_back(in_oneweek_tonow)
        #in_oneweek_tonow = basic_df[(basic_df.intime >= tm_oneweek_ago) & (basic_df.intime < tm_start)]
        #通过 outtime 提取出所有一周前至今的出场数据
        sql_cmd = "SELECT * FROM sr_ft_parkinregion_result WHERE (sr_defregion_code = 'sr001') AND (sr_car_outregiontime >= TIMESTAMP('{}')) AND (sr_car_outregiontime < TIMESTAMP('{}'))".format(tm_oneweek_ago,tm_start)
        out_oneweek_tonow = pd.read_sql(sql_cmd,con=engine)
        out_oneweek_tonow = Modify_field_name_back(out_oneweek_tonow)
        #out_oneweek_tonow = basic_df[(basic_df.outtime >= tm_oneweek_ago) & (basic_df.outtime < tm_start)]
        
        tm_roll = tm_oneweek_ago
        while tm_roll < tm_start:
            '''
            #计算区域结果：
            '''
            #时间（每小时更新一次）、区域名称、区域总泊位数、停入车辆数、周转率、在停车辆数、停车饱和度、是否高峰时段、更新时间
            tm_roll_addone = tm_roll + datetime.timedelta(hours=1)
            in_thishour = in_oneweek_tonow[(in_oneweek_tonow.intime >= tm_roll) & (in_oneweek_tonow.intime < tm_roll_addone)]
            #总泊位数，berth_all,最开始已经定义好
            #停入车辆数：所有概率加到一起。
            #地块停入车辆数：np.sum(in_thishour_prob,axis=0)
            in_thishour_prob = in_thishour.loc[:,'sb00044':'sb00114']
            parked_num = int(sum(np.sum(in_thishour_prob,axis=0)))        
            #周转率
            sr_defregion_aparkturnover = (parked_num / berth_all)*100
            #在停车辆数：在上一个小时结果基础上，增加该小时入场车辆，减去该小时出场车辆。
            #上一小时的在停车辆：
            data_start = area_results_table_oneWeek[area_results_table_oneWeek.sr_stat_time  == tm_roll]
            if len(data_start)==0:
                carin_start = 0
            else:
                carin_start = area_results_table_oneWeek[area_results_table_oneWeek.sr_stat_time  == tm_roll].sr_defregion_ainparkcar.values[0]
            #本小时停入的车辆数：parked_num
            #本小时出场的车辆数：所有概率加到一起
            out_thishour = out_oneweek_tonow[(out_oneweek_tonow.outtime >= tm_roll) & (out_oneweek_tonow.outtime < tm_roll_addone)]
            out_thishour_prob = out_thishour.loc[:,'sb00044':'sb00114']
            out_num = int(sum(np.sum(out_thishour_prob,axis=0)))
            #在停车辆数为：
            sr_defregion_ainparkcar = carin_start + parked_num - out_num
            #饱和度
            sr_defregion_aparksat = (sr_defregion_ainparkcar / berth_all)*100
            #区域剩余泊位数
            sr_defregion_rnousespace = berth_all - sr_defregion_ainparkcar
            #日期类型
            date_tp = date_base[date_base.date_list == tm_roll_addone.strftime('%Y-%m-%d')].date_type.values[0]
            if date_tp == '1':
                date_tp_nm = '工作日'
            elif date_tp == '2':
                date_tp_nm = '双休日'
            else:
                date_tp_nm = '节假日'
            #排队车辆数，如果原表中有，就用原表的数据，如果没有，就用0。
            if len(area_results_table_oneWeek[area_results_table_oneWeek.sr_stat_time  == tm_roll_addone].sr_defregion_aquecar.values) == 0:
                aquecar = 0
            else:
                aquecar = area_results_table_oneWeek[area_results_table_oneWeek.sr_stat_time  == tm_roll_addone].sr_defregion_aquecar.values[0]
            '''
            #计算该小时的地块结果：
            '''
            block_hour_results = pd.DataFrame({})
            all_blk = in_thishour_prob.columns
            for blk_n in all_blk:
                #该地块停入车辆数
                parked_num_blk = int(np.sum(in_thishour_prob[blk_n]))
                #周转率
                berth_blk_all= block_info['sr_defblock_parkingspotnum'][block_info['sr_defblock_code'] == blk_n].values[0]
                sr_defblock_aparkturnover = (parked_num_blk / berth_blk_all)*100
                #在停车辆数：在上一个小时结果基础上，增加该小时入场车辆，减去该小时出场车辆。
                #上一小时该地块的在停车辆：
                block_data_start = block_results_table_oneWeek[block_results_table_oneWeek.sr_stat_time  == tm_roll]
                block_n_data_start = block_data_start[block_data_start.sr_defblock_code == blk_n]
                if len(block_n_data_start)==0:
                    blk_n_carin_start = 0
                else:
                    blk_n_carin_start = block_n_data_start.sr_defblock_ainparkcar.values[0]
                #本小时停入的车辆数：parked_num_blk
                #本小时出场的车辆数：
                out_num_blk = int(np.sum(out_thishour_prob[blk_n]))
                #在停车辆数为：
                sr_defblock_ainparkcar = blk_n_carin_start + parked_num_blk - out_num_blk
                #饱和度
                sr_defblock_aparksat = (sr_defblock_ainparkcar / berth_blk_all)*100
                #构造dataframe
                block_n_results = pd.DataFrame({'sr_defblock_code':blk_n,'sr_stat_year':tm_roll_addone.strftime('%Y'),
                                                'sr_stat_month':tm_roll_addone.strftime('%Y-%m'),'sr_stat_date':tm_roll_addone.strftime('%Y-%m-%d'),'sr_stat_time':tm_roll_addone,
                                                'sr_date_type':date_tp_nm,'sr_stat_hour':int(tm_roll_addone.strftime('%H')),
                                                'sr_defblock_ainparkcar':sr_defblock_ainparkcar,'sr_defblock_aparksat':sr_defblock_aparksat,
                                                'sr_defblock_aparkturnover':sr_defblock_aparkturnover,'sr_defregion_rushhour':'0',
                                                'sr_defregion_code':'sr001'},index=[0])
                #所有地块append
                block_hour_results = block_hour_results.append(block_n_results,ignore_index=True)
                
            #当前地块最高饱和度（百分比） sr_defregion_rblockmaxsat
            sr_defregion_rblockmaxsat = max(block_hour_results.sr_defblock_aparksat)
            #sr_defregion_rparknoindex 区域停车不平衡度指数,区域饱和度 和 最高地块饱和度的 差异越大，不平衡度越高。
            sat_abs = abs(sr_defregion_aparksat - sr_defregion_rblockmaxsat)
            #如果饱和度之差，小于10%，则为低，系数在4以内。差10-30.则为中，系数4-7。差30-60，则为高，系数7-9.9。差60以上，系数为10。
            if sat_abs < 10:
                sr_defregion_rparknoindex = round((sat_abs / 10) * 4, 1)
                sr_defregion_rparknograde = '低'
            elif sat_abs < 30:
                sr_defregion_rparknoindex = round(4+(sat_abs / 30) * 3, 1)
                sr_defregion_rparknograde = '中'
            elif sat_abs < 60:
                sr_defregion_rparknoindex = round(7+(sat_abs / 60) * 2.9, 1)
                sr_defregion_rparknograde = '高'
            else:
                sr_defregion_rparknoindex = 10
                sr_defregion_rparknograde = '高'
    
    
            #在原位置进行修改，如果没有原位置，则直接在之后新建一个
            hour_results = pd.DataFrame({'sr_defregion_code':'sr001','sr_stat_year':tm_roll_addone.strftime('%Y'),
                                         'sr_stat_month':tm_roll_addone.strftime('%Y-%m'),'sr_stat_date':tm_roll_addone.strftime('%Y-%m-%d'),'sr_stat_time':tm_roll_addone,
                                         'sr_date_type':date_tp_nm,'sr_stat_hour':int(tm_roll_addone.strftime('%H')),
                                         'sr_defregion_aparkconindex':0,'sr_defregion_aparkcongrade':'低',
                                         'sr_defregion_acontime':0,'sr_defregion_dgaquecar':0,
                                         'sr_defregion_amaxsat':0,
                                         'sr_defregion_rparknoindex':sr_defregion_rparknoindex,'sr_defregion_rparknograde':sr_defregion_rparknograde,
                                         'sr_defregion_rnousespace':sr_defregion_rnousespace,'sr_defregion_rblockmaxsat':sr_defregion_rblockmaxsat,
                                         'sr_defregion_ainparkcar':sr_defregion_ainparkcar,'sr_defregion_aparksat':sr_defregion_aparksat,
                                         'sr_defregion_aquecar':aquecar,
                                         'sr_defregion_aparkturnover':sr_defregion_aparkturnover,'sr_defregion_rushhour':'0'
                                         },index=[0])
    
            #区域：判断是否有原位置,如果没有，增加。如果有，则替代。
            if len(area_results_table_oneWeek[area_results_table_oneWeek.sr_stat_time  == tm_roll_addone]) == 0:
                area_results_table_oneWeek = area_results_table_oneWeek.append(hour_results,ignore_index=True)
            else:
                area_results_table_oneWeek[area_results_table_oneWeek.sr_stat_time == tm_roll_addone] = hour_results.iloc[0,:].values
            
            #地块：判断是否有原位置,如果没有，增加。如果有，则替代。
            if len(block_results_table_oneWeek[block_results_table_oneWeek.sr_stat_time == tm_roll_addone]) == 0:
                block_results_table_oneWeek = block_results_table_oneWeek.append(block_hour_results,ignore_index=True)
            else:
                #按地块进行替代（以免有顺序差异）？没必要，顺序都是固定的
                block_results_table_oneWeek[block_results_table_oneWeek.sr_stat_time == tm_roll_addone] = block_hour_results.iloc[:,:].values
                #发现重新赋值后，数据格式会改变，在此进行修改。
                block_results_table_oneWeek['sr_stat_time'] = block_results_table_oneWeek.sr_stat_time.apply(lambda x:pd.to_datetime(x))
                block_results_table_oneWeek['sr_stat_date'] = block_results_table_oneWeek.sr_stat_date.apply(lambda x:str(x))
            #近一周循环。
            tm_roll = tm_roll_addone
            
            
        #近一周数据已经全部更新，计算需要考虑全天的指标
        #今日累计拥堵时间（小时） sr_defregion_acontime：分别提取这7天每一天的全部数据，计算拥堵时长。
        #今日识别排队车辆数（辆） sr_defregion_dgaquecar：分别提取这7天每一天的全部数据，把每小时的排队车辆加起来
        #今日最高饱和度（百分比） sr_defregion_amaxsat：分别提取这7天每一天的全部数据，找出最高饱和度。
        
        #综合停车拥堵指数 sr_defregion_aparkconindex：根据前三个变量计算
        #综合停车拥堵程度（高中低） sr_defregion_aparkcongrade：根据前三个变量计算
        #区域的是否高峰时段 sr_defregion_rushhour ：分别提取这7天每一天的全部数据，重新判断每天的高峰时段。
        #地块的是否高峰时段 sr_defregion_rushhour：分别提取这7天每一天的全部数据，重新判断每天的高峰时段。
        sr_stat_time_day = area_results_table_oneWeek.sr_stat_time.apply(lambda x: x.strftime('%Y-%m-%d'))
        sr_stat_time_uq = pd.unique(sr_stat_time_day)
        
        blk_sr_stat_time_day = block_results_table_oneWeek.sr_stat_time.apply(lambda x: x.strftime('%Y-%m-%d'))
        for i in sr_stat_time_uq:
            '''
            #先修改区域结果：
            '''
            results_thisday = area_results_table_oneWeek[sr_stat_time_day == i]
            #今日累计拥堵时间（小时） sr_defregion_acontime：大于80%算拥堵
            sr_defregion_acontime = len(results_thisday[results_thisday.sr_defregion_aparksat > 80])
            #今日识别排队车辆数（辆） sr_defregion_dgaquecar
            sr_defregion_dgaquecar = int(np.sum(results_thisday['sr_defregion_aquecar']))
            #今日最高饱和度（百分比） sr_defregion_amaxsat
            sr_defregion_amaxsat = max(results_thisday['sr_defregion_aparksat'])
            #综合停车拥堵指数 sr_defregion_aparkconindex
            #累计拥堵占比50%，今日识别排队占10%，最高饱和度占40%
            if sr_defregion_acontime < 6:
                jam = (sr_defregion_acontime / 5) * 4.5
            else:
                jam = 5
            
                
            if sr_defregion_dgaquecar < 200:
                lineup = (sr_defregion_dgaquecar / 199) * 0.9
            else:
                lineup = 1
                
            if sr_defregion_amaxsat < 90:
                satu = (sr_defregion_amaxsat / 89) * 3.5
            else:
                satu = 4
                
            sr_defregion_aparkconindex = round(jam + lineup + satu,1)
            #综合停车拥堵程度（高中低） sr_defregion_aparkcongrade
            if sr_defregion_aparkconindex < 4:
                sr_defregion_aparkcongrade = '低'
            elif sr_defregion_aparkconindex < 7:
                sr_defregion_aparkcongrade = '中'
            else:
                sr_defregion_aparkcongrade = '高'
            #以上结果全都进行替换
            results_thisday['sr_defregion_acontime'] = sr_defregion_acontime
            results_thisday['sr_defregion_dgaquecar'] = sr_defregion_dgaquecar
            results_thisday['sr_defregion_amaxsat'] = sr_defregion_amaxsat
            results_thisday['sr_defregion_aparkconindex'] = sr_defregion_aparkconindex
            results_thisday['sr_defregion_aparkcongrade'] = sr_defregion_aparkcongrade
            #区域高峰时段：
            #在停车辆数最大值所在的时间点
            tm_sr_defregion_ainparkcar_max = results_thisday[results_thisday.sr_defregion_ainparkcar == results_thisday.sr_defregion_ainparkcar.max()].sr_stat_time.iloc[0]
            #早高峰：7-10时 或 午高峰：11-15时 或 晚高峰：16-20时 或 夜高峰：21-次日6时(因为涉及跨天，所以直接分两段)
            hour_num = int(tm_sr_defregion_ainparkcar_max.strftime('%H'))
            thisday_hour = results_thisday.sr_stat_time.apply(lambda x: int(x.strftime('%H')))
            if (hour_num > 6) & (hour_num < 11):
                results_thisday.sr_defregion_rushhour = thisday_hour.isin([7,8,9,10]).apply(lambda x:str(int(x)))
            elif (hour_num > 10) & (hour_num < 16):
                results_thisday.sr_defregion_rushhour = thisday_hour.isin([11,12,13,14,15]).apply(lambda x:str(int(x)))
            elif (hour_num > 15) & (hour_num < 21):
                results_thisday.sr_defregion_rushhour = thisday_hour.isin([16,17,18,19,20]).apply(lambda x:str(int(x)))
            elif (hour_num > 20) & (hour_num < 24):
                results_thisday.sr_defregion_rushhour = thisday_hour.isin([21,22,23]).apply(lambda x:str(int(x)))
            else:
                results_thisday.sr_defregion_rushhour = thisday_hour.isin([0,1,2,3,4,5,6]).apply(lambda x:str(int(x)))
            #将这一天的结果覆盖源数据
            area_results_table_oneWeek[sr_stat_time_day == i] = results_thisday
            
            '''
            #再修改地块结果：
            '''
            #地块的高峰时段：涉及多个地块的不同时段。
            blk_results_thisday = block_results_table_oneWeek[blk_sr_stat_time_day == i]
            blk_all = pd.unique(blk_results_thisday.sr_defblock_code)
            #对地块进行循环修改
            for blk_i in blk_all:
                blk_i_results_thisday = blk_results_thisday[blk_results_thisday['sr_defblock_code'] == blk_i]
                #在停车辆数最大值所在的时间点
                tm_blk_i_max = blk_i_results_thisday[blk_i_results_thisday.sr_defblock_ainparkcar == blk_i_results_thisday.sr_defblock_ainparkcar.max()].sr_stat_time.iloc[0]
                #早高峰：7-10时 或 午高峰：11-15时 或 晚高峰：16-20时 或 夜高峰：21-次日6时(因为涉及跨天，所以直接分两段)
                hour_num = int(tm_blk_i_max.strftime('%H'))
                thisday_hour = blk_i_results_thisday.sr_stat_time.apply(lambda x: int(x.strftime('%H')))
                if (hour_num > 6) & (hour_num < 11):
                    blk_i_results_thisday.sr_defregion_rushhour = thisday_hour.isin([7,8,9,10]).apply(lambda x:str(int(x)))
                elif (hour_num > 10) & (hour_num < 16):
                    blk_i_results_thisday.sr_defregion_rushhour = thisday_hour.isin([11,12,13,14,15]).apply(lambda x:str(int(x)))
                elif (hour_num > 15) & (hour_num < 21):
                    blk_i_results_thisday.sr_defregion_rushhour = thisday_hour.isin([16,17,18,19,20]).apply(lambda x:str(int(x)))
                elif (hour_num > 20) & (hour_num < 24):
                    blk_i_results_thisday.sr_defregion_rushhour = thisday_hour.isin([21,22,23]).apply(lambda x:str(int(x)))
                else:
                    blk_i_results_thisday.sr_defregion_rushhour = thisday_hour.isin([0,1,2,3,4,5,6]).apply(lambda x:str(int(x)))
                #将这一地块该天的结果覆盖源数据    
                blk_results_thisday[blk_results_thisday['sr_defblock_code'] == blk_i] = blk_i_results_thisday
            #将所有地块的该天的结果覆盖源数据
            block_results_table_oneWeek[blk_sr_stat_time_day == i] = blk_results_thisday
        
        #在数据库中删除七天前的数据，添加最新的    
        #区域表：删除，添加。
        sql_words = "DELETE FROM srcity_index_parking_defregiondmnd_hour WHERE (sr_defregion_code = 'sr001') AND (sr_stat_time >= TIMESTAMP('{}')) AND (sr_stat_time < TIMESTAMP('{}'))".format(tm_oneweek_ago,tm_start)
        retry_sql_execute(sql_words,3)
        retry_write_todb(area_results_table_oneWeek,3,'srcity_index_parking_defregiondmnd_hour')
        #地块表：删除，添加。
        sql_words = "DELETE FROM srcity_index_parking_defblockdmnd_hour WHERE (sr_defregion_code = 'sr001') AND (sr_stat_time >= TIMESTAMP('{}')) AND (sr_stat_time < TIMESTAMP('{}'))".format(tm_oneweek_ago,tm_start)
        retry_sql_execute(sql_words,3)
        retry_write_todb(block_results_table_oneWeek,3,'srcity_index_parking_defblockdmnd_hour')
        
        #加一个小时，继续循环
        tm_start = tm_start + datetime.timedelta(hours=1)
    
    end =time.clock()
    print(end - start)
    
      
    
    
    
    
    
    
    

