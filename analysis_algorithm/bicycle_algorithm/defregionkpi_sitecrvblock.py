#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 15:01:47 2020

@author: xin-yi.song
"""

'''
策略
0.用QGIS将路网分割为0.5米长的路段
1.用networkx导入路网
2.将自行车站点映射到最近的路段端点#scipy.spatial.cKDTree()
3.将地块出入口映射到最近的路段端点#scipy.spatial.cKDTree()
4.分别求出每个出入口和所有自行车站点之间的路段长度
5.只要出入口周围，有200米以内的自行车站点，该出入口就算被覆盖
6.随后根据出入口的地块名称属性，找到对应的地块，即为已覆盖的地块
'''

import os
import json
import numpy as np
import networkx as nx
import pandas as pd
import geopandas as gpd
from scipy.spatial import cKDTree
from scipy.spatial.distance import pdist
import matplotlib.pyplot as plt
from shapely.geometry import LineString,Point,Polygon
import datetime
import time

#定义时间，规定在每个月第一天的00：10用重新统计出来站点数据计算覆盖率
now = datetime.datetime.now()
#now = datetime.datetime(2020, 5, 1, 0, 10, 0)   
begin = datetime.datetime((now-datetime.timedelta(days=1)).year, 
                          (now-datetime.timedelta(days=1)).month, 
                           1, 0, 0, 0)
end = datetime.datetime((now-datetime.timedelta(days=1)).year, 
                          (now-datetime.timedelta(days=1)).month, 
                          (now-datetime.timedelta(days=1)).day,
                           0, 0, 0)

################################连接RDS#########################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'testdb_suzhou_analysis','utf8mb4'))

#############################按照自定义区域进行计算############################
#程序开始时间
start_time = time.time()  

#分别针对中医院、拙政园选取数据
for j in ['sr005', 'sr006']:

    ##################################导入地块信息###################################
    #导入地块信息，为gcj坐标
    with engine.connect() as conn:
        query = 'select sr_defblock_code, sr_defblock_cname, sr_defblock_boundarycns, sr_defblock_area from srcity_bt_defblockinfo' + '\n' \
              + 'where sr_defregion_code="' + j + '"'
        block_info = pd.read_sql(query, engine)
        
    #转化为dataframe格式
    block_info = pd.DataFrame(block_info)
    #将geojson的中括号转化为polygon
    block_info['geometry'] = block_info['sr_defblock_boundarycns'].apply(lambda x: [tuple(i) for i in json.loads(x)[0]])
    block_info['geometry'] = block_info['geometry'].apply(lambda x: Polygon(x))
    block_info = gpd.GeoDataFrame(block_info, geometry=block_info['geometry'], crs='epsg:4326')
    block_info.to_crs('epsg:32651', inplace=True)
    
    #################################导入路网数据####################################
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    
    #用networkx读路网数据
    if j == 'sr005':
        SOURCE_DATA_DIR = os.path.join(BASE_DIR, 'source_data','中医院路网_分解_32651')
        G = nx.read_shp(str(os.path.join(SOURCE_DATA_DIR, '中医院路网_分解_32651.shp')))#只能读shp，不能读geojson
    elif j == 'sr006':
        SOURCE_DATA_DIR = os.path.join(BASE_DIR, 'source_data','拙政园路网_分解_32651')
        G = nx.read_shp(str(os.path.join(SOURCE_DATA_DIR, '拙政园路网_分解_32651.shp')))#只能读shp，不能读geojson
    
    #从有向图转化为无向图
    GG = nx.Graph(G)
    type(GG)#有向图DiGraph，无向图Graph
    
    #将图的边转化成df格式
    df = nx.to_pandas_edgelist(GG)
    dff = df.loc[:,['source','target']]
    
    #计算边的长度并转成geodataframe格式
    dff.loc[:,"length"] = dff.apply(lambda x: pdist(np.vstack([x['source'],x['target']]), 'euclidean')[0], axis=1)
    dff.loc[:,'geometry'] = dff.apply(lambda x: LineString([x['source'],x['target']]), axis=1)
    dff = gpd.GeoDataFrame(dff, geometry=dff['geometry'])
    
    #重新将df转化为图
    GG_new = nx.from_pandas_edgelist(dff,'source','target', ['source','target','length'])
    
    #为了生成ckdtree,将节点转化为array
    nodes_new = list(GG_new.nodes())
    nA = np.array(nodes_new)
    
    #用路网节点生成一棵ckdtree
    btree = cKDTree(nA)
    
    ##############################导入自行车站点信息##################################
    #导入站点信息，为gcj坐标
    with engine.connect() as conn:
        query = 'select sr_site_code, sr_site_cname, sr_site_lon, sr_site_lat, sr_site_pilenum, sr_defregion_code from srcity_bt_bicyclesiteinfo'
        sta_info = pd.read_sql(query, engine)
        
    #转化为dataframe格式
    sta_info = pd.DataFrame(sta_info)
    sta_info.columns = ['site_code','site_cname','lon','lat','site_pile','defregion_code']
    #将经纬度组合成Point
    sta_info['geometry'] = sta_info.apply(lambda x: Point(x['lon'],x['lat']),axis=1)
    sta_info = gpd.GeoDataFrame(sta_info, geometry=sta_info['geometry'], crs='epsg:4326')
    sta_info.to_crs('epsg:32651', inplace=True)
    #找出自定义区域站点
    defregion = sta_info.loc[sta_info['defregion_code']==j].copy()
    #为自定义区域站点拼接投影坐标
    defregion['x'] = defregion['geometry'].apply(lambda geom: geom.x)
    defregion['y'] = defregion['geometry'].apply(lambda geom: geom.y)
    
    #与地块信息进行相交
    sta_in_block = gpd.sjoin(defregion, block_info, how='inner', op='intersects')
    
    #删除地块内部的站点
    nB_new = np.array(list(zip(defregion[~defregion['site_code'].isin(sta_in_block['site_code'])]['x'],
                               defregion[~defregion['site_code'].isin(sta_in_block['site_code'])]['y'])))
    
    #映射到路网上的点
    dist_B, idx_B = btree.query(nB_new, k=1)
    nodes_B = nA[idx_B]
    
    ################################导入出入口信息###################################
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    
    if j == 'sr005':
        SOURCE_DATA_DIR = os.path.join(BASE_DIR, 'source_data','中医院出入口_gcj.geojson')
    if j == 'sr006':
        SOURCE_DATA_DIR = os.path.join(BASE_DIR, 'source_data','拙政园出入口_gcj.geojson')
        
    gates = gpd.read_file(SOURCE_DATA_DIR)
    
    #转换坐标系
    gates.to_crs('epsg:32651', inplace=True)
    #为出入口拼接投影坐标
    gates['x'] = gates['geometry'].apply(lambda geom: geom.x)
    gates['y'] = gates['geometry'].apply(lambda geom: geom.y)
    
    #将节点转化为array
    nC_new = np.array(list(zip(gates['x'],gates['y'])))
    
    #映射到路网上的点
    dist_C, idx_C = btree.query(nC_new, k=1)
    nodes_C = nA[idx_C]
    
    #可视化检查效果
    road = dff.plot(color='pink')
    plt.scatter(nodes_B[:,0],  nodes_B[:,1])
    plt.scatter(nodes_C[:,0],  nodes_C[:,1])
    plt.show()
    
    ######################求出每个出入口和所有自行车站点之间的路段长度####################
    #节点使用连续整数重新标记
    GG_new = nx.relabel.convert_node_labels_to_integers(GG_new,first_label=0,label_attribute='length') 
    
    #为每个出入口定义最短路函数
    def shortest(num_s):
        #计算最短路
        path_list = []
        length_list = []
        for num_t in idx_B:
            try:
                length,path = nx.bidirectional_dijkstra(GG_new, source=num_s, target=num_t, weight='length')
                path_list.append(path)
                length_list.append(length)
            except:
                print(num_s, ' to ', num_t, '异常')
        #记录最短路径的长度
        if len(length_list)!=0:
            min_length = min(length_list)
            min_path= path_list[length_list.index(min_length)]
            return min_path,min_length
        else:
            return [],20000
          
    #为每个出入口计算最短路
    dist = gates.copy()
    dist.loc[:,'idx_C'] = idx_C
    dist.loc[:,'dist']=20000   
        
    for num_s in idx_C:
        min_path,min_length = shortest(num_s)
        dist.loc[dist['idx_C']==num_s,'dist'] = min_length
        
    #############################找出未被覆盖的地块###################################
    block_yes = list(dist[dist['dist']<=200]['block_name'].unique())#找出200米之内有站点的出入口对应的地块名称
    block_no = list(dist[~dist['block_name'].isin(block_yes)]['block_name'].unique())#找出200米之内没有站点的出入口对应的地块名称
    
    #如果地块内部有站点，将其从block_no中删除
    block_final = [x for x in block_no if x not in list(sta_in_block['sr_defblock_cname'])]
    
    ###############################导入苏州项目数据库#################################
    #按字段顺序准备要导入的数据表
    df = pd.DataFrame()
    df.loc[0,'sr_defregion_code'] = j
    df['sr_stat_year'] = str(begin.year)
    df['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
    df['sr_defregion_area'] = '{:.2f}'.format(block_info['sr_defblock_area'].sum()/100)
    df['sr_defregion_sitenum'] = sta_info[sta_info['defregion_code']==j].shape[0]
    df['sr_defregion_pilenum'] = sta_info[sta_info['defregion_code']==j]['site_pile'].sum()
    df['sr_defregion_sitecvrarea'] = block_info[~block_info['sr_defblock_cname'].isin(block_final)]['sr_defblock_area'].sum()
    df['sr_defregion_sitecvrarea'] = df['sr_defregion_sitecvrarea'].apply(lambda x: '{:.2f}'.format(x/100))
    df['sr_defregion_sitecvrarea_rate'] = '{:.2f}'.format(float(df['sr_defregion_sitecvrarea'])/float(df['sr_defregion_area']))
    df['sr_defregion_sitenocvrarea'] = float(df['sr_defregion_area'])-float(df['sr_defregion_sitecvrarea'])
    df['sr_defregion_sitenocvrarea_rate'] = 1-float(df['sr_defregion_sitecvrarea_rate'])
    df['sr_insertedby'] = 'Xin-Yi Song'
    df['sr_updatedby'] = 'Xin-Yi Song'
    
    with engine.connect() as conn:
        #conn.execute('delete from srcity_index_bicycle_defregionkpi')
        df.to_sql(
                name = 'srcity_index_bicycle_defregionkpi',
                con = engine,
                index = False,
                if_exists = 'append'
                )
    
    ###############################导入苏州项目数据库#################################
    #按字段顺序准备要导入的数据表
    dff = pd.DataFrame()
    dff['sr_defblock_code'] = block_info['sr_defblock_code']
    dff['sr_defregion_code'] = j
    dff['sr_stat_year'] = str(begin.year)
    dff['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
    dff['sr_iscover'] = block_info['sr_defblock_cname'].apply(lambda x: 0 if x in block_final else 1)
    dff['sr_insertedby'] = 'Xin-Yi Song'
    dff['sr_updatedby'] = 'Xin-Yi Song'
    
    with engine.connect() as conn:
        #conn.execute('delete from srcity_index_bicycle_sitecrvblock')
        dff.to_sql(
                name = 'srcity_index_bicycle_sitecrvblock',
                con = engine,
                index = False,
                if_exists = 'append'
                )

#程序结束时间
over_time = time.time() 
#程序运行时间
total_time = over_time - start_time
#打印耗费时间
print(begin,'~',end,':',total_time)