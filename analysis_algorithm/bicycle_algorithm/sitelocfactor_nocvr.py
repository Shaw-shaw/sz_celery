#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 13:38:54 2020

@author: xin-yi.song
"""

import datetime
import time
import os
import sys
import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
from scipy.spatial import cKDTree
from osgeo import gdal

#定义时间，规定在每个月第一天的00：30用重新统计出来的未覆盖区域计算其影响因素
now = datetime.datetime.now()
#now = datetime.datetime(2020, 5, 1, 0, 30, 0)   
begin = datetime.datetime((now-datetime.timedelta(days=1)).year, 
                          (now-datetime.timedelta(days=1)).month, 
                           1, 0, 0, 0)
end = datetime.datetime((now-datetime.timedelta(days=1)).year, 
                          (now-datetime.timedelta(days=1)).month, 
                          (now-datetime.timedelta(days=1)).day,
                           0, 0, 0)

#程序开始时间
start_time = time.time() 
################################连接RDS#########################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'testdb_suzhou_analysis','utf8mb4'))

#############################按照自定义区域进行计算############################
#分别针对中医院、拙政园选取数据
for j in ['sr005','sr006']:
    
    ################################导入未覆盖地块列表################################
    with engine.connect() as conn:
        query = 'select sr_defblock_code,sr_defblock_cname,sr_defblock_area from srcity_bt_defblockinfo' + '\n' \
              + 'where sr_defblock_code in ' + '\n' \
              + '(select sr_defblock_code from srcity_index_bicycle_sitecrvblock ' \
              + 'where sr_iscover=0 and sr_defregion_code="' + j +'")'
        block_no = pd.read_sql(query, engine)
        
    ################################导入出入口信息###################################
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    if j == 'sr005':
        SOURCE_DATA_DIR = os.path.join(BASE_DIR, 'source_data','中医院出入口_gcj.geojson')
    elif j == 'sr006':
        SOURCE_DATA_DIR = os.path.join(BASE_DIR, 'source_data','拙政园出入口_gcj.geojson')
    gates = gpd.read_file(SOURCE_DATA_DIR)
    #转换坐标系
    gates.to_crs('epsg:32651', inplace=True)
    #找出未覆盖区域的出入口作为新站点
    new_sta = gates[gates['block_name'].isin(list(block_no['sr_defblock_cname']))].copy()
    #专门设置一列记录投影坐标的array格式
    new_sta.loc[:,'array'] = new_sta.apply(lambda x: np.array(x['geometry']), axis=1)
    #将新站点转化为节点
    nodes_sta = np.array(list(new_sta['array']))
    #以站点为中心做200米的buffer
    buffers = new_sta.buffer(200)
    sta_buffer = new_sta[['block_name','gate_name','lon','lat']]
    sta_buffer = gpd.GeoDataFrame(sta_buffer, geometry=buffers, crs='epsg:32651')
    
    ################################连接到ODPS######################################
    from odps import ODPS
    
    odps_config = {
                    "access_id": "LZR4s9cdwJdi82ID",
                    "access_key": "jbelpqOhlBNDG4Wv6ILdOty0jaccO8",
                    "project": "csdnsz_dma",
                    "endpoint": "http://service.cn-suzhou-szdn-d01.odps.cloud-ops.szdn.suzhou.gov.cn/api"
                    }
    od = ODPS(
              odps_config.get("access_id"),
              odps_config.get("access_key"), 
              odps_config.get("project"),
              endpoint=odps_config.get("endpoint"))
    
    ###################################读取轨道站点##################################
    #从大脑中读取轨道站点数据,DIM中的数据为gcj02坐标
    sql = 'select * from traffic_base.dim_metro_lineinfo'
    
    with od.execute_sql(sql).open_reader() as reader:
        subway = reader
        subway = subway.to_pandas()
        
    #将经度、纬度转化为Point
    subway['geometry'] = subway.apply(lambda x: Point(x['jd'],x['wd']), axis=1)
    subway = gpd.GeoDataFrame(subway, geometry=subway['geometry'], crs='epsg:4326')
    subway.to_crs('epsg:32651', inplace=True)
    #专门设置一列记录投影坐标的array格式
    subway.loc[:,'array'] = subway.apply(lambda x: np.array(x['geometry']), axis=1)
    #将新站点转化为节点
    nodes_sub = np.array(list(subway['array']))
    
    #用ckdtree找到离站点最近的地铁站
    btree = cKDTree(nodes_sub)
    dist_sub, idx_sub = btree.query(nodes_sta, k=1)
    
    #在sta_buffer中记录dist_sub和name_sub
    sta_buffer.loc[:,'dist_sub'] = dist_sub
    sta_buffer.loc[:,'name_sub'] = list(subway.loc[list(idx_sub),'czmc'])
    
    ################################导入公交站点#####################################
    #从大脑中读取公交站点数据,DIM中的数据为gcj02坐标
    sql = 'select distinct zdmc, jd, wd from traffic_base.dim_bus_standinfo'
    
    with od.execute_sql(sql).open_reader() as reader:
        bus = reader
        bus = bus.to_pandas()
    
    #将经度、纬度转化为Point
    bus['geometry'] = bus.apply(lambda x: Point(x['jd'],x['wd']), axis=1)
    bus = gpd.GeoDataFrame(bus, geometry=bus['geometry'], crs='epsg:4326')
    bus.to_crs('epsg:32651', inplace=True)
    
    #跟sta_buffer做相交
    tmp = gpd.sjoin(sta_buffer, bus, how="left", op='contains')
    tmp = tmp.groupby(by=['block_name','gate_name'],as_index=True).size().reset_index(name='bus_count')
    sta_buffer = pd.merge(sta_buffer, tmp, how='left', on=['block_name','gate_name'])
    
    ###############################导入停车场位置####################################
    #从大脑中读取停车场数据,DIM中的数据为gcj02坐标
    sql = 'select * from traffic_base.dim_parking_siteinfo'
    
    with od.execute_sql(sql).open_reader() as reader:
        park = reader
        park = park.to_pandas()
        
    #将经度、纬度转化为Point
    park['geometry'] = park.apply(lambda x: Point(float(x['longitude']),float(x['latitude'])), axis=1)
    park = gpd.GeoDataFrame(park, geometry=park['geometry'], crs='epsg:4326')
    park.to_crs('epsg:32651', inplace=True)
    
    #跟sta_buffer做相交
    tmp = gpd.sjoin(sta_buffer, park, how="left", op='contains')
    tmp = tmp.groupby(by=['block_name','gate_name'],as_index=True).size().reset_index(name='park_count')
    sta_buffer = pd.merge(sta_buffer, tmp, how='left', on=['block_name','gate_name'])
    
    ###############################导入POI##########################################
    #从文件中导入poi，不以code区分类型，直接以retype区分类型
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    SOURCE_DATA_DIR = os.path.join(BASE_DIR, 'source_data','suzhou_poi_alltypes_gcj_20200413.geojson')
    poi = gpd.read_file(SOURCE_DATA_DIR)
    
    #将地理坐标转化为投影坐标
    poi.to_crs('epsg:32651', inplace=True)
    
    #列出所有类型
    retypes = list(poi['retype'].unique())
    #依次跟sta_buffer做相交
    for retype in retypes:
        tmp = gpd.sjoin(sta_buffer, poi[poi['retype']==retype], how="left", op='contains')
        tmp = tmp.groupby(by=['block_name','gate_name'],as_index=True).size().reset_index(name=retype + '_count')
        sta_buffer = pd.merge(sta_buffer, tmp, how='left', on=['block_name','gate_name'])
    
    ##################################导入人口数据###################################
    #从RDS中导入基站人口数据
    with engine.connect() as conn:
        query = 'select * from srcity_ft_popdstr'
        pop = pd.read_sql(query, engine)
        
    #调用construct_geodataframe函数，将geojson转换成geometry
    sys.path.append(os.path.dirname(os.path.abspath(__file__)))
    from construct_geodataframe import construct_gdf_from_df
    pop = construct_gdf_from_df(pop,'sr_pop_pointspatialtype','sr_pop_pointgc')
    
    #在QGIS中做一张核密度栅格图，重新投影坐标系为32651，横坐标像素大小为200，纵坐标大小像素为200，该操作是为了得到栅格图尺寸
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    SOURCE_DATA_DIR = os.path.join(BASE_DIR, 'source_data','pop_raster.tif')
    dataset = gdal.Open(SOURCE_DATA_DIR)  
    dataset.GetProjection()#读取TIFF文件坐标系
    
    #得到基站点对应的栅格范围
    spatial_range = dataset.GetGeoTransform()#读取仿射变换矩阵，共有六个参数，描述的是栅格行列号和地理坐标之间的关系
    row_col_shape = (dataset.RasterYSize, dataset.RasterXSize)#栅格像元数，YSize为行数，XSize为列数
    band_width = 2500#带宽
         
    #调用函数文件进行核密度估计
    from pop_density_200 import create_train_X, construct_grids, pop_kde
    
    for pop_field in ['sr_pop_resdntnum','sr_pop_worknum']:
        
        train_X, sample_weight = create_train_X(pop, pop_field)
        xy = construct_grids(spatial_range, row_col_shape)
        pop_in_pixel_32651 = pop_kde(spatial_range, row_col_shape, band_width, xy, 
                                     train_X, sample_weight, pop_field)
        #专门设置一列记录投影坐标的array格式
        pop_in_pixel_32651.loc[:,'array'] = pop_in_pixel_32651.apply(lambda x: np.array(x['geometry']), axis=1)
        #将新站点转化为节点
        nodes_pop = np.array(list(pop_in_pixel_32651['array']))
    
        #用ckdtree找到离站点最近的栅格点
        btree = cKDTree(nodes_pop)
        dist_pop, idx_pop = btree.query(nodes_sta, k=1)
    
        #在sta_buffer中记录gongzuo的population
        sta_buffer.loc[:,'pop_'+pop_field.split('-')[-1]] = list(pop_in_pixel_32651.loc[list(idx_pop),'population'])
    
    ###############################导入苏州项目数据库#################################
    #按字段顺序准备要导入的数据表
    tmp = sta_buffer.groupby(by='block_name').mean().reset_index()
    tmp['block_code'] = tmp['block_name'].apply(lambda x: block_no[block_no['sr_defblock_cname']==x]['sr_defblock_code'].values[0])
    df = pd.DataFrame()
    df['sr_defblock_code'] = tmp['block_code']
    df['sr_defregion_code'] = j
    df['sr_stat_year'] = str(begin.year)
    df['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
    df['sr_defblock_gridworkpop'] = tmp['pop_sr_pop_worknum'].apply(lambda x: '{:.0f}'.format(x))#工作人口
    df['sr_defblock_gridrespop'] = tmp['pop_sr_pop_resdntnum'].apply(lambda x: '{:.0f}'.format(x))#居住人口
    df['sr_defblock_bizpoi'] = tmp['商务办公_count'].apply(lambda x: '{:.0f}'.format(x)) #商务办公poi数
    df['sr_defblock_servepoi'] = tmp['商业服务_count'].apply(lambda x: '{:.0f}'.format(x))#商业服务poi数
    df['sr_defblock_leipoi'] = tmp['文体休闲_count'].apply(lambda x: '{:.0f}'.format(x))#文体休闲poi数
    df['sr_defblock_medpoi'] = tmp['医疗设施_count'].apply(lambda x: '{:.0f}'.format(x))#医疗设施poi数
    df['sr_defblock_schoolpoi'] = tmp['学校_count'].apply(lambda x: '{:.0f}'.format(x))#学校poi数
    df['sr_defblock_adminnum'] = tmp['行政办公_count'].apply(lambda x: '{:.0f}'.format(x))#行政办公poi数
    df['sr_defblock_parkpoi'] = tmp['公园_count'].apply(lambda x: '{:.0f}'.format(x))#公园poi数
    df['sr_defblock_busstopnum'] = tmp['bus_count'].apply(lambda x: '{:.0f}'.format(x))#公交站数量
    df['sr_defblock_subwaydist'] = tmp['dist_sub'].apply(lambda x: '{:.0f}'.format(x))#地铁站距离 
    df['sr_defblock_parkingnum'] = tmp['park_count'].apply(lambda x: '{:.0f}'.format(x))#停车场数量
    df['sr_defblock_blindarea'] = tmp['block_name'].apply(lambda x: block_no[block_no['sr_defblock_cname']==x]['sr_defblock_area'].values[0]/100)
    df['sr_insertedby'] = 'Xin-Yi Song'
    df['sr_updatedby'] = 'Xin-Yi Song'
        
    with engine.connect() as conn:
        #conn.execute('delete from srcity_index_bicycle_sitelocfactor_nocvr')
        df.to_sql(
                  name = 'srcity_index_bicycle_sitelocfactor_nocvr',
                  con = engine,
                  index = False,
                  if_exists = 'append'
                  )
    
    #程序结束时间
    over_time = time.time() 
    #程序运行时间
    total_time = over_time - start_time
    #打印耗费时间
    print(begin,'~',end,':',total_time)