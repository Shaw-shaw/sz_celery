#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 10:38:54 2020

@author: xin-yi.song
"""

import pandas as pd
import datetime
import calendar
import time

#%matplotlib qt5

#规定在每个月1号的01:30，计算上个月数据的平均值
now = datetime.datetime.now()
#now = datetime.datetime(2020, 4, 1, 1, 30, 0)
last_month = now-datetime.timedelta(days=1)

################################连接到RDS#######################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'testdb_suzhou_analysis','utf8mb4'))
#engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('test_suzhou', '123456', '101.89.91.175', '33065', 'testdb_suzhou_analysis_gongan02','utf8'))

#############################按照自定义区域进行计算############################
#程序开始时间
start_time = time.time()  

#分别针对中医院、拙政园选取数据
for j in ["sr005", "sr006"]:

    ###############################导入每小时站点关联表###############################
    #导入上个月的数据
    with engine.connect() as conn:
        query = 'select * from srcity_index_bicycle_defsiterel_hour' + '\n' \
              + 'where sr_defregion_code="' + j + '"' + '\n' \
              + 'and sr_stat_month=' + '"' + str(last_month.year) + '-' + str(last_month.month).zfill(2) + '"'
        siterel_hour = pd.read_sql(query, engine)
    
    #按日期类型计算,平均值=次数和/上个月的天数
    for date_type in ['节假日','工作日','双休日']:#date_type = '工作日'
        days = calendar.monthrange(last_month.year,last_month.month)[1]
        tmp = siterel_hour[siterel_hour['sr_date_type']==date_type]
        tmp = tmp.groupby(by=['sr_stat_hour','sr_site_code_rent','sr_site_code_return'])['sr_site_relperhour'].sum().reset_index()
        tmp['sr_site_relperhour'] = tmp['sr_site_relperhour']/days
        tmp['sr_date_type'] = date_type
        if len(tmp.index)!=0:
            #找到最粗OD线所在时段
            rushhour = tmp.loc[tmp['sr_site_relperhour'].idxmax(),'sr_stat_hour']
            #将该时段的OD线从高到低排序，确定阈值为第30条的取值
            tmp_rushhour = tmp[tmp['sr_stat_hour']==rushhour].sort_values(by='sr_site_relperhour', ascending=False).reset_index(drop=True)   
            thre = tmp_rushhour.loc[29,'sr_site_relperhour']
            #thre = tmp['sr_site_relperhour'].quantile(0.4) 
            #构造级别对应列表
            rank = pd.DataFrame()
            rank['up'] = tmp_rushhour[tmp_rushhour['sr_site_relperhour']>=thre]['sr_site_relperhour'].unique()[:-1]
            rank['bottom'] = tmp_rushhour[tmp_rushhour['sr_site_relperhour']>=thre]['sr_site_relperhour'].unique()[1:]
            rank['rank'] = range(5,5-rank.shape[0],-1)
            #如果级别小于1，将后续级别都包含至第1级
            rank.loc[rank['rank']<1,'rank'] = 1
            #将级别对应表中的上限改为无穷大
            rank.loc[rank['up'].idxmax(),'up'] = float('Inf')
            #为全部数据匹配等级
            tmp['rank'] =0
            for i in range(0, rank.shape[0]):
                tmp['rank'] = tmp.apply(lambda x: rank.loc[i,'rank'] 
                                                  if x['sr_site_relperhour']<rank.loc[i,'up'] and x['sr_site_relperhour']>=rank.loc[i,'bottom'] else x['rank'], axis=1)
            tmp = tmp[tmp['rank']>0]
            
    ###############################导入苏州项目数据库#################################
        if len(tmp.index)!=0: 
            #按字段顺序准备要导入的数据表
            df = tmp.rename(columns={'sr_site_relperhour':'sr_site_relhavgm',
                                     'rank':'sr_site_relrank'})
            df['sr_site_relhavgm'] = df['sr_site_relhavgm'].apply(lambda x: '{:.2f}'.format(x))
            df['sr_defregion_code'] = j
            df['sr_stat_year'] = str(last_month.year)
            df['sr_stat_month'] = str(last_month.year) + '-' + str(last_month.month).zfill(2)
            df['sr_insertedby'] = 'Xin-Yi Song'
            df['sr_updatedby'] = 'Xin-Yi Song'
    
            with engine.connect() as conn:
                #conn.execute('delete from srcity_index_bicycle_defsiterel_havgm')
                df.to_sql(
                        name = 'srcity_index_bicycle_defsiterel_havgm',
                        con = engine,
                        index = False,
                        if_exists = 'append'
                        )
   
#程序结束时间
over_time = time.time() 
#程序运行时间
total_time = over_time - start_time
#打印耗费时间
print(str(last_month.year),'-',str(last_month.month),':',total_time)
