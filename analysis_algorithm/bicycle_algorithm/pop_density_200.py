# -*- coding: utf-8 -*-
import numpy as np
import geopandas
from shapely.geometry import Point
from sklearn.neighbors import KernelDensity
###############################定义训练集########################################
def create_train_X(pop, pop_field):
    
    pop_point_4326 = pop#导入柏杨处理的基站点数据，gcj坐标
    pop_point_32651 = pop_point_4326.to_crs({'init':'epsg:32651'})#将地理坐标系转化为投影坐标系
    
    train_X = []#训练数据的坐标位置
    sample_weight = np.array([])#训练数据的人口数
    for index,i in pop_point_32651.iterrows():
        lng_lat_list = np.array([])
        lng_lat_list = np.append(lng_lat_list,i.geometry.x)
        lng_lat_list = np.append(lng_lat_list,i.geometry.y)
        train_X.append(lng_lat_list)
        sample_weight = np.append(sample_weight, i[pop_field])

    sample_weight = np.nan_to_num(sample_weight)
    train_X = np.array(train_X)
    
    return train_X, sample_weight

#################################构建网格#######################################
'''
放射变换矩阵每个元素下标都代表着不同的含义：
0：图像左上角的X坐标
1：图像东西方向分辨率
2：旋转角度，如果图像北方朝上，该值为0
3：图像左上角的Y坐标
4：旋转角度，如果图像北方朝上，该值为0
5：图像南北方向分辨率
'''
def construct_grids(spatial_range, row_col_shape):
    #找出图像的坐标边界 
    pixel_width = spatial_range[1]#像素宽度
    pixel_height = spatial_range[5]#像素高度
    x_min = spatial_range[0]#最左侧的x坐标
    x_max = x_min + row_col_shape[1] * pixel_width#最右侧的x坐标
    y_max = spatial_range[3]#最上侧的y坐标
    y_min = y_max + row_col_shape[0] * pixel_height#最下侧的y坐标
    #找到每个栅格的中心坐标
    x_grid = np.arange(x_min, x_max, pixel_width) + (pixel_width/2)
    y_grid = np.arange(y_max, y_min, pixel_height) + (pixel_height/2)
    #生成网格点，返回list,有两个元素,第一个元素是X轴的取值,第二个元素是Y轴的取值    
    X, Y = np.meshgrid(x_grid, y_grid)
    xy = np.vstack([X.ravel(), Y.ravel()]).T#按行展开，垂直（按行）顺序堆叠数组
    
    return xy

#############################进行核密度估计######################################
def pop_kde(spatial_range, row_col_shape, band_width, xy, train_X, sample_weight, pop_field):
    
    kde = KernelDensity(bandwidth= band_width, metric='euclidean', kernel='gaussian')
    kde.fit(train_X, sample_weight)
    
    #人口的概率密度分布
    population_prob_density =  np.exp(kde.score_samples(xy))
    #像元中人口分布的概率
    population_prob_in_pixel = population_prob_density *  spatial_range[1] * np.abs(spatial_range[5]) 
    #像元中人口分布的数量
    population_in_pixel = np.round(population_prob_in_pixel * sample_weight.sum(),0)

    population_in_pixel_32651 = geopandas.GeoDataFrame(population_in_pixel,crs='+init=epsg:32651')
    point_list = []
    for i in xy:
        point = Point(i[0], i[1])
        point_list.append(point)

    population_in_pixel_32651.geometry = point_list
    population_in_pixel_32651.columns = ['population','geometry']
    
#    population_in_pixel_4326 = population_in_pixel_32651.to_crs({'init':'epsg:4326'})
#    population_in_pixel_4326.to_file(out_path, driver='GeoJSON')
    
    return population_in_pixel_32651





    