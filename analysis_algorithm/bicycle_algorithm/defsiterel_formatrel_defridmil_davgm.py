#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 21:35:06 2020

@author: xin-yi.song
"""
import math
import pandas as pd
import datetime

#%matplotlib qt5

#规定在每个月1号的01:00，计算上个月数据的平均值
now = datetime.datetime.now()
#now = datetime.datetime(2020, 4, 1, 1, 0, 0)
last_month = now-datetime.timedelta(days=1)

################################连接到RDS#######################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'testdb_suzhou_analysis','utf8mb4'))

###############################导入每日站点关联表#################################
#导入上个月的数据
with engine.connect() as conn:
    query = 'select * from srcity_index_bicycle_defsiterel_day where sr_stat_month=' + '"' + str(last_month.year) + '-' + str(last_month.month).zfill(2) + '"'
    rel_day = pd.read_sql(query, engine)
    
#转化为dataframe格式
rel_day = pd.DataFrame(rel_day)
#按日期类型计算平均值
for date_type in ['节假日', '双休日', '工作日']:#date_type = '工作日'
    tmp_rel = rel_day[rel_day['sr_date_type']==date_type]
    tmp_rel = tmp_rel.groupby(by=['sr_site_code_rent','sr_site_code_return','sr_defregion_code']).mean().reset_index()
    tmp_rel['sr_date_type'] = date_type
    #print(tmp_rel)
    
###############################导入苏州项目数据库#################################
    if len(tmp_rel)!=0: 
        #按字段顺序准备要导入的数据表
        df = tmp_rel.drop(columns=['id'])
        df['sr_stat_year'] = str(last_month.year)
        df['sr_stat_month'] = str(last_month.year) + '-' + str(last_month.month).zfill(2)
        df['sr_insertedby'] = 'Xin-Yi Song'
        df['sr_updatedby'] = 'Xin-Yi Song'
        df.rename(columns={'sr_site_relperday':'sr_site_reldavgm'}, inplace=True)
        df['sr_site_reldavgm'] = df['sr_site_reldavgm'].apply(lambda x: '{:.2f}'.format(x))

        with engine.connect() as conn:
            #conn.execute('delete from srcity_index_bicycle_defsiterel_davgm')
            df.to_sql(
                    name = 'srcity_index_bicycle_defsiterel_davgm',
                    con = engine,
                    index = False,
                    if_exists = 'append'
                    )   

        print(date_type, 'davgm:', df.shape[0])    

###############################导入每日建筑功能关联表##############################
#导入上个月的数据
with engine.connect() as conn:
    query = 'select * from srcity_index_bicycle_formatrel_day where sr_stat_month=' + '"' + str(last_month.year) + '-' + str(last_month.month).zfill(2) + '"'
    format_day = pd.read_sql(query, engine)
    
#转化为dataframe格式
format_day = pd.DataFrame(format_day)
#按日期类型计算平均值
for date_type in ['节假日', '双休日', '工作日']:#date_type = '工作日'
    tmp_format = format_day[format_day['sr_date_type']==date_type]
    tmp_format = tmp_format.groupby(by=['sr_format_code_rent','sr_format_code_return','sr_defregion_code']).mean().reset_index()
    tmp_format['sr_date_type'] = date_type
    #print(tmp_format)

###############################导入苏州项目数据库#################################
    if len(tmp_format)!=0: 
        #按字段顺序准备要导入的数据表
        dff = tmp_format.drop(columns=['id'])
        dff['sr_stat_year'] = str(last_month.year)
        dff['sr_stat_month'] = str(last_month.year) + '-' + str(last_month.month).zfill(2)
        dff['sr_insertedby'] = 'Xin-Yi Song'
        dff['sr_updatedby'] = 'Xin-Yi Song'
        dff.rename(columns={'sr_format_relperday':'sr_format_reldavgm'}, inplace=True)
        dff['sr_format_reldavgm'] = dff['sr_format_reldavgm'].apply(lambda x: '{:.2f}'.format(x))

        with engine.connect() as conn:
            #conn.execute('delete from srcity_index_bicycle_formatrel_davgm')
            dff.to_sql(
                    name = 'srcity_index_bicycle_formatrel_davgm',
                    con = engine,
                    index = False,
                    if_exists = 'append'
                    )   

        print(date_type, 'davgm:', dff.shape[0])    

##################################导入每日骑行里程################################
#导入上个月的数据
with engine.connect() as conn:
    query = 'select * from srcity_index_bicycle_defridmil_day where sr_stat_month=' + '"' + str(last_month.year) + '-' + str(last_month.month).zfill(2) + '"'
    mile_day = pd.read_sql(query, engine)
    
#转化为dataframe格式
mile_day = pd.DataFrame(mile_day)

#按日期类型计算平均值
for date_type in ['节假日', '双休日', '工作日']:#date_type = '工作日'
    tmp_mile = mile_day[mile_day['sr_date_type']==date_type]
    
    #当数据不为空时
    if len(tmp_mile)!=0: 
        tmp_mile = tmp_mile.groupby(by=['sr_defregion_code']).mean().reset_index()
        tmp_mile.iloc[:,1:] = tmp_mile.iloc[:,1:].apply(lambda x: round(x) if not math.isnan(x) else 0)
        tmp_mile['sr_date_type'] = date_type
        #print(tmp_mile)    
###############################导入苏州项目数据库#################################
        #按字段顺序准备要导入的数据表
        dfff = tmp_mile.drop(columns=['id'])
        dfff['sr_stat_year'] = str(last_month.year)
        dfff['sr_stat_month'] = str(last_month.year) + '-' + str(last_month.month).zfill(2)
        dfff['sr_insertedby'] = 'Xin-Yi Song'
        dfff['sr_updatedby'] = 'Xin-Yi Song'
        dfff.rename(columns={'sr_defregion_rideperday_milzo':'sr_defregion_ridedavgm_milzo', \
                             'sr_defregion_rideperday_milot':'sr_defregion_ridedavgm_milot', \
                             'sr_defregion_rideperday_miltt':'sr_defregion_ridedavgm_miltt', \
                             'sr_defregion_rideperday_miltf':'sr_defregion_ridedavgm_miltf', \
                             'sr_defregion_rideperday_milfm':'sr_defregion_ridedavgm_milfm'}, inplace=True)

        with engine.connect() as conn:
            #conn.execute('delete from srcity_index_bicycle_defridmil_davgm')
            dfff.to_sql(
                    name = 'srcity_index_bicycle_defridmil_davgm',
                    con = engine,
                    index = False,
                    if_exists = 'append'
                    )   

        print(date_type, 'davgm:', dfff.shape[0])    
