#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 21:16:32 2020

@author: xin-yi.song
"""

# 根据type和coordinates字段，将DataFrame转为GeoDataFrame
# 默认坐标系为epsg:4326

import pandas as pd
import geopandas as gpd
from shapely.geometry import shape

def construct_gdf_from_df(df:pd.DataFrame, type_fieldname:str, coordinates_fieldname:str, input_crs='+init=epsg:4326'):
    
    features_list = []
    for index,i in df.iterrows():
        geometry_dict={}
        geometry_dict['geometry'] = {}
        geometry_dict['geometry']['type'] = i[type_fieldname]
        geometry_dict['geometry']['coordinates'] = eval(i[coordinates_fieldname])
        features_list.append(geometry_dict)
    geojson_geometry = {}
    geojson_geometry['features'] = features_list
    
    new_geom = gpd.GeoDataFrame()
    for i_feature in geojson_geometry['features']:
        gp_geom = shape(i_feature['geometry'])
        temp_geom = gpd.GeoDataFrame(index=[0], crs=input_crs, geometry=[gp_geom])
        new_geom = new_geom.append(temp_geom,ignore_index= True)
    
    gdf = gpd.GeoDataFrame(df, geometry=new_geom.geometry)
    
    return gdf