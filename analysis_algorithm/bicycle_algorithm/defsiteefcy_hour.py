#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 10:12:29 2020

@author: xin-yi.song
"""

import pandas as pd
import datetime
import time
#%matplotlib qt5

################################连接到RDS#######################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'db_sr_transport','utf8mb4'))

################################导入站点数据#####################################
#导入站点信息，为gcj坐标
with engine.connect() as conn:
    query = 'select sr_site_code, sr_site_cname, sr_site_pilenum, sr_region_code, sr_defregion_code, sr_site_lon, sr_site_lat from srcity_bt_bicyclesiteinfo'
    sta_info = pd.read_sql(query, engine)
    
#转化为dataframe格式
sta_info = pd.DataFrame(sta_info)
sta_info.columns = ['site_code','site_cname','site_pile','region_code','defregion_code','lon','lat']
#找出中医院站点
zhongyiyuan = list(sta_info[sta_info['defregion_code']=='sr005']['site_code'])
#找出拙政园站点
zhuozhengyuan = list(sta_info[sta_info['defregion_code']=='sr006']['site_code'])

################################连接到ODPS######################################
from odps import ODPS

odps_config = {
                "access_id": "V6MeotKzDBgOv6vI",
                "access_key": "P7YzxeRMfdr43e22C55ioOmsJP2UNu",
                "project": "csdnsz_dma",
                "endpoint": "http://service.cn-suzhou-szdn-d01.odps.cloud-ops.szdn.suzhou.gov.cn/api"
                }
od = ODPS(
          odps_config.get("access_id"),
          odps_config.get("access_key"), 
          odps_config.get("project"),
          endpoint=odps_config.get("endpoint"))

################################定义时间########################################
#for dd in pd.period_range(start='2020-05-04 09:10:00',end='2020-05-04 09:10:00', freq='H'):
#定义时间，规定在下个小时的第10分钟，计算上个小时的
now = datetime.datetime.now()
#now = datetime.datetime(dd.year, dd.month, dd.day, dd.hour, 10, 0)

begin = datetime.datetime((now-datetime.timedelta(hours=1)).year, 
                          (now-datetime.timedelta(hours=1)).month, 
                          (now-datetime.timedelta(hours=1)).day, 
                          (now-datetime.timedelta(hours=1)).hour, 0, 0)
    
end = begin+datetime.timedelta(hours=1)
        
###############################查询日期属性##################################
'''星期以数字标识:Sunday=0, Monday=1, ..., Saturday=6
    日期类型:1：工作日, 2：休息日, 3：节假日'''  
date_type = {'1':'工作日', '2':'双休日', '3':'节假日'}
        
#查询当天的时间属性
sql = 'select * from traffic_base.dim_date_base' + '\n' \
    + 'where date_list=' + '"' + str(begin.year) + '-' + str(begin.month).zfill(2) + '-' + str(begin.day).zfill(2) + '"'
    
with od.execute_sql(sql).open_reader() as reader:
    date = reader
    date = date.to_pandas()
      
#############################导入该小时借还车数据############################## 
#程序开始时间
start_time = time.time()
    
#按小时导入城管公共自行车扫码借还记录    
sql = 'select * from csdnsz_bas.cg_bike_log' + '\n' \
    + 'where ((jcsk >= ' + '"' + str(begin) + '"' + ' and jcsk < '  + '"' + str(end) + '") ' \
    + 'or (ghsk >= '+ '"' + str(begin) + '"' + ' and ghsk < ' + '"' + str(end) + '"))'+ '\n' \
    + 'and ghsk > jcsk' +'\n' 
    #+ 'and (jczh in ' + str(tuple(zhongyiyuan)) +' or hczh in ' + str(tuple(zhongyiyuan)) + ')'
    
with od.execute_sql(sql).open_reader() as reader:
    data = reader
    data = data.to_pandas()
    
##用空数据进行尝试
#data = pd.DataFrame(columns=['id', 'ch', 'sjh', 'jczh', 'jcw', 'jcsk', 'hczh', 'hcw', 'ghsk', 'kxh','update_time', 'dt'])
    
#调整格式，并计算骑行时间  
data.loc[:,'jcsk'] = data.loc[:,'jcsk'].astype('datetime64')
data.loc[:,'ghsk'] = data.loc[:,'ghsk'].astype('datetime64')
#data.loc[:,'duration'] = (data['ghsk'] - data['jcsk']).dt.total_seconds()/60

'''
清洗规则：
0. 挑选手机号、卡序号无缺失的数据
1. 挑选车号无缺失的数据
2. 挑选借、还车站号无缺失的数据
3. 挑选借车站号不为0 & 还车站号不为0 & 还车站号不为999999
'''
#构建清洗条件
cond0 = (data.loc[:,'sjh'].notna()) & (data.loc[:,'kxh'].notna())
cond1 = data.loc[:,'ch'].notna()
cond2 = (data.loc[:,'jczh'].notna()) & (data.loc[:,'hczh'].notna())
cond3 = (data.loc[:,'jczh'] != '0') & (data.loc[:,'hczh'] != '0') & (data.loc[:,'hczh'] != '999999')   
#构建新数据
data = data[cond0 & cond1 & cond2 & cond3]
data = data[['jczh','jcsk','hczh','ghsk','sjh','kxh','ch']]
    
##############################按站点计算借还量################################
#不管数据是否为空，都可计算借还量
tmp_rent = data[(data['jcsk'].dt.hour==begin.hour) & (data['jczh'].isin(list(sta_info['site_code'])))]
tmp_retu = data[(data['ghsk'].dt.hour==begin.hour) & (data['hczh'].isin(list(sta_info['site_code'])))]
            
rent = tmp_rent.groupby(by=['jczh']).size().reset_index(name='rent_count')
retu = tmp_retu.groupby(by=['hczh']).size().reset_index(name='retu_count')
rent_user = tmp_rent.drop_duplicates(subset=['jczh','sjh','kxh'], keep='last')
retu_user = tmp_retu.drop_duplicates(subset=['hczh','sjh','kxh'], keep='last')
user = pd.DataFrame(data=list(sta_info['site_code']), columns=['czh'])
user['user'] = user['czh'].apply(lambda x: rent_user[rent_user['jczh']==x].shape[0] \
                                                 + retu_user[retu_user['hczh']==x].shape[0])
    
################################读取点位状态#################################
#查询该小时的城管公共自行车点位状态实时数据
sql = 'select czh,czmc,cws,xxwzms,lon,lat,kjs,update_time from csdnsz_bas.cg_bike_state' + '\n' \
    + 'where update_time ' + '>= datetime(' + '"' + str(begin) + '")' + '\n' \
    + 'and update_time ' + '< datetime(' + '"' + str(end) + '")'+ '\n' 
    #+ 'and czh in ' + str(tuple(zhongyiyuan))
        
with od.execute_sql(sql).open_reader() as reader:
    site = reader
    site = site.to_pandas()
        
############################按站点计算无车可借/无位可还########################
#不管数据是否为空，都可计算无车可借/无位可还
norent = site[site['kjs']=='0'].groupby(by ='czh').size().reset_index(name='norent')
noretu = site[site['cws']==site['kjs']].groupby(by ='czh').size().reset_index(name='noretu')
        
###################导入苏州项目数据库，运行效率评价#############################
#按字段顺序准备要导入的数据表
df = pd.DataFrame()
df['sr_site_code'] = list(sta_info['site_code'])
df['sr_defregion_code'] = df['sr_site_code'].apply(lambda x: 'sr005' if x in zhongyiyuan 
                                                       else ('sr006' if x in zhuozhengyuan else '0'))
df['sr_stat_year'] = str(begin.year)
df['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
df['sr_stat_date'] = str(begin.year) + '-' + str(begin.month).zfill(2) + '-' + str(begin.day).zfill(2)
df['sr_stat_hour'] = str(begin.hour)
df['sr_date_type'] = date_type[date['date_type'][0]]
df['sr_site_userperhour'] = user['user']
df['sr_site_rentperhour'] = pd.merge(df, rent, left_on='sr_site_code', right_on='jczh', how='left')['rent_count']
df['sr_site_returnperhour'] = pd.merge(df, retu, left_on='sr_site_code', right_on='hczh', how='left')['retu_count']
df['sr_site_norentperhour'] = pd.merge(df, norent, left_on='sr_site_code', right_on='czh', how='left')['norent']
df['sr_site_noreturnperhour'] = pd.merge(df, noretu, left_on='sr_site_code', right_on='czh', how='left')['noretu']
df['sr_insertedby'] = 'Xin-Yi Song'
df['sr_updatedby'] = 'Xin-Yi Song'
df = df.fillna(0)
   
with engine.connect() as conn:
    #conn.execute('delete from srcity_index_bicycle_defsiteefcy_hour')
    df.to_sql(
            name = 'srcity_index_bicycle_defsiteefcy_hour',
            con = engine,
            index = False,
            if_exists = 'append'
            )
    
#程序结束时间
over_time = time.time() 
#程序运行时间
total_time = over_time - start_time
#打印耗费时间
print(begin,'~',end,':',total_time,'data:',data.shape[0])
