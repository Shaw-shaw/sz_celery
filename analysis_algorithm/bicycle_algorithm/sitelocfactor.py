#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 13:23:11 2020

@author: xin-yi.song
"""

import datetime
import time
import os
import sys
import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
from scipy.spatial import cKDTree
from osgeo import gdal
from sklearn.ensemble import RandomForestRegressor

#定义时间，规定在每个月第一天的01：00用重新计算出来的未覆盖区域影响因素进行回归
#设置的begin,end为获取自行车借还数据的时间范围
now = datetime.datetime.now()
#now = datetime.datetime(2020, 5, 1, 1, 0, 0)   
begin = datetime.datetime((now-datetime.timedelta(days=1)).year, 
                          (now-datetime.timedelta(days=1)).month, 
                           1, 0, 0, 0)
end = datetime.datetime((now-datetime.timedelta(days=1)).year, 
                          (now-datetime.timedelta(days=1)).month, 
                          (now-datetime.timedelta(days=1)).day,
                           0, 0, 0)

#程序开始时间
start_time = time.time() 

###################################计算X########################################
################################连接RDS#########################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'testdb_suzhou_analysis','utf8mb4'))

################################导入自行车站点###################################
#导入站点信息，为gcj坐标
with engine.connect() as conn:
    query = 'select sr_site_code, sr_site_cname, sr_site_pilenum, sr_defregion_code, sr_site_lon, sr_site_lat from srcity_bt_bicyclesiteinfo'
    sta_info = pd.read_sql(query, engine)
    
#转化为dataframe格式
sta_info = pd.DataFrame(sta_info)
sta_info.columns = ['site_code','site_cname','site_pile','defregion_code','lon','lat']
#将经纬度组合成Point
sta_info['geometry'] = sta_info.apply(lambda x: Point(x['lon'],x['lat']),axis=1)
sta_info = gpd.GeoDataFrame(sta_info, geometry=sta_info['geometry'], crs='epsg:4326')
sta_info.to_crs('epsg:32651', inplace=True)
#专门设置一列记录投影坐标的array格式
sta_info.loc[:,'array'] = sta_info.apply(lambda x: np.array(x['geometry']), axis=1)
nodes_sta = np.array(list(sta_info['array']))
#以站点为中心做200米的buffer
buffers = sta_info.buffer(200)
sta_buffer = sta_info[['site_code','site_cname','site_pile','defregion_code','lon','lat']]
sta_buffer = gpd.GeoDataFrame(sta_buffer, geometry=buffers, crs='epsg:32651')

################################连接到ODPS######################################
from odps import ODPS

odps_config = {
                "access_id": "LZR4s9cdwJdi82ID",
                "access_key": "jbelpqOhlBNDG4Wv6ILdOty0jaccO8",
                "project": "csdnsz_dma",
                "endpoint": "http://service.cn-suzhou-szdn-d01.odps.cloud-ops.szdn.suzhou.gov.cn/api"
                }
od = ODPS(
          odps_config.get("access_id"),
          odps_config.get("access_key"), 
          odps_config.get("project"),
          endpoint=odps_config.get("endpoint"))

###################################读取轨道站点##################################
#从大脑中读取轨道站点数据,DIM中的数据为gcj02坐标
sql = 'select * from traffic_base.dim_metro_lineinfo'

with od.execute_sql(sql).open_reader() as reader:
    subway = reader
    subway = subway.to_pandas()
    
#将经度、纬度转化为Point
subway['geometry'] = subway.apply(lambda x: Point(x['jd'],x['wd']), axis=1)
subway = gpd.GeoDataFrame(subway, geometry=subway['geometry'], crs='epsg:4326')
subway.to_crs('epsg:32651', inplace=True)
#专门设置一列记录投影坐标的array格式
subway.loc[:,'array'] = subway.apply(lambda x: np.array(x['geometry']), axis=1)
#将新站点转化为节点
nodes_sub = np.array(list(subway['array']))

#用ckdtree找到离站点最近的地铁站
btree = cKDTree(nodes_sub)
dist_sub, idx_sub = btree.query(nodes_sta, k=1)

#在sta_buffer中记录dist_sub和name_sub
sta_buffer.loc[:,'dist_sub'] = dist_sub
sta_buffer.loc[:,'name_sub'] = list(subway.loc[list(idx_sub),'czmc'])

################################导入公交站点#####################################
#从大脑中读取公交站点数据,DIM中的数据为gcj02坐标
sql = 'select distinct zdmc, jd, wd from traffic_base.dim_bus_standinfo'

with od.execute_sql(sql).open_reader() as reader:
    bus = reader
    bus = bus.to_pandas()

#将经度、纬度转化为Point
bus['geometry'] = bus.apply(lambda x: Point(x['jd'],x['wd']), axis=1)
bus = gpd.GeoDataFrame(bus, geometry=bus['geometry'], crs='epsg:4326')
bus.to_crs('epsg:32651', inplace=True)

#跟sta_buffer做相交
tmp = gpd.sjoin(sta_buffer, bus, how="left", op='contains')
tmp = tmp.groupby(by='site_code',as_index=True).size().reset_index(name='bus_count')
sta_buffer = pd.merge(sta_buffer, tmp, how='left', on='site_code')

###############################导入停车场位置####################################
#从大脑中读取停车场数据,DIM中的数据为gcj02坐标
sql = 'select * from traffic_base.dim_parking_siteinfo'

with od.execute_sql(sql).open_reader() as reader:
    park = reader
    park = park.to_pandas()
    
#将经度、纬度转化为Point
park['geometry'] = park.apply(lambda x: Point(float(x['longitude']),float(x['latitude'])), axis=1)
park = gpd.GeoDataFrame(park, geometry=park['geometry'], crs='epsg:4326')
park.to_crs('epsg:32651', inplace=True)

#跟sta_buffer做相交
tmp = gpd.sjoin(sta_buffer, park, how="left", op='contains')
tmp = tmp.groupby(by='site_code',as_index=True).size().reset_index(name='park_count')
sta_buffer = pd.merge(sta_buffer, tmp, how='left', on='site_code')

###############################导入POI##########################################
#从文件中导入poi，不以code区分类型，直接以retype区分类型
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
SOURCE_DATA_DIR = os.path.join(BASE_DIR, 'source_data','suzhou_poi_alltypes_gcj_20200413.geojson')
poi = gpd.read_file(SOURCE_DATA_DIR)

#将地理坐标转化为投影坐标
poi.to_crs('epsg:32651', inplace=True)

#列出所有类型
retypes = list(poi['retype'].unique())
#依次跟sta_buffer做相交
for retype in retypes:
    tmp = gpd.sjoin(sta_buffer, poi[poi['retype']==retype], how="left", op='contains')
    tmp = tmp.groupby(by='site_code',as_index=True).size().reset_index(name=retype + '_count')
    sta_buffer = pd.merge(sta_buffer, tmp, how='left', on='site_code')
    
##################################导入人口数据###################################
#从RDS中导入基站人口数据
with engine.connect() as conn:
    query = 'select * from srcity_ft_popdstr'
    pop = pd.read_sql(query, engine)
    
#调用construct_geodataframe函数，将geojson转换成geometry
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from construct_geodataframe import construct_gdf_from_df
pop = construct_gdf_from_df(pop,'sr_pop_pointspatialtype','sr_pop_pointgc')

#在QGIS中做一张核密度栅格图，重新投影坐标系为32651，横坐标像素大小为200，纵坐标大小像素为200，该操作是为了得到栅格图尺寸
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
SOURCE_DATA_DIR = os.path.join(BASE_DIR, 'source_data','pop_raster.tif')
dataset = gdal.Open(SOURCE_DATA_DIR)  
dataset.GetProjection()#读取TIFF文件坐标系

#得到基站点对应的栅格范围
spatial_range = dataset.GetGeoTransform()#读取仿射变换矩阵，共有六个参数，描述的是栅格行列号和地理坐标之间的关系
row_col_shape = (dataset.RasterYSize, dataset.RasterXSize)#栅格像元数，YSize为行数，XSize为列数
band_width = 2500#带宽
     
#调用函数文件进行核密度估计
from pop_density_200 import create_train_X, construct_grids, pop_kde

for pop_field in ['sr_pop_resdntnum','sr_pop_worknum']:
    
    train_X, sample_weight = create_train_X(pop, pop_field)
    xy = construct_grids(spatial_range, row_col_shape)
    pop_in_pixel_32651 = pop_kde(spatial_range, row_col_shape, band_width, xy, 
                                 train_X, sample_weight, pop_field)
    #专门设置一列记录投影坐标的array格式
    pop_in_pixel_32651.loc[:,'array'] = pop_in_pixel_32651.apply(lambda x: np.array(x['geometry']), axis=1)
    #将新站点转化为节点
    nodes_pop = np.array(list(pop_in_pixel_32651['array']))

    #用ckdtree找到离站点最近的栅格点
    btree = cKDTree(nodes_pop)
    dist_pop, idx_pop = btree.query(nodes_sta, k=1)

    #在sta_buffer中记录gongzuo的population
    sta_buffer.loc[:,'pop_'+pop_field.split('_')[-1]] = list(pop_in_pixel_32651.loc[list(idx_pop),'population'])

###################################计算Y########################################
##########################导入RDS中上个月按小时借还车数据###########################
with engine.connect() as conn:
    query = 'select * from srcity_index_bicycle_defsiteefcy_hour' + '\n' \
          + 'where sr_stat_month=' + '"' + str(begin.year) + '-' + str(begin.month).zfill(2) + '"'
    data = pd.read_sql(query, engine)
    
#按日期计算每个站点的高峰借还车量
tmp = data.groupby(by=['sr_stat_date','sr_date_type','sr_site_code']) \
     [['sr_site_rentperhour','sr_site_returnperhour']].max().reset_index()
#增加字段记录0-1变量的特征日
tmp['workday'] = tmp['sr_date_type'].apply(lambda x: 1 if x=='工作日' else 0)
tmp['weekend'] = tmp['sr_date_type'].apply(lambda x: 1 if x=='双休日' else 0)
tmp['holiday'] = tmp['sr_date_type'].apply(lambda x: 1 if x=='节假日' else 0)

#拼接XY表
xy = pd.merge(sta_buffer, tmp, how='inner', left_on='site_code', right_on='sr_site_code')

#############################按照自定义区域进行计算############################
#分别针对中医院、拙政园选取数据
for j in ['sr005','sr006']:

    ##################################regression###################################
    #导入未覆盖区域影响因子
    with engine.connect() as conn:
        query = 'select * from srcity_index_bicycle_sitelocfactor_nocvr' + '\n' \
              + 'where sr_defregion_code="' + j + '"'
        block_no = pd.read_sql(query, engine)
    block_no.rename(columns={'sr_defblock_gridworkpop':'pop_worknum',#工作人口
                             'sr_defblock_gridrespop':'pop_resdntnum',#居住人口
                             'sr_defblock_bizpoi':'商务办公_count',#商务办公poi数
                             'sr_defblock_servepoi':'商业服务_count',#商业服务poi数
                             'sr_defblock_leipoi':'文体休闲_count',#文体休闲poi数
                             'sr_defblock_medpoi':'医疗设施_count',#医疗设施poi数
                             'sr_defblock_schoolpoi':'学校_count',#学校poi数
                             'sr_defblock_adminnum':'行政办公_count',#行政办公poi数
                             'sr_defblock_parkpoi':'公园_count',#公园poi数
                             'sr_defblock_busstopnum':'bus_count',#公交站数量
                             'sr_defblock_subwaydist':'dist_sub',#地铁站距离
                             'sr_defblock_parkingnum':'park_count'}, inplace=True)#停车场数量 
                                 
    #添加workday, weekday
    block_no_1 = block_no.copy(); block_no_1['workday']= 1; block_no_1['weekend']= 0; block_no_1['holiday']= 0;
    block_no_2 = block_no.copy(); block_no_2['workday']= 0; block_no_2['weekend']= 1; block_no_2['holiday']= 0;
    block_no_3 = block_no.copy(); block_no_3['workday']= 0; block_no_3['weekend']= 0; block_no_3['holiday']= 1;
    block_no = block_no_1.append([block_no_2,block_no_3], ignore_index=True)
    
    #按借车数量、还车数量进行回归
    for y in ['sr_site_rentperhour', 'sr_site_returnperhour']:
        #准备X_train, y_train
        X_train = xy[['dist_sub','bus_count', 'park_count', '商务办公_count','商业服务_count', '文体休闲_count', 
                         '医疗设施_count', '学校_count', '行政办公_count', '公园_count', 'pop_worknum', 'pop_resdntnum',
                         'workday', 'weekend', 'holiday']]
        y_train = xy[y]
        #准备X_predict
        X_predict = block_no[['dist_sub','bus_count', 'park_count', '商务办公_count','商业服务_count', '文体休闲_count', 
                              '医疗设施_count', '学校_count', '行政办公_count', '公园_count', 'pop_worknum', 'pop_resdntnum',
                              'workday', 'weekend', 'holiday']]
        #随机森林回归
        rf = RandomForestRegressor(n_estimators=100, criterion='mse', n_jobs=-1)
        rf.fit(X_train, y_train)
        
        #进行预测
        y_predict = rf.predict(X_predict)
        block_no[y] = y_predict
    
        #特征重要性选择
        imp = rf.feature_importances_
        imp = pd.DataFrame(data=imp, index=X_train.columns.tolist(), columns=['importance']).reset_index()
        #imp = imp.sort_values(by='importance', ascending=False)
        #imp.plot(kind='bar')  
        locals()['imp_'+y.split('_')[-1]] = imp
    
    #将借车重要度和还车重要度合并
    imp = pd.merge(imp_rentperhour, imp_returnperhour, on='index', how='inner')
    imp = imp[~imp['index'].isin(['workday','weekend','holiday'])]
    imp['per'] = (imp['importance_x'] + imp['importance_y'])/(imp['importance_x'].sum() + imp['importance_y'].sum())
    imp['per'] = imp['per'].apply(lambda x: '{:.2f}'.format(x))
        
    ###################导入苏州项目数据库，未覆盖范围预测结果#############################
    #按字段顺序准备要导入的数据表
    df = pd.DataFrame()
    df['sr_defblock_code'] = block_no['sr_defblock_code'].unique()
    df['sr_defregion_code'] = j
    df['sr_stat_year'] = str(begin.year)
    df['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
    df['sr_defblock_rentnum_work'] = df['sr_defblock_code'].apply(lambda x: block_no[(block_no['workday']==1) & (block_no['sr_defblock_code']==x)]['sr_site_rentperhour'].values[0])#工作日高峰时段借车数量
    df['sr_defblock_returnnum_work'] = df['sr_defblock_code'].apply(lambda x: block_no[(block_no['workday']==1) & (block_no['sr_defblock_code']==x)]['sr_site_returnperhour'].values[0])#工作日高峰时段还车数量
    df['sr_defblock_rentnum_rest'] = df['sr_defblock_code'].apply(lambda x: block_no[(block_no['weekend']==1) & (block_no['sr_defblock_code']==x)]['sr_site_rentperhour'].values[0])#休息日高峰时段借车数量
    df['sr_defblock_returnnum_rest'] = df['sr_defblock_code'].apply(lambda x: block_no[(block_no['weekend']==1) & (block_no['sr_defblock_code']==x)]['sr_site_returnperhour'].values[0])#休息日高峰时段还车数量
    df['sr_defblock_rentnum_hldy'] = df['sr_defblock_code'].apply(lambda x: block_no[(block_no['holiday']==1) & (block_no['sr_defblock_code']==x)]['sr_site_rentperhour'].values[0])#节假日高峰时段借车数量
    df['sr_defblock_returnnum_hldy'] = df['sr_defblock_code'].apply(lambda x: block_no[(block_no['holiday']==1) & (block_no['sr_defblock_code']==x)]['sr_site_returnperhour'].values[0]) #节假日高峰时段还车数量
    df['sr_defblock_recpilemin'] = df.apply(lambda x: round((x['sr_defblock_rentnum_work'] + x['sr_defblock_returnnum_work'])/0.35*0.8), axis=1)#推荐桩位数最小值，高峰小时周转率以0.35计，最小值为计算值的80%
    df['sr_defblock_recpilemax'] = df.apply(lambda x: round((x['sr_defblock_rentnum_work'] + x['sr_defblock_returnnum_work'])/0.35*1.2), axis=1)#推荐桩位数最大值，高峰小时周转率以0.35计，最大值为计算值的120%
    
    with engine.connect() as conn:
        for block in list(df['sr_defblock_code']):
            tmp = df[df['sr_defblock_code']==block].iloc[0,:]
            sql = 'update srcity_index_bicycle_sitelocfactor_nocvr' +'\n' \
                + 'set sr_defblock_rentnum_work=' + str(tmp['sr_defblock_rentnum_work']) + ',' + '\n' \
                + 'sr_defblock_returnnum_work=' + str(tmp['sr_defblock_returnnum_work']) + ',' + '\n' \
                + 'sr_defblock_rentnum_rest=' + str(tmp['sr_defblock_rentnum_rest']) + ',' + '\n' \
                + 'sr_defblock_returnnum_rest=' + str(tmp['sr_defblock_returnnum_rest']) + ',' + '\n' \
                + 'sr_defblock_rentnum_hldy=' + str(tmp['sr_defblock_rentnum_hldy']) + ',' + '\n' \
                + 'sr_defblock_returnnum_hldy=' + str(tmp['sr_defblock_returnnum_hldy']) + ',' + '\n' \
                + 'sr_defblock_recpilemin=' + str(tmp['sr_defblock_recpilemin']) + ',' + '\n' \
                + 'sr_defblock_recpilemax=' + str(tmp['sr_defblock_recpilemax']) + ',' + '\n' \
                + 'sr_insertedby="Xin-Yi Song"' + ',' + '\n' \
                + 'sr_updatedby="Xin-Yi Song"' + '\n' \
                + 'where sr_defblock_code=' + '"' + block + '" ' \
                + 'and sr_stat_month=' + '"' + str(begin.year) + '-' + str(begin.month).zfill(2) + '";'
            conn.execute(sql)
    
    ###################导入苏州项目数据库，影响因子排序#############################
    #按字段顺序准备要导入的数据表
    keys = ['dist_sub','bus_count','park_count','商务办公_count','商业服务_count','文体休闲_count','医疗设施_count',        
            '学校_count','行政办公_count','公园_count','pop_worknum','pop_resdntnum']        
    values = ['地铁站距离','公交站数量','停车场数量','商务办公poi数','商业服务poi数','文体休闲poi数','医疗设施poi数',
              '学校poi数','行政办公poi数','公园poi数','工作人口','居住人口']
    factors = dict(zip(keys,values))
    dff = imp[['index','per']].copy()
    dff.rename(columns={'index':'sr_sitefactor','per':'sr_sitefactorvalue'},inplace=True)
    dff['sr_sitefactor'] = dff['sr_sitefactor'].apply(lambda x: factors[x])
    dff['sr_defregion_code'] = j
    dff['sr_stat_year'] = str(begin.year)
    dff['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
    dff['sr_insertedby'] = 'Xin-Yi Song'
    dff['sr_updatedby'] = 'Xin-Yi Song'
    
    with engine.connect() as conn:
        #conn.execute('delete from srcity_index_bicycle_sitelocfactor')
        dff.to_sql(
                  name = 'srcity_index_bicycle_sitelocfactor',
                  con = engine,
                  index = False,
                  if_exists = 'append'
                  )
    
    #程序结束时间
    over_time = time.time() 
    #程序运行时间
    total_time = over_time - start_time
    #打印耗费时间
    print(begin,'~',end,':',total_time,'data:',data.shape[0])
    
    
