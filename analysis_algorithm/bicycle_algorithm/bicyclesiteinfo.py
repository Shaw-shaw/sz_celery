#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 16:20:35 2020

@author: xin-yi.song
"""

import json
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point,Polygon
import datetime
import time

#定义时间，规定在每个月第一天的00：00计算上一个月的数据
now = datetime.datetime.now()
#now = datetime.datetime(2020, 7, 1, 0, 0, 0)   
begin = datetime.datetime((now-datetime.timedelta(days=1)).year, 
                          (now-datetime.timedelta(days=1)).month, 
                           1, 0, 0, 0)
end = datetime.datetime(now.year, now.month, now.day, 0, 0, 0)

#程序开始时间
start_time = time.time() 
################################连接到ODPS######################################
from odps import ODPS


odps_config = {
                "access_id": "LZR4s9cdwJdi82ID",
                "access_key": "jbelpqOhlBNDG4Wv6ILdOty0jaccO8",
                "project": "csdnsz_dma",
                "endpoint": "http://service.cn-suzhou-szdn-d01.odps.cloud-ops.szdn.suzhou.gov.cn/api"
                }
od = ODPS(
          odps_config.get("access_id"),
          odps_config.get("access_key"), 
          odps_config.get("project"),
          endpoint=odps_config.get("endpoint"))

###################################读取点位状态##################################
##获取最新一个月的日期
#sql = 'select max(update_time) as max_time from csdnsz_bas.cg_bike_state'
#
#with od.execute_sql(sql).open_reader() as reader:
#    data = reader
#    data = data.to_pandas()
#    data = pd.to_datetime(data['max_time'])[0]

#查询最新一个月的城管公共自行车点位状态实时数据（按站号、时间排序）
sql = 'select czh,czmc,cws,xxwzms,lon,lat,max(update_time) as update_time from csdnsz_bas.cg_bike_state' + '\n' \
    + 'where update_time ' + '>= datetime(' + '"' + str(begin) + '")' + '\n' \
    + 'and update_time ' + '< datetime(' + '"' + str(end) + '")'+ '\n' \
    + 'and lon >"120" and lat >"30"' + '\n' \
    + 'group by czh,czmc,cws,xxwzms,lon,lat' + '\n' \
    + 'order by cast(czh as int),update_time'

   
with od.execute_sql(sql).open_reader() as reader:
    data = reader
    data = data.to_pandas()

###############################站点信息预处理####################################
#当站号相同时，保留更新时间更晚的记录
sta_info = data.drop_duplicates(subset=['czh'],keep='last')   
sta_info = sta_info[['czh','czmc','cws','xxwzms','lon','lat']]

#将站点信息修改为带坐标格式
geom = [Point(xy) for xy in zip(sta_info.lon, sta_info.lat)]
sta_info = gpd.GeoDataFrame(sta_info, geometry=geom)
sta_info.crs = ({'init':'epsg:4326'})
sta_info.to_crs('epsg:32651', inplace=True)

################################连接RDS#########################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'db_sr_transport','utf8mb4'))

################################读取中医院信息###################################
#导入中医院边界信息
with engine.connect() as conn:
    query = 'select sr_defregion_code, sr_defregion_cname, sr_defregion_boundarycns from srcity_bt_defregioninfo' + '\n' \
            'where sr_defregion_cname="中医院"'
    zhongyiyuan = pd.read_sql(query, engine)

#重命名
zhongyiyuan.columns = ['defregion_code','defregion_name','defregion_geojson']
#将geojson的中括号转化为polygon
zhongyiyuan['geometry'] = zhongyiyuan['defregion_geojson'].apply(lambda x: [tuple(i) for i in json.loads(x)[0]])
zhongyiyuan['geometry'] = zhongyiyuan['geometry'].apply(lambda x: Polygon(x))
zhongyiyuan = gpd.GeoDataFrame(zhongyiyuan, geometry=zhongyiyuan['geometry'], crs={'init':'epsg:4326'})
zhongyiyuan.to_crs('epsg:32651', inplace=True)
#与sta_info计算距离
sta_info['distance_zhongyiyuan'] = sta_info.geometry.apply(lambda x: gpd.GeoSeries(x).distance(zhongyiyuan))
sta_info.loc[sta_info['distance_zhongyiyuan']<=150,'defregion_code'] = 'sr005'
sta_info['defregion_code'] = sta_info['defregion_code'].fillna('0')
##通过绘图来检查
#zhongyiyuan.plot(facecolor='none', edgecolor='black')

################################读取拙政园信息###################################
#导入拙政园边界信息
with engine.connect() as conn:
    query = 'select sr_defregion_code, sr_defregion_cname, sr_defregion_boundarycns from srcity_bt_defregioninfo' + '\n' \
            'where sr_defregion_cname="拙政园"'
    zhuozhengyuan = pd.read_sql(query, engine)

#重命名
zhuozhengyuan.columns = ['defregion_code','defregion_name','defregion_geojson']
#将geojson的中括号转化为polygon
zhuozhengyuan['geometry'] = zhuozhengyuan['defregion_geojson'].apply(lambda x: [tuple(i) for i in json.loads(x)[0]])
zhuozhengyuan['geometry'] = zhuozhengyuan['geometry'].apply(lambda x: Polygon(x))
zhuozhengyuan = gpd.GeoDataFrame(zhuozhengyuan, geometry=zhuozhengyuan['geometry'], crs={'init':'epsg:4326'})
zhuozhengyuan.to_crs('epsg:32651', inplace=True)
#与sta_info计算距离
sta_info['distance_zhuozhengyuan'] = sta_info.geometry.apply(lambda x: gpd.GeoSeries(x).distance(zhuozhengyuan))
sta_info.loc[sta_info['distance_zhuozhengyuan']<=40,'defregion_code'] = 'sr006'
sta_info['defregion_code'] = sta_info['defregion_code'].fillna('0')
##通过绘图来检查
#base = zhuozhengyuan.plot(facecolor='none', edgecolor='black')
#sta_info[sta_info['defregion_code']=='sr006'].plot(ax = base)

###################################读取行政区划信息##############################
#导入行政区信息
with engine.connect() as conn:
    query = 'select sr_region_code, sr_region_cname, sr_region_boundarycns from srcity_bt_regioninfo'
    region = pd.read_sql(query, engine)
    
#重命名
region.columns = ['region_code','region_name','region_geojson']
#将geojson的中括号转化为polygon
region['geometry'] = region['region_geojson'].apply(lambda x: [tuple(i) for i in json.loads(x)[0][0]])
region['geometry'] = region['geometry'].apply(lambda x: Polygon(x))
region = gpd.GeoDataFrame(region, geometry=region['geometry'])
region.crs=({'init':'epsg:4326'})
region.to_crs('epsg:32651', inplace=True)
#与sta_info相交
sta_info = gpd.sjoin(sta_info, region, how='inner', op='within')
sta_info.drop(columns=['index_right','region_geojson'], inplace=True)
sta_info.reset_index(drop=True, inplace=True)
##通过绘图来检查
#region.plot(facecolor='none', edgecolor='black')

###################################读取建筑功能信息###############################
#导入自定义区域的建筑功能
with engine.connect() as conn:
    query = 'select sr_bldgfunc, sr_bldgfunc_boundarycns from srcity_bt_bldgfuncinfo' + '\n' \
            'where sr_defregion_code="sr005" or sr_defregion_code="sr006"'
    landuse = pd.read_sql(query, engine)
    
#重命名
landuse.columns = ['bldgfunc','landuse_geojson']
#将geojson的中括号转化为polygon
landuse['geometry'] = landuse['landuse_geojson'].apply(lambda x: [tuple(i) for i in json.loads(x)[0]])
landuse['geometry'] = landuse['geometry'].apply(lambda x: Polygon(x))
landuse = gpd.GeoDataFrame(landuse, geometry=landuse['geometry'], crs={'init':'epsg:4326'})
landuse.to_crs('epsg:32651', inplace=True)

#与sta_info计算距离
for i in range(0, sta_info.shape[0]):
    landuse['distance'] = landuse['geometry'].apply(lambda x: x.distance(sta_info.loc[i,'geometry']))
    sta_info.loc[i,'bldgfunc'] = landuse.loc[landuse['distance'].idxmin(),'bldgfunc']
#删除自定义区域外的建筑功能类型
sta_info.loc[sta_info['defregion_code']=='0','bldgfunc'] = None

###############################导入苏州项目数据库#################################
#按字段顺序准备要导入的数据表
df = pd.DataFrame()
df['sr_site_code'] = sta_info['czh']
df['sr_site_cname'] = sta_info['czmc']
df['sr_site_lon'] = sta_info['lon']
df['sr_site_lat'] = sta_info['lat']
df['sr_spatial_datatype'] = 'Point'
df['sr_site_pilenum'] = sta_info['cws']
df['sr_site_format'] = sta_info['bldgfunc']
df['sr_region_code'] = sta_info['region_code']
df['sr_defregion_code'] = sta_info['defregion_code']
df['sr_insertedby'] = 'Xin-Yi Song'
df['sr_updatedby'] = 'Xin-Yi Song'

with engine.connect() as conn:
    conn.execute('delete from srcity_bt_bicyclesiteinfo')
    df.to_sql(
              name = 'srcity_bt_bicyclesiteinfo',
              con = engine,
              index = False,
              if_exists = 'append'
              )

#程序结束时间
over_time = time.time() 
#程序运行时间
total_time = over_time - start_time
#打印耗费时间
print(begin,'~',end,':',total_time)