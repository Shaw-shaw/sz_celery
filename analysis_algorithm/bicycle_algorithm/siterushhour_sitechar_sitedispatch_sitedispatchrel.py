#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 20:01:11 2020

@author: xin-yi.song
"""

import datetime
import time
import pandas as pd
import calendar
from shapely.geometry import Point
import geopandas as gpd

#定义时间，规定在每个月第一天的00：30在计算出的所有站点月度时均借频次中，挑选出自定义区域内的站点
now = datetime.datetime.now()
#now = datetime.datetime(2020, 5, 1, 0, 30, 0)   
begin = datetime.datetime((now-datetime.timedelta(days=1)).year, 
                          (now-datetime.timedelta(days=1)).month, 
                           1, 0, 0, 0)
end = datetime.datetime((now-datetime.timedelta(days=1)).year, 
                          (now-datetime.timedelta(days=1)).month, 
                          (now-datetime.timedelta(days=1)).day,
                           0, 0, 0)

#程序开始时间
start_time = time.time() 
################################连接到RDS#######################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'testdb_suzhou_analysis','utf8mb4'))

################################导入站点数据#####################################
#导入站点信息，为gcj坐标
with engine.connect() as conn:
    query = 'select sr_site_code, sr_defregion_code, sr_site_lon, sr_site_lat from srcity_bt_bicyclesiteinfo'
    sta_info = pd.read_sql(query, engine)
    
#转化为dataframe格式
sta_info = pd.DataFrame(sta_info)
#将经纬度组合成Point
geom = [Point(xy) for xy in zip(sta_info.sr_site_lon, sta_info.sr_site_lat)]
sta_info = gpd.GeoDataFrame(sta_info, geometry=geom)
sta_info.crs = 'epsg:4326'
sta_info.to_crs('epsg:32651', inplace=True)

#############################按照自定义区域进行计算############################
#分别针对中医院、拙政园选取数据
for j in ['sr005','sr006']:

    ###########################导入月度时均站点运行效率评价##############################
    #导入上个月的数据
    with engine.connect() as conn:
        query = 'select * from srcity_index_bicycle_defsiteefcy_havgm' + '\n' \
              + 'where sr_defregion_code="' + j + '"' + '\n' \
              + 'and sr_stat_month=' + '"' + str(begin.year) + '-' + str(begin.month).zfill(2) + '"'
        efcy_havgm = pd.read_sql(query, engine)
    
    efcy_havgm.rename(columns={'sr_site_userhavgm':'sr_site_rentperhour','sr_site_renthavgm':'sr_site_returnperhour'}, inplace=True) 
    
    ###############################导入站点实时调度需求表##############################
    #导入上个月的数据
    with engine.connect() as conn:
        query = 'select * from srcity_index_bicycle_sitedispatchdmd_hour' + '\n' \
              + 'where sr_defregion_code="' + j + '"' + '\n' \
              + 'and sr_stat_month=' + '"' + str(begin.year) + '-' + str(begin.month).zfill(2) + '"'
        dmd_hour = pd.read_sql(query, engine)
    
    dmd_havgm = dmd_hour.groupby(by=['sr_date_type','sr_site_code','sr_stat_hour'])[['sr_site_remainpile', 'sr_site_pilettl']].mean().reset_index()
    
    ###################导入苏州项目数据库，高峰时段借还车频次#############################
    #按字段顺序准备要导入的数据表
    df =  efcy_havgm.drop(columns=['id','sr_site_returnhavgm','sr_site_norenthavgm','sr_insertedat','sr_updatedat'])  
            
    with engine.connect() as conn:
        #conn.execute('delete from srcity_index_bicycle_siterushhour')
        df.to_sql(
                  name = 'srcity_index_bicycle_siterushhour',
                  con = engine,
                  index = False,
                  if_exists = 'append'
                  )
    
    ############################导入苏州项目数据库，静态调度表##########################
    #按日期类型分别计算
    for date_type in ['节假日','双休日','工作日']:#date_type = '工作日'
        
        #挑出特征日的借还数据,计算调度时间
        efcy_tmp = efcy_havgm[efcy_havgm['sr_date_type']==date_type]
        efcy_tmp_tmp = pd.DataFrame()
        dmd_tmp = pd.DataFrame()
        
        if len(efcy_tmp.index)!=0:        
            #计算借车高峰时段    
            rent = efcy_tmp.groupby(by='sr_site_code')['sr_site_rentperhour'].max().reset_index()
            rent['sr_stat_hour'] = rent.apply(lambda x: efcy_tmp[(efcy_tmp['sr_site_code']==x['sr_site_code']) 
                                                               & (efcy_tmp['sr_site_rentperhour']==x['sr_site_rentperhour'])] 
                                                                          ['sr_stat_hour'].min(), axis=1)
            #计算还车高峰时段
            retu = efcy_tmp.groupby(by='sr_site_code')['sr_site_returnperhour'].max().reset_index()
            retu['sr_stat_hour'] = retu.apply(lambda x: efcy_tmp[(efcy_tmp['sr_site_code']==x['sr_site_code'])
                                                               & (efcy_tmp['sr_site_returnperhour']==x['sr_site_returnperhour'])]
                                                                          ['sr_stat_hour'].min(), axis=1)
    
            #计算全局调度时间
            dispatchhour_rent = rent['sr_stat_hour'].value_counts().sort_index().idxmax()-1
            dispatchhour_retu = retu['sr_stat_hour'].value_counts().sort_index().idxmax()-1
            dispatchhour = min([dispatchhour_rent, dispatchhour_retu])
    
            #挑出特征日高峰时段的桩位数据,得到高峰时段（即调度时段+1）的站点特征
            dmd_tmp = dmd_havgm[(dmd_havgm['sr_date_type']==date_type) & (dmd_havgm['sr_stat_hour']==dispatchhour+1)]
            efcy_tmp_tmp = efcy_havgm[(efcy_havgm['sr_date_type']==date_type) & (efcy_havgm['sr_stat_hour']==dispatchhour+1)]
            
        #站点特征的评价标准，剩余桩位数/总桩位数>=0.5:日常需调入，其余为日常需调出
        if len(dmd_tmp.index)!=0:
            sitechar = pd.DataFrame()
            sitechar['sr_site_code'] = dmd_tmp['sr_site_code'].unique()     
            sitechar['sr_site_char'] = sitechar['sr_site_code'].apply(lambda x: dmd_tmp[dmd_tmp['sr_site_code']==x]['sr_site_remainpile'].mean()/ \
                                                                                dmd_tmp[dmd_tmp['sr_site_code']==x]['sr_site_pilettl'].mean())     
            sitechar['sr_site_char'] = sitechar['sr_site_char'].apply(lambda x: '日常需调入' if x>=0.5 else '日常需调出')
            sitechar['sr_defregion_code'] = j
            sitechar['sr_stat_year'] = str(begin.year)
            sitechar['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
            sitechar['sr_date_type'] = date_type
            sitechar['sr_insertedby'] = 'Xin-Yi Song'
            sitechar['sr_updatedby'] = 'Xin-Yi Song'
           
            with engine.connect() as conn:
                #conn.execute('delete from srcity_index_bicycle_sitechar')
                sitechar.to_sql(
                              name = 'srcity_index_bicycle_sitechar',
                              con = engine,
                              index = False,
                              if_exists = 'append'
                              )   
    
        #挑出特征日高峰时段的借还数据，计算推荐调度车辆：依据历史数据计算出该站点在高峰时段（即调度时段+1）可能借出的车辆数
        if len(efcy_tmp_tmp.index)!=0:
            dff = pd.merge(rent, retu, on='sr_site_code', how='inner')
            dff = pd.merge(dff, efcy_tmp_tmp[['sr_site_code','sr_site_rentperhour']], on='sr_site_code', how='left')
            dff.rename(columns={'sr_stat_hour_x':'sr_defregion_rushhour_rent',
                                'sr_stat_hour_y':'sr_defregion_rushhour_return',
                                'sr_site_rentperhour_x':'sr_defregion_rushhour_rentrate',
                                'sr_site_returnperhour':'sr_defregion_rushhour_returnrate',
                                'sr_site_rentperhour_y':'dispatchhour'}, inplace=True)
            dff['sr_defregion_optmtime'] = dispatchhour 
            dff['sr_defregion_recminnum'] = dff['dispatchhour'].apply(lambda x: round(x*0.8)) #推荐日常调度车辆最小值      
            dff['sr_defregion_recmaxnum'] = dff['dispatchhour'].apply(lambda x: round(x*1.2)) #推荐日常调度车辆最大值
            days = calendar.monthrange(now.year,now.month)[1]
            dff['sr_defregion_mmrecmin'] = dff['sr_defregion_recminnum'].apply(lambda x: round(x*days)) #月度预计调度车辆最小值
            dff['sr_defregion_mmrecmax'] = dff['sr_defregion_recmaxnum'].apply(lambda x: round(x*days))       #月度预计调度车辆最大值
            dff['sr_defregion_code'] = j
            dff['sr_stat_year'] = str(begin.year)
            dff['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
            dff['sr_date_type'] = date_type
            dff['sr_insertedby'] = 'Xin-Yi Song'
            dff['sr_updatedby'] = 'Xin-Yi Song'
            dff.drop(columns='dispatchhour',inplace=True)
           
            with engine.connect() as conn:
                #conn.execute('delete from srcity_index_bicycle_sitedispatch')
                dff.to_sql(
                            name = 'srcity_index_bicycle_sitedispatch',
                            con = engine,
                            index = False,
                            if_exists = 'append'
                             )   
    
        #挑出特征日高峰时段的桩位数据，计算其余站点的可调入站点和可调出站点
        if len(dmd_tmp.index)!=0:
            #按每个站点计算
            for sta in dmd_tmp['sr_site_code']:
                if len(dmd_tmp[dmd_tmp['sr_site_code']!=sta].index)!=0:
                    #导入苏州项目数据库
                    tmp = pd.merge(dmd_tmp, sta_info[['sr_site_code','geometry']], on='sr_site_code', how='inner')
                    tmp = pd.merge(tmp, sitechar[['sr_site_code','sr_site_char']], on='sr_site_code', how='inner')
                    tmp['kjs'] = tmp['sr_site_pilettl'] - tmp['sr_site_remainpile']
                    dfff = tmp[tmp['sr_site_code']!=sta][['sr_site_code','sr_site_char','geometry']].copy()
                    dfff.rename(columns={'sr_site_code':'sr_site_code_nearby'}, inplace=True)
                    dfff['sr_site_code'] = sta
                    dfff['sr_defregion_code'] = j
                    dfff['sr_stat_year'] = str(begin.year)
                    dfff['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
                    dfff['sr_date_type'] = date_type
                    dfff['sr_site_dist'] = dfff.apply(lambda x: x['geometry'].distance(tmp[tmp['sr_site_code']==sta]['geometry'].tolist()[0]), axis=1)
                    dfff['sr_site_dist'] = dfff['sr_site_dist'].apply(lambda x: round(x))
                    dfff.drop(columns='geometry',inplace=True)
                    dfff['sr_site_callablemax'] = tmp[tmp['sr_site_code']!=sta]['kjs']#可调出车辆最大值
                    dfff['sr_site_callablemin'] = tmp[tmp['sr_site_code']!=sta]['kjs'].apply(lambda x: round(x*0.8))#可调出车辆最小值
                    dfff['sr_site_tunablemax'] = tmp[tmp['sr_site_code']!=sta]['sr_site_remainpile']#可调入车辆最大值
                    dfff['sr_site_tunablemin'] = tmp[tmp['sr_site_code']!=sta]['sr_site_remainpile'].apply(lambda x: round(x*0.8))#可调入车辆最小值
                    dfff['sr_insertedby'] = 'Xin-Yi Song'
                    dfff['sr_updatedby'] = 'Xin-Yi Song'
            
                    with engine.connect() as conn:
                        #conn.execute('delete from srcity_index_bicycle_sitedispatchrel')
                        dfff.to_sql(
                                  name = 'srcity_index_bicycle_sitedispatchrel',
                                  con = engine,
                                  index = False,
                                  if_exists = 'append'
                                  )   
                        
        
        
#程序结束时间
over_time = time.time() 
#程序运行时间
total_time = over_time - start_time
#打印耗费时间
print(begin,'~',end,':',total_time)