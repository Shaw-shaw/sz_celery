#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 14:29:17 2020

@author: xin-yi.song
"""

import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
import datetime

#%matplotlib qt5

#定义时间，规定在下个小时的第10分钟，计算上个小时的数据
now = datetime.datetime.now()
#now = datetime.datetime(2020, 4, 9, 23, 10, 0)
begin = datetime.datetime((now-datetime.timedelta(hours=1)).year, 
                          (now-datetime.timedelta(hours=1)).month, 
                          (now-datetime.timedelta(hours=1)).day, 
                          0, 0, 0)
end = datetime.datetime(now.year, now.month, now.day, now.hour, 0, 0)
if end.hour == 0:
    end_hour = 24
else:
    end_hour = end.hour
                        
################################连接到RDS#####################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'db_sr_transport','utf8mb4'))

################################导入站点数据#####################################
#导入站点信息，为gcj坐标
with engine.connect() as conn:
    query = 'select sr_site_code, sr_site_cname, sr_site_pilenum, sr_region_code, sr_site_lon, sr_site_lat from srcity_bt_bicyclesiteinfo'
    sta_info = pd.read_sql(query, engine)
    
#转化为dataframe格式
sta_info = pd.DataFrame(sta_info)
sta_info.columns = ['site_code','site_cname','site_pile','region_code','lon','lat']
#将经纬度组合成Point
sta_info['geometry'] = sta_info.apply(lambda x: Point(x['lon'],x['lat']),axis=1)
sta_info = gpd.GeoDataFrame(sta_info, geometry=sta_info['geometry'], crs={'init':'epsg:4326'})
sta_info.to_crs({'init':'epsg:32651'}, inplace=True)

################################连接到ODPS######################################
from odps import ODPS

odps_config = {
                "access_id": "LZR4s9cdwJdi82ID",
                "access_key": "jbelpqOhlBNDG4Wv6ILdOty0jaccO8",
                "project": "csdnsz_dma",
                "endpoint": "http://service.cn-suzhou-szdn-d01.odps.cloud-ops.szdn.suzhou.gov.cn/api"
                }
od = ODPS(
          odps_config.get("access_id"),
          odps_config.get("access_key"), 
          odps_config.get("project"),
          endpoint=odps_config.get("endpoint"))

################################导入今日借还车数据################################
#按小时导入城管公共自行车扫码借还记录
sql = 'select * from csdnsz_bas.cg_bike_log' + '\n' \
    + 'where ((jcsk >= ' + '"' + str(begin) + '"' + ' and jcsk < '  + '"' + str(end) + '")' + '\n' \
    + 'or (ghsk >= '+ '"' + str(begin) + '"' + ' and ghsk < ' + '"' + str(end) + '"))'+ '\n' \
    + 'and ghsk > jcsk'

with od.execute_sql(sql).open_reader() as reader:
    data = reader
    data = data.to_pandas()

##用空数据进行尝试
#data = pd.DataFrame(columns=['id', 'ch', 'sjh', 'jczh', 'jcw', 'jcsk', 'hczh', 'hcw', 'ghsk', 'kxh','update_time', 'dt'])

#调整格式，并计算骑行时间  
data.loc[:,'jcsk'] = data.loc[:,'jcsk'].astype('datetime64')
data.loc[:,'ghsk'] = data.loc[:,'ghsk'].astype('datetime64')
data.loc[:,'duration'] = (data['ghsk'] - data['jcsk']).dt.total_seconds()
'''
清洗规则：
0. 挑选手机号、卡序号无缺失的数据
1. 挑选车号无缺失的数据
2. 挑选借、还车站号无缺失的数据
3. 挑选借车站号不为0 & 还车站号不为0 & 还车站号不为999999
'''
#构建清洗条件
cond0 = (data.loc[:,'sjh'].notna()) & (data.loc[:,'kxh'].notna())
cond1 = data.loc[:,'ch'].notna()
cond2 = (data.loc[:,'jczh'].notna()) & (data.loc[:,'hczh'].notna())
cond3 = (data.loc[:,'jczh'] != '0') & (data.loc[:,'hczh'] != '0') & (data.loc[:,'hczh'] != '999999')   
#构建新数据
data = data[cond0 & cond1 & cond2 & cond3]
data = data[['jczh','jcsk','hczh','ghsk','sjh','kxh','ch','duration']]
#拼接站点经纬度、站点名称
data = pd.merge(data, sta_info[['site_code','region_code','geometry']], how = 'inner', left_on = 'jczh', right_on = 'site_code')
data = pd.merge(data, sta_info[['site_code','region_code','geometry']], how = 'inner', left_on = 'hczh', right_on = 'site_code')

##############################按区域进行计算#####################################
#如果原始数据非空
if len(data.index)!=0: 
    #计算骑行距离
    data['distance'] = data.apply(lambda x: x['geometry_x'].distance(x['geometry_y']), axis=1)
    #按区域进行计算   
    result = pd.DataFrame()
    for region in ['320505','320506','320507','320508','320509','320571']:
        table = pd.DataFrame()
        bike_pool = set()
        #按时段进行计算
        for i in range(0,end_hour):
            #整理出借还数据
            tmp_rent = data[(data['region_code_x']==region) & (data['jcsk'].dt.hour==i)]
            tmp_return = data[(data['region_code_y']==region) & (data['ghsk'].dt.hour==i)]
            #按顺序计算指标
            table.loc[i,'stat_hour'] = i#统计时段
            table.loc[i,'rider'] = tmp_rent.groupby(by=['sjh','kxh']).size().shape[0]#区域_每小时骑行人数（人）
            table['cumsum_rider'] = table['rider'].transform(pd.Series.cumsum)#区域_按小时累计骑行人数（人）
            table.loc[i,'mile'] = tmp_rent['distance'].sum()#区域_每小时骑行里程（公里）
            table['cumsum_mile'] = table['mile'].transform(pd.Series.cumsum)#区域_按小时累计骑行里程（公里）
            table.loc[i,'ride'] = tmp_rent.shape[0]#区域_每小时骑行次数（次），骑行次数指借车次数
            table['cumsum_ride'] = table['ride'].transform(pd.Series.cumsum)#区域_按小时累计骑行次数（次），骑行次数指借车次数
            table.loc[i,'turnover'] = (tmp_rent.shape[0] + tmp_return.shape[0])/sta_info[sta_info['region_code']==region]['site_pile'].sum()#区域_每小时周转率（百分比）
            table['cumsum_turnover'] = table['turnover'].transform(pd.Series.cumsum)#区域_按小时累计周转率（百分比）
            bike_pool = bike_pool.union(tmp_rent['ch'].unique())
            table.loc[i,'usage'] = list(map(lambda x,y: x/y if y!=0 else 0, \
                                                    [tmp_rent.shape[0]],[tmp_rent['ch'].nunique()]))[0]#区域_每小时车辆使用率（次/辆）
            table.loc[i,'cumsum_usage'] = list(map(lambda x,y: x/y if y!=0 else 0, \
                                                    [table.loc[i,'cumsum_ride']],[len(bike_pool)]))[0]#区域_按小时累计车辆使用率（次/辆）
            table.loc[i,'ridetime'] = tmp_rent['duration'].sum()#区域_每小时骑行时间（秒）
            table['cumsum_ridetime'] = table['ridetime'].transform(pd.Series.cumsum)#区域_按小时累计骑行时间
            table.loc[i,'rent'] = tmp_rent.shape[0]
            table.loc[i,'return'] = tmp_return.shape[0]
            
        #根据行政区进行拼接
        table['region_code'] = region
        result = pd.concat([result,table], ignore_index=True)

    ##############只选择当前时刻的###############
    result = result[result['stat_hour']==i]
    #按字段顺序准备要导入的数据表
    df = pd.DataFrame()
    df['sr_region_code'] = result['region_code']
    df['sr_stat_year'] = str(begin.year)
    df['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
    df['sr_stat_date'] = str(begin.year) + '-' + str(begin.month).zfill(2) + '-' + str(begin.day).zfill(2)
    df['sr_stat_hour'] = result['stat_hour']
    df['sr_region_riderperhour'] = result['rider']
    df['sr_region_acumrider_hour'] = result['cumsum_rider']
    df['sr_region_mileperhour'] = result['mile'].apply(lambda x: '{:.0f}'.format(x/1000))#米换算成公里
    df['sr_region_acummile_hour'] =result['cumsum_mile'].apply(lambda x: '{:.0f}'.format(x/1000))#米换算成公里
    df['sr_region_rideperhour'] = result['ride']
    df['sr_region_acumride_hour'] = result['cumsum_ride']
    df['sr_region_turnoverperhour'] = result['turnover'].apply(lambda x: '{:.0f}'.format(x*100))#换算成百分比
    df['sr_region_acumturnover_hour'] = result['cumsum_turnover'].apply(lambda x: '{:.0f}'.format(x*100))#换算成百分比
    df['sr_region_usageperhour'] = result['usage'].apply(lambda x: '{:.2f}'.format(x))
    df['sr_region_acumusage_hour'] = result['cumsum_usage'].apply(lambda x: '{:.2f}'.format(x))
    df['sr_region_ridetimeperhour'] = result['ridetime'].apply(lambda x: '{:.0f}'.format(x/3600))
    df['sr_region_acumuridetime_hour'] = result['cumsum_ridetime'].apply(lambda x: '{:.0f}'.format(x/3600))
    df['sr_region_rentperhour'] = result['rent']
    df['sr_region_returnperhour'] = result['return']
    df['sr_insertedby'] = 'Xin-Yi Song'
    df['sr_updatedby'] = 'Xin-Yi Song'

###############################导入苏州项目数据库#################################
with engine.connect() as conn:
    #conn.execute('delete from srcity_index_bicycle_regiondmnd_hour')
    if len(data.index)!=0:
        print('df写入开始。。。')
        df.to_sql(
                  name = 'srcity_index_bicycle_regiondmnd_hour',
                  con = engine,
                  index = False,
                  if_exists = 'append'
                  )
        print('df写入成功过。。。')
    else:
        print(begin,'~',end,'无数据')
    
################################导入苏州项目数据库#################################
#只选择当前时刻的
dff = df[df['sr_stat_hour']==i]

#按字段顺序准备要导入的数据表
if len(data.index)!=0: 
    dff = dff[['sr_region_code', \
              'sr_stat_year', \
              'sr_stat_month', \
              'sr_stat_date', \
              'sr_region_acumrider_hour', \
              'sr_region_acummile_hour', \
              'sr_region_acumride_hour', \
              'sr_region_acumturnover_hour', \
              'sr_region_acumusage_hour', \
              'sr_region_acumuridetime_hour', \
              'sr_insertedby', \
              'sr_updatedby']]
    dff.columns = ['sr_region_code', \
                   'sr_stat_year', \
                   'sr_stat_month', \
                   'sr_stat_date', \
                   'sr_region_acumriders_date', \
                   'sr_region_acummileage_date', \
                   'sr_region_acumtimes_date', \
                   'sr_region_acumturnover_date', \
                   'sr_region_acumusage_date', \
                   'sr_region_acumridetime_date', \
                   'sr_insertedby', \
                   'sr_updatedby']
                      
        
with engine.connect() as conn:
    conn.execute('delete from srcity_index_bicycle_regiondmnd_day where sr_stat_date="' +  str(begin.year) + '-' + str(begin.month).zfill(2) + '-' + str(begin.day).zfill(2) + '"')
    if len(data.index)!=0: 
        print('dff写入开始。。。')
        dff.to_sql(
                  name = 'srcity_index_bicycle_regiondmnd_day',
                  con = engine,
                  index = False,
                  if_exists = 'append'
                  )
        print('dff写入成功。。。')
    else:
        print(begin,'~',end,'无数据')