#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 13:05:34 2020

@author: xin-yi.song
"""
import os
import sys
import json
import pandas as pd
import geopandas as gpd
from shapely.geometry import Polygon,Point
import time
#%matplotlib qt5

#规定脚本在bicyclesiteinfo.py后运行
#程序开始时间
start_time = time.time() 
################################连接RDS#########################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'db_sr_transport','utf8mb4'))

################################读取行政区信息###################################
#导入行政区边界信息，为gcj坐标
with engine.connect() as conn:
    query = 'select sr_region_code, sr_region_cname, sr_region_area, sr_region_boundarycns from srcity_bt_regioninfo'
    region = pd.read_sql(query, engine)

#重命名
region.columns=['region_code','region_cname','region_area','region_geojson']
#将geojson的中括号转化为polygon
region['geometry'] = region['region_geojson'].apply(lambda x: [tuple(i) for i in json.loads(x)[0][0]])
region['geometry'] = region['geometry'].apply(lambda x: Polygon(x))
region = gpd.GeoDataFrame(region, geometry=region['geometry'], crs={'init':'epsg:4326'})
region.to_crs({'init':'epsg:32651'}, inplace=True)
##通过绘图来检查
#base = region.plot(facecolor='none', edgecolor='black')

################################导入人口数据#####################################
#导入最大人口，为gcj坐标
with engine.connect() as conn:
    query = 'select sr_pop_pointgc, sr_pop_pointspatialtype, sr_pop_maxnum from srcity_ft_popdstr'
    pop = pd.read_sql(query, engine)
    
#调用construct_geodataframe函数，将geojson转化为geometry
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from construct_geodataframe import construct_gdf_from_df
pop = construct_gdf_from_df(pop, 'sr_pop_pointspatialtype', 'sr_pop_pointgc')
pop.to_crs({'init':'epsg:32651'}, inplace=True)

#行政区和人口基站相交
tmp = gpd.sjoin(region, pop, how='left', op='contains')
tmp = tmp.groupby(by=['region_code'])['sr_pop_maxnum'].sum().reset_index()
tmp['sr_pop_maxnum'] = tmp['sr_pop_maxnum'].apply(lambda x: '{:.2f}'.format(x/10000))
region = pd.merge(region, tmp, how='left', on='region_code')

################################导入站点数据#####################################
#导入站点信息，为gcj坐标
with engine.connect() as conn:
    query = 'select sr_site_code, sr_site_cname, sr_site_lon, sr_site_lat, sr_site_pilenum, sr_region_code from srcity_bt_bicyclesiteinfo'
    sta_info = pd.read_sql(query, engine)
    
#转化为dataframe格式
sta_info = pd.DataFrame(sta_info)
sta_info.columns = ['site_code','site_cname','lon','lat','site_pile','region_code']
#将经纬度组合成Point
sta_info['geometry'] = sta_info.apply(lambda x: Point(x['lon'],x['lat']),axis=1)
sta_info = gpd.GeoDataFrame(sta_info, geometry=sta_info['geometry'], crs={'init':'epsg:4326'})
sta_info.to_crs({'init':'epsg:32651'}, inplace=True)

#统计区域站点数/密度
tmp = sta_info.groupby(by=['region_code'])['site_code'].count().reset_index(name='site_num')
region = pd.merge(region, tmp, how='left', on='region_code')
region['site_density'] = region['site_num']/region['region_area']
region['site_density'] = region['site_density'].apply(lambda x: '{:.2f}'.format(x))

#统计区域桩位数/密度
tmp = sta_info.groupby(by=['region_code'])['site_pile'].sum().reset_index(name='pile_num')
region = pd.merge(region, tmp, how='left', on='region_code')
region['pile_density'] = region['pile_num']/region['region_area']
region['pile_density'] = region['pile_density'].apply(lambda x: '{:.2f}'.format(x))
#统计每万人桩位数
region['pile_percent'] = region['pile_num']/region['sr_pop_maxnum'].astype(float)
region['pile_percent'] = region['pile_percent'].apply(lambda x: '{:.2f}'.format(x))

#做出200米覆盖范围
buffers = sta_info.buffer(distance=200)
buffers = region.intersection(buffers.unary_union)
region['buffer_area'] = buffers.area 
region['buffer_area'] = region['buffer_area'].apply(lambda x: '{:.2f}'.format(x/10**6))
#统计站点覆盖率
region['buffer_percent'] = region['buffer_area'].astype(float)/region['region_area'].astype(float)
region['buffer_percent'] = region['buffer_percent'].apply(lambda x: '{:.2f}'.format(x))
#统计站点覆盖人口
region['buffer_pop'] = region['sr_pop_maxnum'].astype(float)*region['buffer_percent'].astype(float)
region['buffer_pop'] = region['buffer_pop'].apply(lambda x: '{:.2f}'.format(x))
##通过绘图来检查
#base = region.plot(facecolor='none', edgecolor='black')
#buffers.plot(ax=base, edgecolor='black', cmap='OrRd')

################################导入苏州项目数据库#################################
#按字段顺序准备要导入的数据表
df = pd.DataFrame()
df['sr_region_code'] = region['region_code']
df['sr_region_pop'] = region['sr_pop_maxnum']
df['sr_region_sitenum'] = region['site_num']
df['sr_region_sitedensity'] = region['site_density']
df['sr_region_pilenum'] = region['pile_num']
df['sr_region_piledensity'] = region['pile_density']
df['sr_region_pilenum_per'] = region['pile_percent']
df['sr_region_sitecvrarea'] = region['buffer_area']
df['sr_region_sitecvrrate'] = region['buffer_percent']
df['sr_region_sitecvrpop'] = region['buffer_pop']
df['sr_insertedby'] = 'Xin-Yi Song'
df['sr_updatedby'] = 'Xin-Yi Song'

with engine.connect() as conn:
    conn.execute('delete from srcity_index_bicycle_regionsupl')
    df.to_sql(
              name = 'srcity_index_bicycle_regionsupl',
              con = engine,
              index = False,
              if_exists = 'append'
              )

#程序结束时间
over_time = time.time() 
#程序运行时间
total_time = over_time - start_time
#打印耗费时间
print(total_time)