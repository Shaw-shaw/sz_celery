#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 15:32:20 2020

@author: xin-yi.song
"""

import pandas as pd
import datetime
import time
from shapely.geometry import Point
import geopandas as gpd
from sklearn import preprocessing  
#%matplotlib qt5

################################连接到RDS#######################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'db_sr_transport','utf8mb4'))

################################导入站点数据#####################################
#导入站点信息，为gcj坐标
with engine.connect() as conn:
    query = 'select sr_site_code, sr_site_cname, sr_site_pilenum, sr_region_code, sr_defregion_code, sr_site_lon, sr_site_lat from srcity_bt_bicyclesiteinfo'
    sta_info = pd.read_sql(query, engine)
    
#转化为dataframe格式
sta_info = pd.DataFrame(sta_info)
sta_info.columns = ['site_code','site_cname','site_pile','region_code','defregion_code','lon','lat']
#找出中医院站点
zhongyiyuan = list(sta_info[sta_info['defregion_code']=='sr005']['site_code'])
#找出拙政园站点
zhuozhengyuan = list(sta_info[sta_info['defregion_code']=='sr006']['site_code'])

################################连接到ODPS######################################
from odps import ODPS

odps_config = {
                "access_id": "V6MeotKzDBgOv6vI",
                "access_key": "P7YzxeRMfdr43e22C55ioOmsJP2UNu",
                "project": "csdnsz_dma",
                "endpoint": "http://service.cn-suzhou-szdn-d01.odps.cloud-ops.szdn.suzhou.gov.cn/api"
                }
od = ODPS(
          odps_config.get("access_id"),
          odps_config.get("access_key"), 
          odps_config.get("project"),
          endpoint=odps_config.get("endpoint"))

################################定义时间########################################
#for dd in pd.date_range(start='2020-05-01 00:40:00', end='2020-05-02 00:10:00', periods=48):
#定义时间，规定在每个小时的第10、40分钟，计算前半个小时的数据
now = datetime.datetime.now()
#now = datetime.datetime(2020,7,29,14,40)
#now = dd
    
begin = datetime.datetime((now-datetime.timedelta(minutes=40)).year, 
                          (now-datetime.timedelta(minutes=40)).month, 
                          (now-datetime.timedelta(minutes=40)).day, 
                          (now-datetime.timedelta(minutes=40)).hour, 
                          (now-datetime.timedelta(minutes=40)).minute, 0)
end = begin+datetime.timedelta(minutes=30)
    
this_month = datetime.datetime(begin.year,begin.month,1,0,0,0)
#    last_month = datetime.datetime((this_month-datetime.timedelta(days=1)).year, 
#                                   (this_month-datetime.timedelta(days=1)).month, 
#                                   (this_month-datetime.timedelta(days=1)).day, 
#                                   0, 0, 0)
last_month = datetime.datetime(2019, 11, 1, 0, 0, 0)
    
###############################查询日期属性##################################
'''星期以数字标识:Sunday=0, Monday=1, ..., Saturday=6
    日期类型:1：工作日, 2：休息日, 3：节假日'''  
date_type = {'1':'工作日', '2':'双休日', '3':'节假日'}
        
#查询当天的时间属性
sql = 'select * from traffic_base.dim_date_base' + '\n' \
    + 'where date_list=' + '"' + str(begin.year) + '-' + str(begin.month).zfill(2) + '-' + str(begin.day).zfill(2) + '"'
    
with od.execute_sql(sql).open_reader() as reader:
    date = reader
    date = date.to_pandas()
    
#############################按照自定义区域进行计算############################
#程序开始时间
start_time = time.time()  
    
#分别针对中医院、拙政园选取数据
for j in [zhuozhengyuan, zhongyiyuan]:
    if j == zhuozhengyuan:
        defregion = 'sr006'
    elif j == zhongyiyuan:
        defregion = 'sr005'
        
    ################################读取点位状态#################################
    #查询该小时的城管公共自行车点位状态实时数据（按站号、时间排序），7月之前在traffic_base.ods_bike_state_di里
    sql = 'select czh,czmc,cws,xxwzms,lon,lat,kjs,update_time from csdnsz_bas.cg_bike_state' + '\n' \
        + 'where update_time ' + '>= datetime(' + '"' + str(begin) + '")' + '\n' \
        + 'and update_time ' + '< datetime(' + '"' + str(end) + '")'+ '\n' \
        + 'and czh in ' + str(tuple(j)) + '\n' \
        + 'order by cast(czh as int),update_time'
        
    with od.execute_sql(sql).open_reader() as reader:
        site = reader
        site = site.to_pandas()
        
    ########################导入上一个月这个时段的平均借还量########################
    with engine.connect() as conn:
        query = 'select sr_site_code,sr_site_renthavgm,sr_site_returnhavgm from srcity_index_bicycle_defsiteefcy_havgm' + '\n' \
              + 'where sr_defregion_code="' + defregion + '"' + '\n' \
              + 'and sr_stat_month="' + str(last_month.year) + '-' + str(last_month.month).zfill(2) + '"' + '\n' \
              + 'and sr_stat_hour=' + '"' + str(end.hour) + '"' + '\n' \
              + 'and sr_date_type=' + '"' + date_type[date['date_type'][0]] + '"'
        efcy_havgm = pd.read_sql(query, engine)
    
    ###########################计算站点缺车/缺位状态##############################
    site = site.drop_duplicates(subset=['czh'], keep='last').reset_index(drop=True)
    site['cws'] = site['cws'].astype(int)
    site['kjs'] = site['kjs'].astype(int)
    site['remainpile'] = site['cws'] - site['kjs']
    site = pd.merge(site, efcy_havgm, left_on='czh', right_on='sr_site_code', how='inner')
    '''
    缺车/缺桩判断规则：
    缺车指标：（剩余可借车辆+下一时段还车数-下一时段借车数）/桩位数，（0，0.2）强，（0.2，0.8）中，（0.8，1）弱
    缺桩指标：（剩余桩位数+下一时段借车数-下一时段还车数）/桩位数，（0，0.2）强，（0.2，0.8）中，（0.8，1）弱
    '''        
    site['lackbike'] = (site['kjs']+site['sr_site_returnhavgm']/2-site['sr_site_renthavgm']/2)/site['cws']
    site['lackpile'] = (site['remainpile']+site['sr_site_renthavgm']/2-site['sr_site_returnhavgm']/2)/site['cws']
    #导入苏州项目数据库
    df = pd.DataFrame()
    df['sr_site_code'] = site['czh']
    df['sr_defregion_code'] = defregion
    df['sr_stat_year'] = str(begin.year)
    df['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
    df['sr_stat_date'] = str(begin.year) + '-' + str(begin.month).zfill(2) + '-' + str(begin.day).zfill(2)
    df['sr_date_type'] = date_type[date['date_type'][0]]
    if begin.minute==0:
        df['sr_stat_hour'] = '{:.1f}'.format(begin.hour)
    elif begin.minute==30:
        df['sr_stat_hour'] = '{:.1f}'.format(begin.hour+0.5)
    df['sr_site_remainpile'] = site['remainpile']
    df['sr_site_pilettl'] = site['cws']
    df['sr_site_lackbike'] = site['lackbike'].apply(lambda x: '强' if x<=0.2 else ('中' if x>0.2 and x<=0.8 else '弱'))
    df['sr_site_lackpile'] = site['lackpile'].apply(lambda x: '强' if x<=0.2 else ('中' if x>0.2 and x<=0.8 else '弱'))
    df['sr_insertedby'] = 'Xin-Yi Song'
    df['sr_updatedby'] = 'Xin-Yi Song'
    
    with engine.connect() as conn:
        #conn.execute('delete from srcity_index_bicycle_sitedispatchdmd_hour')
        df.to_sql(
                name = 'srcity_index_bicycle_sitedispatchdmd_hour',
                con = engine,
                index = False,
                if_exists = 'append'
                )    
        
    #########################计算自定义区域站点数/桩位数############################
    defregion_site = pd.DataFrame({'sitenum':[site['czh'].nunique()],
                                   'pilenum':[site['cws'].sum()],
                                   'remainpile':[site['remainpile'].sum()]})
    
    #导入苏州项目数据库
    dff = defregion_site.copy()
    dff.rename(columns={'sitenum':'sr_defregion_sitenum',
                        'pilenum':'sr_defregion_pilenum',
                        'remainpile':'sr_defregion_remainpilettl'}, inplace=True)
    dff['sr_defregion_code'] = defregion
    dff['sr_stat_year'] = str(begin.year)
    dff['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
    dff['sr_stat_date'] = str(begin.year) + '-' + str(begin.month).zfill(2) + '-' + str(begin.day).zfill(2)
    dff['sr_date_type'] = date_type[date['date_type'][0]]
    if begin.minute==0:
        dff['sr_stat_hour'] = '{:.1f}'.format(begin.hour)
    elif begin.minute==30:
        dff['sr_stat_hour'] = '{:.1f}'.format(begin.hour+0.5)
    dff['sr_insertedby'] = 'Xin-Yi Song'
    dff['sr_updatedby'] = 'Xin-Yi Song'    
        
    with engine.connect() as conn:
        #conn.execute('delete from srcity_index_bicycle_defregionpilecur')
        dff.to_sql(
                name = 'srcity_index_bicycle_defregionpilecur',
                con = engine,
                index = False,
                if_exists = 'append'
                )      
        
    ##########################计算推荐调度站点####################################
    #为每一个站点推荐可调度站点
    for sta in site['czh']:
        if len(site[site['czh']!=sta].index)!=0:
            #将经纬度组合成Point
            geom = [Point(xy) for xy in zip(site.lon, site.lat)]
            site = gpd.GeoDataFrame(site, geometry=geom)
            site.crs = {'init':'epsg:4326'}
            site.to_crs({'init':'epsg:32651'}, inplace=True)
            #导入苏州项目数据库
            dfff = site[site['czh']!=sta][['czh','geometry']].copy()
            dfff['sr_defregion_code'] = defregion
            dfff['sr_stat_year'] = str(begin.year)
            dfff['sr_stat_month'] = str(begin.year) + '-' + str(begin.month).zfill(2)
            dfff['sr_stat_date'] = str(begin.year) + '-' + str(begin.month).zfill(2) + '-' + str(begin.day).zfill(2)
            dfff['sr_date_type'] = date_type[date['date_type'][0]]
            if begin.minute==0:
                dfff['sr_stat_hour'] = '{:.1f}'.format(begin.hour)
            elif begin.minute==30:
                dfff['sr_stat_hour'] = '{:.1f}'.format(begin.hour+0.5)
            dfff['sr_site_code'] = sta
            dfff.rename(columns={'czh':'sr_site_code_nearby'}, inplace=True)
            dfff['sr_site_dist'] = dfff.apply(lambda x: x['geometry'].distance(site[site['czh']==sta]['geometry'].tolist()[0]), axis=1)
            dfff['sr_site_dist'] = dfff['sr_site_dist'].apply(lambda x: round(x))
            dfff.drop(columns='geometry',inplace=True)
            dfff['sr_site_callablemax'] = site[site['czh']!=sta]['kjs']#可调出车辆最大值
            dfff['sr_site_callablemin'] = site[site['czh']!=sta]['kjs'].apply(lambda x: round(x*0.8))#可调出车辆最小值
            dfff['sr_site_tunablemax'] = site[site['czh']!=sta]['remainpile']#可调入车辆最大值
            dfff['sr_site_tunablemin'] = site[site['czh']!=sta]['remainpile'].apply(lambda x: round(x*0.8))#可调入车辆最小值
            '''
            匹配度（越大越好）= 距离指数（最大距离/当前距离）
                            + 可调出车辆指数（当前可调出/缺车数）if sta 为缺车站点
                            + 可调入车辆指数（当前可调入/缺位数）if sta 为缺位站点
                            + 无 if sta 为中性站点
            备注：每个指数都进行了归一化
            '''
            minmax = preprocessing.MinMaxScaler() 
            dfff['sr_site_match'] = minmax.fit_transform((dfff['sr_site_dist'].max()/dfff['sr_site_dist']).values.reshape(-1, 1))
            if df[df['sr_site_code']==sta]['sr_site_lackbike'].tolist()[0]=='强':
                dfff['sr_site_match'] = dfff['sr_site_match'] \
                                      + dfff['sr_site_callablemax']/df[df['sr_site_code']==sta]['sr_site_remainpile'].tolist()[0]
            elif df[df['sr_site_code']==sta]['sr_site_lackpile'].tolist()[0]=='强':
                dfff['sr_site_match'] = dfff['sr_site_match'] \
                                      + dfff['sr_site_tunablemax']/site[site['czh']==sta]['kjs'].tolist()[0]
            dfff['sr_insertedby'] = 'Xin-Yi Song'
            dfff['sr_updatedby'] = 'Xin-Yi Song'      
        else:
            pass
            
            
        with engine.connect() as conn:
            if len(site[site['czh']!=sta].index)!=0:
                #conn.execute('delete from srcity_index_bicycle_sitedispatch_hour')
                dfff.to_sql(
                        name = 'srcity_index_bicycle_sitedispatch_hour',
                        con = engine,
                        index = False,
                        if_exists = 'append'
                        )   
            else:
                print(sta,'无可推荐站点')
    
    #程序结束时间
    over_time = time.time() 
    #程序运行时间
    total_time = over_time - start_time
    #打印耗费时间
    print(begin,'~',end,':',total_time)
