#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 13:52:08 2020

@author: xin-yi.song
"""

import pandas as pd
import datetime

#%matplotlib qt5

##规定在每个月1号的01:30，计算上个月数据的平均值
now = datetime.datetime.now()
#now = datetime.datetime(2020, 9, 1, 1, 30, 0)
last_month = now-datetime.timedelta(days=1)
#last_month = datetime.datetime(2019, 11, 1, 0, 0, 0)

################################连接到RDS#######################################
from sqlalchemy import create_engine

#engine = create_engine("mysql+pymysql://{}:{}@{}/{}?charset={}".format('用户名', '登录密码', '127.0.0.1:3306', 'port', '数据库名','字符编码'))
engine = create_engine("mysql+pymysql://{}:{}@{}:{}/{}?charset={}".format('qftc_01', 'qftc123', 'rm-tu3433ni4neu34l3o.mysql.rds.cloud-ops.szdn.suzhou.gov.cn', '3306', 'db_sr_transport','utf8mb4'))

###########################导入每小时站点运行效率评价##############################
#导入上个月的数据
with engine.connect() as conn:
    query = 'select * from srcity_index_bicycle_defsiteefcy_hour' + '\n' \
          + 'where (sr_defregion_code="sr005" or sr_defregion_code="sr006")' + '\n' \
          + 'and sr_stat_month=' + '"' + str(last_month.year) + '-' + str(last_month.month).zfill(2) + '"'

    efcy_hour = pd.read_sql(query, engine)
    
#转化为dataframe格式
efcy_hour = pd.DataFrame(efcy_hour)
#找出中医院站点
zhongyiyuan = list(efcy_hour[efcy_hour['sr_defregion_code']=='sr005']['sr_site_code'].unique())
#找出拙政园站点
zhuozhengyuan = list(efcy_hour[efcy_hour['sr_defregion_code']=='sr006']['sr_site_code'].unique())

#按日期类型计算平均值
for date_type in ['节假日', '工作日', '双休日']:#date_type = '工作日'
    tmp_hour = efcy_hour[efcy_hour['sr_date_type']==date_type]
    tmp_hour = tmp_hour.groupby(by=['sr_stat_hour','sr_site_code']).mean().reset_index()
    tmp_hour['sr_date_type'] = date_type
    #print(tmp_hour)
    
###############################导入苏州项目数据库#################################
    if len(tmp_hour.index)!=0: 
        #按字段顺序准备要导入的数据表
        df = tmp_hour.drop(columns=['id','sr_site_userperhour'])
        df['sr_defregion_code'] = df['sr_site_code'].apply(lambda x: 'sr005' if x in zhongyiyuan 
                                                               else ('sr006' if x in zhuozhengyuan else '0'))
        df['sr_stat_year'] = str(last_month.year)
        df['sr_stat_month'] = str(last_month.year) + '-' + str(last_month.month).zfill(2)
        df['sr_insertedby'] = 'Xin-Yi Song'
        df['sr_updatedby'] = 'Xin-Yi Song'
        df.rename(columns={'sr_site_rentperhour':'sr_site_renthavgm', \
                           'sr_site_returnperhour':'sr_site_returnhavgm', \
                           'sr_site_norentperhour':'sr_site_norenthavgm', \
                           'sr_site_noreturnperhour':'sr_site_noreturnhavgm'}, inplace=True)

        with engine.connect() as conn:
            #conn.execute('delete from srcity_index_bicycle_defsiteefcy_havgm')
            df.to_sql(
                    name = 'srcity_index_bicycle_defsiteefcy_havgm',
                    con = engine,
                    index = False,
                    if_exists = 'append'
                    )

        print(date_type, 'havgm:', df.shape[0])
        
#############################导入每日站点运行效率评价##############################
#导入上个月的数据
with engine.connect() as conn:
    query =  'select * from srcity_index_bicycle_defsiteefcy_day where sr_stat_month=' + '"' + str(last_month.year) + '-' + str(last_month.month).zfill(2) + '"'
            #'select * from srcity_index_bicycle_defsiteefcy_day where sr_stat_month="2019-11"'
    efcy_day = pd.read_sql(query, engine)
    
#转化为dataframe格式
efcy_day = pd.DataFrame(efcy_day)
#找出中医院站点
zhongyiyuan = list(efcy_day[efcy_day['sr_defregion_code']=='sr005']['sr_site_code'].unique())
#找出拙政园站点
zhuozhengyuan = list(efcy_day[efcy_day['sr_defregion_code']=='sr006']['sr_site_code'].unique())

#按日期类型计算平均值
for date_type in ['工作日', '双休日', '节假日']:#date_type = '工作日'
    tmp_day = efcy_day[efcy_day['sr_date_type']==date_type]
    tmp_day = tmp_day.groupby(by=['sr_site_code']).mean().reset_index()
    tmp_day['sr_date_type'] = date_type
    #print(tmp_day)
    
###############################导入苏州项目数据库#################################
    if len(tmp_day.index)!=0: 
        #按字段顺序准备要导入的数据表
        dff = tmp_day.drop(columns=['id'])
        dff['sr_defregion_code'] = df['sr_site_code'].apply(lambda x: 'sr005' if x in zhongyiyuan 
                                                                else ('sr006' if x in zhuozhengyuan else '0'))
        dff['sr_stat_year'] = str(last_month.year)
        dff['sr_stat_month'] = str(last_month.year) + '-' + str(last_month.month).zfill(2)
        dff['sr_insertedby'] = 'Xin-Yi Song'
        dff['sr_updatedby'] = 'Xin-Yi Song'
        dff.rename(columns={'sr_site_userperday':'sr_site_userdavgm', \
                           'sr_site_rentperday':'sr_site_rentdavgm', \
                           'sr_site_returnperday':'sr_site_returndavgm', \
                           'sr_site_turnoverperday':'sr_site_turnoverdavgm', \
                           'sr_site_norentperday':'sr_site_norentdavgm', \
                           'sr_site_noreturnperday':'sr_site_noreturndavgm', \
                           'sr_site_usernumperday_one':'sr_site_usernumdavgm_one', \
                           'sr_site_usernumperday_two':'sr_site_usernumdavgm_two', \
                           'sr_site_usernumperday_three':'sr_site_usernumdavgm_three', \
                           'sr_site_usernumperday_fourm':'sr_site_usernumdavgm_fourm'}, inplace=True)

        dff['sr_site_userdavgm'] = dff['sr_site_userdavgm'].apply(lambda x:'{:.1f}'.format(x))
        dff['sr_site_rentdavgm'] = dff['sr_site_rentdavgm'].apply(lambda x:'{:.1f}'.format(x))
        dff['sr_site_returndavgm'] = dff['sr_site_returndavgm'].apply(lambda x:'{:.1f}'.format(x))
        dff['sr_site_turnoverdavgm'] = dff['sr_site_turnoverdavgm'].apply(lambda x:'{:.4f}'.format(x))
        dff['sr_site_norentdavgm'] = dff['sr_site_norentdavgm'].apply(lambda x:'{:.1f}'.format(x/24))
        dff['sr_site_noreturndavgm'] = dff['sr_site_noreturndavgm'].apply(lambda x:'{:.1f}'.format(x/24))
        

        with engine.connect() as conn:
            #conn.execute('delete from srcity_index_bicycle_defsiteefcy_davgm')
            dff.to_sql(
                    name = 'srcity_index_bicycle_defsiteefcy_davgm',
                    con = engine,
                    index = False,
                    if_exists = 'append'
                    )   

        print(date_type, 'davgm:', dff.shape[0])    
    
